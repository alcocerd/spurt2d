//
//  QuadTexture.vsh
//  spurt2d
//
//  Created by Daniel Alcocer on 12/29/13.
//
//

// Position of vertex.
attribute vec4 position;

// Texture coordinate of vertex.
attribute vec2 textureCoordinate;
varying vec2 textureCoordinateOut;

void main(void)
{
    textureCoordinateOut = textureCoordinate;
    
    gl_Position = position;
}
