//
//  QuadTextureColor.vsh
//  spurt2d
//
//  Created by Daniel Alcocer on 12/17/13.
//
//

// Position of vertex.
attribute vec4 position;

// Color of vertex.
attribute vec4 color;
varying lowp vec4 colorOut;

// Texture coordinate of vertex.
attribute vec2 textureCoordinate;
varying vec2 textureCoordinateOut;

void main(void)
{
    colorOut = color;
    textureCoordinateOut = textureCoordinate;
    
    gl_Position = position;
}
