//
//  QuadTextureColor.fsh
//  spurt2d
//
//  Created by Daniel Alcocer on 12/17/13.
//
//

// Color.
varying lowp vec4 colorOut;
// Texture coordinate.
varying highp vec2 textureCoordinateOut;

// Texture to render.
uniform sampler2D textureID;

void main(void)
{
    gl_FragColor = colorOut * texture2D(textureID, textureCoordinateOut);
}
