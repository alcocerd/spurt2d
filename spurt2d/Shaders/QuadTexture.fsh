//
//  QuadTexture.fsh
//  spurt2d
//
//  Created by Daniel Alcocer on 12/29/13.
//
//

// Texture coordinate.
varying highp vec2 textureCoordinateOut;

// Texture to render.
uniform sampler2D textureID;

void main(void)
{
    gl_FragColor = texture2D(textureID, textureCoordinateOut);
}
