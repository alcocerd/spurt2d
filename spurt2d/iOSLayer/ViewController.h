//
//  ViewController.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/17/13.
//
//

#import <GLKit/GLKit.h>

@interface ViewController : GLKViewController

-(void)pause;
-(void)unpause;

@end
