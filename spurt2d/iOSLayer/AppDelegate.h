//
//  AppDelegate.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/17/13.
//
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
@private
    ViewController* viewController;
}
@property (strong, nonatomic) UIWindow *window;

@end
