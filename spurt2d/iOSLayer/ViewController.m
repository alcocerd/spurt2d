//
//  ViewController.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/17/13.
//
//

#import "ViewController.h"

#import "Director.h"

@interface ViewController ()
{
    Director* director;
}

@property (strong, nonatomic) EAGLContext* context;

-(void)tearDownGL;

@end

@implementation ViewController

static const int PREFERRED_FRAMES_PER_SECOND = 60;

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    // If this class has this method (7.0 or later), update the status bar.
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        // This should utilize the overridden "prefersStatusBarHidden" method.
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    // Create our OpenGL ES 2 context.
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    if (!self.context)
    {
        logCritical(@"Failed to create ES context");
    }
    
    // A few configurations.
    GLKView* view = (GLKView*) self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    self.preferredFramesPerSecond = PREFERRED_FRAMES_PER_SECOND;
    
    [EAGLContext setCurrentContext:self.context];
    
    director = [[Director alloc] initWithContext:self.context andView:(GLKView*)view andShowExample:true];
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // TODO: Test this method out and find out what we should be doing here (especially in the if statement).
    if ([self isViewLoaded] && ([[self view] window] == nil))
    {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context)
        {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }
    
    // Dispose of any resources that can be recreated.
}

-(void)tearDownGL
{
    // I believe this is here for the case that tearDownGL (from didReceiveMemoryWarning maybe) occurs before viewDidLoad, but not sure why it's necessary.
    [EAGLContext setCurrentContext:self.context];
    
    // Delete buffers, shaders, etc.
    // TODO: Make a method in Director that handles this.
}

#pragma mark - GLKView and GLKViewController delegate methods

-(void)update
{
    // Director handles how we do logic and rendering, so we just use the other method.
}

-(void)glkView:(GLKView*)view drawInRect:(CGRect)rect
{
    [director update];
}

#pragma mark - Input

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    [director touchesBegan:touches];
}

-(void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    [director touchesMoved:touches];
}

-(void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
    [director touchesEnded:touches];
}

#pragma mark - Pausing

-(void)pause
{
    [director pause];
}

-(void)unpause
{
    [director unpause];
}

#pragma mark - View Orientation

// These are set up for portrait orientation, UIInterfaceOrientationIsLandscape can also be used for landscape.
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:duration];
        [UIView commitAnimations];
    }
}

#pragma mark -

-(void)dealloc
{
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context)
    {
        [EAGLContext setCurrentContext:nil];
    }
}

@end
