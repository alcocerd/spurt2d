//
//  main.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/17/13.
//
//

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
