#!/bin/bash

# Go to this directory.
cd ${0%/*}

# Convert.
for i in *.wav; do
	afconvert -f AIFC -d ima4 -c 1 $i;
done

# Move the files to resources. Aifc files go in the music folder.
for filename in *.aifc; do
	mv "$filename" "../../../resources/audio/music"
done

exit 0