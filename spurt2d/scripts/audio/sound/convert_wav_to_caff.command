#!/bin/bash

# Go to this directory.
cd ${0%/*}

# Convert.
for i in *.wav; do
	afconvert -f caff -d LEI16 -c 1 $i;
done

# Move the files to resources. Caff files go in the sound folder.
for filename in *.caf; do
	mv "$filename" "../../../resources/audio/sound"
done

exit 0