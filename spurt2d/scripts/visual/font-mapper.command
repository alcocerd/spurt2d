#!/bin/bash

##############################
# A WORD OF CAUTION:
# If you plan on publishing any sort of content that uses fonts, MAKE SURE YOU KNOW HOW THE LICENSE OF THOSE FONTS WORK.  Be careful!
##############################

# Go to the directory that this script file resides in.
cd ${0%/*}

# Do some java compilations.
mkdir bin
javac -d bin src/com/danielalcocer/spurtmap/*.java
javac -d bin -cp bin src/com/danielalcocer/spurtmap/texturemapping/*.java
javac -d bin -cp bin src/com/danielalcocer/spurtmap/fontmapping/*.java

# First parameter is the directory.

# Second parameter is the max size.  You can safely pick a big size, as the code will crop the texture down.

# Third parameter is how much padding you want between each character.  Now, this parameter keeps me up at night, because ideally we shouldn't need it.  But without going into much detail, it's something I don't have time to develop around at the moment.  Anywho, if you are using this in conjunction with the texture mapping, you generally want this number to be 2^(n-1), where n is your scale count when you call texturemapping.Main.  So if your scale count is 3, 2^(3-1) = 4 is your padding.  I hate this just as much as you do, but if you are using this for other purposes, just make it 0!

# Fourth parameter is a boolean for producing extra "debug" textures which display the bounding boxes of the characters.  This is really handy so that an artist can more easily dress up the fonts to their liking.

# Fifth and sixth parameters are where the textures and mapping files go (respectively).
# If the directories don't exist, they will be created.

java -cp bin com.danielalcocer.spurtmap.fontmapping.Main fonts/ 4096 4 false textures/mappedFonts/ ../../source/graphics/assets/

# Done.
exit 0

######################################################################

# How to prepare your chosen directory:
# 1. Place any true type font files you want to use in that directory (currently just checks the root, it won't search in subfolders).
# 2. Place a file in that directory called "fonts" (again, the root).
# 3. For every font you want to map, place this information on its own line:

# <filename with extension> <font size> <ascii start value, inclusive> <ascii end value, inclusive> <horizontal padding.  This is typically only needed for italics or fonts which go out of their ascent bounds, I hate it but I haven't found a way around it.  You'll know you need it when characters leak into other characters' bounding boxes (then just a few pixels should do the trick, it will be relative to the size of the font).  Otherwise, just use 0.>

# Don't include the # symbol, an example would be:

# ArialBold.ttf 40 33 126 10
# Arial.ttf 40 33 126 0

# This will map a bold and regular Arial font, each ranging from the '!' symbol to the '~' symbol.
