#!/bin/bash

# Go to the directory that this script file resides in.
cd ${0%/*}

# Do some java compilations.
mkdir bin
javac -d bin src/com/danielalcocer/spurtmap/*.java
javac -d bin -cp bin src/com/danielalcocer/spurtmap/texturemapping/*.java
javac -d bin -cp bin src/com/danielalcocer/spurtmap/fontmapping/*.java

# First parameter is the directory.  I personally will want my resources to reside
# outside of this folder, something like: ../../folder/to/textures/

# Second parameter is the max size.  This should be the maximum texture size your
# targeted highest-end GPU can handle (for instance, iPad 3 can handle 4096).

# Third parameter is how many times you want it to scale down.  This suits my need just
# fine for mobile development, but the code could easily be changed to take a list
# of scales (and if you do that, you should probably change how the max size parameter
# works).

# Fifth and sixth parameters are where the textures and mapping files go (respectively).
# If the directories don't exist, they will be created.

java -cp bin com.danielalcocer.spurtmap.texturemapping.Main textures/ 4096 3 ../../resources/visual/ ../../source/graphics/assets/

# Done.
exit 0


# Extra notes:
# If you are having memory issues with the heap, try making the heap bigger with this parameter (or go even bigger!):

# -Xmx512m