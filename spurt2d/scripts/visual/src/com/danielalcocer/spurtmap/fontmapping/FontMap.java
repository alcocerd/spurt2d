package com.danielalcocer.spurtmap.fontmapping;

public class FontMap
{
	private CharacterAddress[] characterAddresses;
	
	private String name;
	private int asciiStart;
	private int height;
	private int spaceWidth;
	
	//private int textureWidth;
	private int textureHeight;
	
	private int horizontalPadding;
	
	public FontMap(CharacterAddress[] characterAddresses, String name, int asciiStart, int height, int spaceWidth, int textureWidth, int textureHeight, int horizontalPadding)
	{
		this.characterAddresses = characterAddresses;
		
		this.name = name;
		this.asciiStart = asciiStart;
		this.height = height;
		this.spaceWidth = spaceWidth;
		
		//this.textureWidth = textureWidth;
		this.textureHeight = textureHeight;
		
		this.horizontalPadding = horizontalPadding;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getAsciiStart()
	{
		return asciiStart;
	}
	
	public float getSpaceWidth()
	{
		return spaceWidth;
	}
	
	public float getHeight()
	{
		return height;
	}
	
	public float getHorizontalPadding()
	{
		return horizontalPadding;
	}
	
	public float[] getLefts()
	{
		float[] lefts = new float[characterAddresses.length];
		int index = 0;
		for (CharacterAddress characterAddress : characterAddresses)
		{
			lefts[index] = characterAddress.getX();
			index++;
		}
		
		return lefts;
	}
	
	public float[] getBottoms()
	{
		float[] bottoms = new float[characterAddresses.length];
		int index = 0;
		for (CharacterAddress characterAddress : characterAddresses)
		{
			// Make the origin the bottom left of the texture.
			bottoms[index] = textureHeight - characterAddress.getY();
			index++;
		}
		
		return bottoms;
	}
	
	public float[] getWidths()
	{
		float[] widths = new float[characterAddresses.length];
		int index = 0;
		for (CharacterAddress characterAddress : characterAddresses)
		{
			widths[index] = characterAddress.getWidth();
			index++;
		}
		
		return widths;
	}
	
	public int getCharacterCount()
	{
		return characterAddresses.length;
	}
}
