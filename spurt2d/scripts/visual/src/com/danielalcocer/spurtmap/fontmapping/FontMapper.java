package com.danielalcocer.spurtmap.fontmapping;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.danielalcocer.spurtmap.texturemapping.TextureMapper;

public class FontMapper
{
	private static final String FONT_FILE_NAME = "fonts";
	
	private PrintWriter debugWriter = null;
	
	public void begingMapping(String[] arguments)
	{
		String fontDirectory = arguments[0];
		
		int maxSize = Integer.parseInt(arguments[1]);
		int padding = Integer.parseInt(arguments[2]);
		boolean drawCharacterFrames = Boolean.parseBoolean(arguments[3]);
		String fontTextureOutputDirectory = arguments[4];
		String fontAddressOutputDirectory = arguments[5];
		
		// Make the directories if they didn't already exist.
		new File(fontTextureOutputDirectory).mkdirs();
		new File(fontAddressOutputDirectory).mkdirs();
		
		try
		{
			debugWriter = new PrintWriter(fontDirectory + "debug_font_addresses.txt", "UTF-8");
			
			BufferedReader bufferedReader = new BufferedReader(new FileReader(fontDirectory + FONT_FILE_NAME));
			String line = bufferedReader.readLine();
			
			ArrayList<FontMap> fontMaps = new ArrayList<FontMap>();
			
			while (line != null)
			{
				String[] parameters = line.split("\\s+");
				String name = parameters[0];
				int fontSize = Integer.parseInt(parameters[1]);
				int asciiStart = Integer.parseInt(parameters[2]);
				int asciiEnd = Integer.parseInt(parameters[3]);
				// Anywhere in this code that uses horizontalPadding is unfortunate, since the solution is so simple, but the
				// information to fix it is simply not there.  This is necessary since some characters render outside of their
				// rendering areas.  Even Oracle suggests this padding method:
				// http://www.oracle.com/technetwork/java/index-137037.html#Q_What_is_the_difference_between
				int horizontalPadding = Integer.parseInt(parameters[4]);
				
				// Load the front from the directory passed in.
				Font font = loadFont(fontDirectory, name, fontSize);
				
				// Strip out the file extension and add what the ascii range is.
				String adjustedName = name.substring(0, name.lastIndexOf('.')) + fontSize + "_" + asciiStart + "_" + asciiEnd;
				
				// Draw it and save it.
				FontMap fontMap = createFontMap(false, font, fontTextureOutputDirectory, adjustedName, asciiStart, maxSize, asciiEnd, padding, horizontalPadding);
				// Record it.
				fontMaps.add(fontMap);
				
				// Debugging, let's us see a version with frames in the texture.
				if (drawCharacterFrames)
					createFontMap(true, font, fontTextureOutputDirectory, adjustedName + "_WithFrames", asciiStart, maxSize, asciiEnd, padding, horizontalPadding);
				
				line = bufferedReader.readLine();
			}
			
			bufferedReader.close();
			
			ObjCFontWriter objCFontWriter = new ObjCFontWriter(fontAddressOutputDirectory, TextureMapper.TEXTURE_ASSET_CLASS_NAME, TextureMapper.TEXTURE_ASSET_ENUMERATOR, TextureMapper.TEXTURE_ASSET_PREFIX);
			objCFontWriter.write(fontMaps);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			//System.out.println("List probable errors here.");
		}
		
		if (debugWriter != null)
		{
			debugWriter.close();
		}
	}
	
	// Ascii range so you can just print lower case, or just upper case, or just numbers, etc.
	private BufferedImage drawFont(boolean drawCharacterFrames, BufferedImage fontTexture, Font font, int asciiStart, int maxWidth, int padding, CharacterAddress[] addresses, int horizontalPadding)
	{
		// Set up the font.
		Graphics2D graphics = this.createGraphics(fontTexture, font);
		FontMetrics fontMetrics = graphics.getFontMetrics();
		
		int maxDescent = fontMetrics.getMaxDescent();
		int fontHeight = fontMetrics.getHeight();
		
		// Build the string of ascii characters.
		int stringBoxWidth = 0;
		int stringBoxHeight;
		int currentX = 0;
		int currentY = fontHeight;
		for (int i = 0; i < addresses.length; i++)
		{
			char character = (char) (i + asciiStart);
			String characterAsString = String.valueOf(character);
			int characterLength = fontMetrics.stringWidth(characterAsString);
			
			if (currentX + characterLength + horizontalPadding > maxWidth)
			{
				currentX = 0;
				currentY += fontHeight + padding;
			}
			
			currentX += horizontalPadding;
			
			addresses[i] = new CharacterAddress(currentX, currentY, characterLength);
			
			// Debug frames, if you plan on editing your fonts after running this, just make sure the character fits
			// in the box.
			if (drawCharacterFrames)
			{
				graphics.setColor(Color.PINK);
				graphics.fillRect(currentX-horizontalPadding, currentY-fontHeight, characterLength+(horizontalPadding * 2), fontHeight);
			}
			
			// Draw the string.
			int baseLine = currentY - maxDescent;
			graphics.setColor(Color.WHITE);
			graphics.drawString(characterAsString, currentX, baseLine);
			
			// Accumulate string length.
			currentX += characterLength + padding + horizontalPadding;
			
			// Ensure we always get the max width.
			if (currentX > stringBoxWidth)
			{
				stringBoxWidth = currentX;
			}
		}
		
		stringBoxWidth -= padding;
		stringBoxHeight = currentY;
		
		// Crop the texture.
		return fontTexture.getSubimage(0, 0, stringBoxWidth, stringBoxHeight);
	}
	
	private Font loadFont(String directory, String name, float size)
	{
		Font sizedFont = null;
		File fontFile = new File(directory + name);
		// TODO: Have code here that checks if the font is on the system if the fontFile does not exist.
		// TODO: Catch FontFormatException to explain to the user that the OS does not like the format of the file.
		System.out.println("Mapping " + name);
		try
		{
			// Make the font.
			Font font = Font.createFont(Font.TRUETYPE_FONT, fontFile);
			sizedFont = font.deriveFont(size);
			// Register the font.
			// TODO: Do we need to register it?
			GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
			graphicsEnvironment.registerFont(font);
			
			/*Font[] allFonts = graphicsEnvironment.getAllFonts();
			for (Font test : allFonts)
			{
				System.out.println(test.getName());
			}*/
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return sizedFont;
	}
	
	private FontMap createFontMap(boolean drawCharacterFrames, Font font, String fontTextureOutputDirectory, String name, int asciiStart, int maxSize, int asciiEnd, int padding, int horizontalPadding)
	{
		BufferedImage fontTexture = new BufferedImage(maxSize, maxSize, BufferedImage.TYPE_INT_ARGB);
		
		int characterCount = asciiEnd - asciiStart + 1;
		CharacterAddress[] addresses = new CharacterAddress[characterCount];
		fontTexture = drawFont(drawCharacterFrames, fontTexture, font, asciiStart, maxSize, padding, addresses, horizontalPadding);
		
		TextureMapper.writePNG(fontTexture, fontTextureOutputDirectory, name);
		
		Graphics2D graphics = this.createGraphics(fontTexture, font);
		FontMetrics fontMetrics = graphics.getFontMetrics();
		// These 2 things are global for the entire font (ie don't change on a per character basis).
		int fontHeight = fontMetrics.getHeight();
		int spaceWidth = fontMetrics.stringWidth(String.valueOf(' '));
		
		if (!drawCharacterFrames)
		{
			debugWriter.println(name + ", font height: " + fontHeight + ", space width: " + spaceWidth + ", horizontal padding: " + horizontalPadding);
			
			for (int i = 0; i < addresses.length; i++)
			{
				CharacterAddress address = addresses[i];
				debugWriter.println(address.getX() + " " + address.getY() + " " + address.getWidth());
			}
		}
		
		fontTexture.flush();
		
		return new FontMap(addresses, name, asciiStart, fontHeight, spaceWidth, fontTexture.getWidth(), fontTexture.getHeight(), horizontalPadding);
	}
	
	public Graphics2D createGraphics(BufferedImage fontTexture, Font font)
	{
		Graphics2D graphics = fontTexture.createGraphics();
		graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		graphics.setFont(font);
		
		return graphics;
	}
}
