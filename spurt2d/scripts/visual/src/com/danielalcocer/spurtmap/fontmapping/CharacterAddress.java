package com.danielalcocer.spurtmap.fontmapping;

public class CharacterAddress
{
	private int x;
	private int y;
	private int width;
	
	public CharacterAddress(int x, int y, int width)
	{
		this.x = x;
		this.y = y;
		this.width = width;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public int getWidth()
	{
		return width;
	}
}
