package com.danielalcocer.spurtmap;

// Note: This class is no longer being used for writing out Objective-C classes, as it was decided to let the implementation
// decide how to handle rendering.  However, this class may still be useful for other purposes.

// This class handles texel mapping so that we don't get texture bleeding.
public class HalfPixelCorrection
{
	// Assuming a bottom-left origin coordinate system, use this for left-side and bottom-side pixel coordinates.
	public static float correctUp(int value)
	{
		return (float)value + 0.5f;
	}
	
	// Assuming a bottom-left origin coordinate system, use this for right-side and top-side pixel coordinates.
	public static float correctDown(int value)
	{
		return (float)value - 0.5f;
	}
	
	// If coordinates are stored as "x" and "width" and "y" and "height," then the width and heights should be
	// corrected with this function.
	public static float correctLength(int value)
	{
		return (float)value - 1.0f;
	}
}
