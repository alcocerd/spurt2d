package com.danielalcocer.spurtmap.texturemapping;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

public class TextureAddress
{
	private String filePath;
	
	private int scaleFactor;
	
	private int width;
	private int height;
	
	private int x;
	private int y;
	
	private int page;
	
	public TextureAddress(File textureFile, int scaleFactor)
	{
		this.scaleFactor = scaleFactor;
		assignIdentity(textureFile);
	}
	
	private void assignIdentity(File textureFile)
	{
		filePath = textureFile.getAbsolutePath();
		
		ImageInputStream inStream = null;
		try
		{
			inStream = ImageIO.createImageInputStream(textureFile);
			Iterator<ImageReader> imageReaders = ImageIO.getImageReaders(inStream);
			// If we have readers.
		    if (imageReaders.hasNext())
		    {
		    	// Read the image and grab the width and height.
		        ImageReader imageReader = imageReaders.next();
		        imageReader.setInput(inStream);
		        // Should we round here or let it just truncate down?
		        width = imageReader.getWidth(0) / scaleFactor;
		        height = imageReader.getHeight(0) / scaleFactor;
		        
		        // Ensure no 0's.
		        if (width < 1)
		        	width = 1;
		        if (height < 1)
		        	height = 1;
		        
		        // Done with it.
		        imageReader.dispose();
		    }
		    // Close the stream.
		    if (inStream != null)
		    {
		    	inStream.flush();
		    	inStream.close();
		    }
		}
		catch (Exception e)
		{
			System.out.println("Failed getting dimensions of texture: " + textureFile.getAbsolutePath());
			e.printStackTrace();
		}
	}
	
	public void assignAddress(int x, int y, int page)
	{
		this.x = x;
		this.y = y;
		this.page = page;
	}
	
	public String getFormattedAddress()
	{
		return getName() + " " + x + " " + y + " " + width + " " + height + " " + page;
	}
	
	public String getName()
	{
		int nameStart = filePath.lastIndexOf('/') + 1;
		// If not found, just start from the beginning.
		if (nameStart == -1)
			nameStart = 0;
		int nameEnd = filePath.lastIndexOf('.');
		
		return filePath.substring(nameStart, nameEnd);
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public int getPage()
	{
		return page;
	}
	
	public int getArea()
	{
		return width * height;
	}
	
	public int getLongestSide()
	{
		if (width > height)
			return width;
		else
			return height;
	}
	
	public void drawTexture(BufferedImage mappedTexture)
	{
		File textureFile = new File(filePath);
		try
		{
			// Read it into memory.
			BufferedImage texture = ImageIO.read(textureFile);
			
			// Scale it appropriately.
			texture = scaleTexture(texture);
			
			// Draw the texture on the mapped texture.
			Graphics graphics = mappedTexture.getGraphics();
			graphics.drawImage(texture, x, y, null);
			
			graphics.dispose();
			texture.flush();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private BufferedImage scaleTexture(BufferedImage texture)
	{
		// Only scale if the scaling will actually do something.
		if (scaleFactor != 1)
		{
			int width = texture.getWidth() / scaleFactor;
			int height = texture.getHeight() / scaleFactor;
			if (width <= 0)
				width = 1;
			if (height <= 0)
				height = 1;
			
			// Apparently this is not the best way to scale textures (as per some light googling), but in terms of quality, 
			// it meets my expectations.
			// TODO: If time permits, look into other scaling methods.
			Image scaledTexture = texture.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
			texture = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			texture.getGraphics().drawImage(scaledTexture, 0, 0, null);
		}
		
		return texture;
	}
	
	// We want images to not only be originated by their bottom left corner, but also, the entire map's origin
	// should be the bottom left corner.
	public float getBottomByBottomLeftOrigin(float mapHeight)
	{
		return mapHeight - y - height;
	}
}
