package com.danielalcocer.spurtmap.texturemapping;

import java.util.ArrayList;

public class TextureMap
{
	private ArrayList<TextureAddress> textureAddresses;
	private ArrayList<String> fileNames;
	private int scaleTier;
	private int scaleFactor;
	
	private int width;
	private int height;
	
	public TextureMap(int scaleTier, int scaleFactor, int width, int height)
	{
		textureAddresses = new ArrayList<TextureAddress>();
		fileNames = new ArrayList<String>();
		
		this.scaleTier = scaleTier;
		this.scaleFactor = scaleFactor;
		
		this.width = width;;
		this.height = height;
	}
	
	public void addTexturePage(ArrayList<TextureAddress> texturePage)
	{
		textureAddresses.addAll(texturePage);
	}
	
	public void addFileName(String fileName)
	{
		fileNames.add(fileName);
	}
	
	public ArrayList<TextureAddress> getTextureAddresses()
	{
		return textureAddresses;
	}
	
	public ArrayList<String> getfileNames()
	{
		return fileNames;
	}
	
	public int getScaleTier()
	{
		return scaleTier;
	}
	
	public int getScaleFactor()
	{
		return scaleFactor;
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
}
