package com.danielalcocer.spurtmap.texturemapping;

// TODO: Add support for animations.
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.imageio.ImageIO;

public class TextureMapper
{
	public static final String TEXTURE_ASSET_CLASS_NAME = "TextureMapping";
	public static final String TEXTURE_ASSET_ENUMERATOR = "TEXTURE_ASSET";
	public static final String TEXTURE_ASSET_PREFIX = "TA_";
	
	private static final String SCALE_TIER_OVERRIDE = "MAP_RESERVED_TIER_";
	private static final String PNG = "png";
	
	private PrintWriter addressWriter = null;
	
	public void beginMapping(String[] arguments)
	{
		try
		{
			String textureDirectory = arguments[0];
			int maxSize = Integer.parseInt(arguments[1]);
			int scaleCount = Integer.parseInt(arguments[2]);
			String textureOutputDirectory = arguments[3];
			String addressOutputDirectory = arguments[4];
			
			// Make the directories if they didn't already exist.
			new File(textureOutputDirectory).mkdirs();
			new File(addressOutputDirectory).mkdirs();
			
			if (maxSize > 0 && scaleCount > 0)
			{
				map(textureDirectory, textureOutputDirectory, addressOutputDirectory, maxSize, scaleCount);
			}
			else
			{
				System.out.println("max size and scale count must be greater than 0.");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Remember that three arguments are expected: a valid directory, the maximum size of the map, and the number of scales to perform.\n" +
					"Example: ../some/folder/ 4096 4");
		}
	}
	
	// Function that goes through a given directory, and compiles all of the textures into one big map (or a few maps).
	private void map(String inputDirectory, String textureOutputDirectory, String addressOutputDirectory, int maxSize, int scaleCount)
	{
		// For writing our addresses to a file.
		try
		{
			addressWriter = new PrintWriter(inputDirectory + "debug_texture_addresses.txt", "UTF-8");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		// Go to the directory.
		File textureDirectory = new File(inputDirectory);		
		
		// Information to write out the file.
		ArrayList<TextureMap> textureMaps = new ArrayList<TextureMap>();
		
		for (int scaleTier = 0; scaleTier < scaleCount; scaleTier++)
		{
			// Cut it in half at every scale tier.
			int scaleFactor = getScaleFactor(scaleTier);
			int scaledMaxSize = maxSize / scaleFactor;
			
			TextureMap textureMap = new TextureMap(scaleTier, scaleFactor, scaledMaxSize, scaledMaxSize);
			
			LinkedList<TextureAddress> textureAddresses = new LinkedList<TextureAddress>();
			
			// Go through the directory, and find out the dimensions of each texture, filling in the list.
			evaluateDirectory(textureDirectory, textureAddresses, scaleTier);
			
			int mapPage = 0;
			// We now have a list of textures that need to be mapped (each iteration will produce a new map).
			while (!textureAddresses.isEmpty())
			{
				// Map for this set of textures.
				ArrayList<TextureAddress> texturePage = new ArrayList<TextureAddress>();
				
				// Now fit as many textures into that texture as possible.
				mapTextureAddresses(texturePage, textureAddresses, mapPage, 0, 0, scaledMaxSize, scaledMaxSize);
				
				// Create a large texture where we will place all of the textures into.
				BufferedImage mappedTexture = new BufferedImage(scaledMaxSize, scaledMaxSize, BufferedImage.TYPE_INT_ARGB);
				// Draw them onto the map and spit out their addresses in a text file.
				commitTextures(texturePage, mappedTexture);
								
				// Save the texture to disk.
				String fileName = scaleTier + "_mappedtexture_" + mapPage;
				writePNG(mappedTexture, textureOutputDirectory, fileName);
				
				// Add this page to the map.
				textureMap.addTexturePage(texturePage);
				textureMap.addFileName(fileName);
				
				// Done with it.
				mappedTexture.flush();
				
				mapPage++;
			}
			
			// Add the map to the list of maps.
			textureMaps.add(textureMap);
		}
		
		ObjCTextureWriter objCTextureWriter = new ObjCTextureWriter(addressOutputDirectory);
		objCTextureWriter.write(textureMaps);
		
		if (addressWriter != null)
		{
			addressWriter.close();
		}
	}
	
	// Go through the directory, adding to the mapped texture.
	private void evaluateDirectory(File folder, LinkedList<TextureAddress> textureAddresses, int scaleTier)
	{
		for (File file : folder.listFiles())
		{
			String fileName = file.getName();
			// If it's a folder, go through it.
			if (file.isDirectory())
			{
				// If this folder is flagged to override the scaling...
				if (fileName.contains(SCALE_TIER_OVERRIDE))
				{
					// Then make sure it's for this tier.
					if (fileName.equals(SCALE_TIER_OVERRIDE + scaleTier))
					{
						System.out.println("found it! tier " + scaleTier);
						// Evaluate it.
						evaluateDirectory(file, textureAddresses, scaleTier);
					}
				}
				// Otherwise just do regular scaling.
				else
				{
					evaluateDirectory(file, textureAddresses, scaleTier);
				}
			}
			else
			{
				int index = fileName.lastIndexOf('.');
				
				if (index > 0)
				{
					String extensionType = fileName.substring(index + 1);
					
					// Make sure it's a png and add it to the list of textures to map.
					if (extensionType.equalsIgnoreCase(PNG))
					{
						TextureAddress textureAddress = new TextureAddress(file, getScaleFactor(scaleTier));
						textureAddresses.add(textureAddress);
					}
				}
			}
		}
	}
	
	// TODO: Should log when a texture is bigger than MAX size,
	// and when there are 2 textures have the same name.
	// maybe run these checks before we start mapping, and it doesn't map (so developers can fix the errors and re-run).
	
	// This algorithm works by placing textures to the right of it, below it, and to the bottom right of it. 
	// Then, it finds the best-fitting texture that can fit in the bounds it has (which is very easy to calculate, usually just the edge
	// of the texture or a parent gives you the bound).
	// Repeat.
	private void mapTextureAddresses(ArrayList<TextureAddress> texturePage, LinkedList<TextureAddress> textureAddresses, int mapPage, int x, int y, int maxWidth, int maxHeight)
	{
		TextureAddress biggestValidTexture = null;
		int foundIndex = -1;
		int index = 0;
		// Go through the list.
		for (TextureAddress textureAddress : textureAddresses)
		{
			// Find the one that fits best given the max width and height.
			if ((biggestValidTexture == null || textureAddress.getLongestSide() > biggestValidTexture.getLongestSide()) && textureAddress.getWidth() <= maxWidth && textureAddress.getHeight() <= maxHeight)
			{
				biggestValidTexture = textureAddress;
				foundIndex = index; 
			}
			index++;
		}
		
		if (foundIndex >= 0)
		{
			biggestValidTexture.assignAddress(x, y, mapPage);
			texturePage.add(biggestValidTexture);
			textureAddresses.remove(foundIndex);
			
			int left = x;
			int top = y;
			int right = x + biggestValidTexture.getWidth();
			int bottom = y + biggestValidTexture.getHeight();
			int extraHorizontal = maxWidth - biggestValidTexture.getWidth();
			int extraVertical = maxHeight - biggestValidTexture.getHeight();
			
			// Map everything to the right of this texture.
			mapTextureAddresses(texturePage, textureAddresses, mapPage, right, top, extraHorizontal, biggestValidTexture.getHeight());
			// Map everything below this texture.
			mapTextureAddresses(texturePage, textureAddresses, mapPage, left, bottom, biggestValidTexture.getWidth(), extraVertical);
			// Map everything to the right and below this texture.
			mapTextureAddresses(texturePage, textureAddresses, mapPage, right, bottom, extraHorizontal, extraVertical);
		}
	}
	
	// Draw them onto the map and spit out their addresses in a text file.
	private void commitTextures(ArrayList<TextureAddress> texturePage, BufferedImage mappedTexture)
	{
		for (TextureAddress textureAddress : texturePage)
		{
			textureAddress.drawTexture(mappedTexture);
			// TODO: Formatted address do not tell us what scale factor they have, fix this bug.  Should we separate each scale
			// tier out into its own file like the texture maps?
			addressWriter.println(textureAddress.getFormattedAddress());
		}
	}
	
	private int getScaleFactor(int scaleTier)
	{
		return (int) Math.pow(2, scaleTier);
	}
	
	public static void writePNG(BufferedImage texture, String textureOutputDirectory, String name)
	{
		try
		{
			FileOutputStream outStream = new FileOutputStream(textureOutputDirectory + name + "." + PNG);
			ImageIO.write(texture, PNG, outStream);
			
			outStream.flush();
			outStream.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
