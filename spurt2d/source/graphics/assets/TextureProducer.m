//
//  TextureProducer.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/28/13.
//
//

#import "TextureProducer.h"

#import <GLKit/GLKit.h>

#import "ScreenOrientator.h"

#import "FontMapping.h"

@interface TextureProducer ()

-(id)initTextureProducer;

-(void)loadResourcesByScaleTier:(int)scaleTier;

-(int)loadTexture:(NSString*)imageName;

@end

@implementation TextureProducer

-(id)initTextureProducer
{
    if (self = [super init])
    {
        [self loadResourcesByScaleTier:[[ScreenOrientator sharedInstance] getScaleTier]];
    }
    return self;
}

+(TextureProducer*)sharedInstance
{
    static TextureProducer* textureProducer;
    static dispatch_once_t once;
    dispatch_once(&once, ^
    {
        textureProducer = [[TextureProducer alloc] initTextureProducer];
    });
    
    return textureProducer;
}

-(void)loadResourcesByScaleTier:(int)scaleTier
{
    [TextureMapping load:scaleTier];
    
    // Load textures.
    textureMapNames = [[NSMutableArray alloc] init];
    NSArray* textureFileNames = [TextureMapping getFileNames];
    
    for (NSString* textureFileName in textureFileNames)
    {
        int textureMapName = [self loadTexture:textureFileName];
        [textureMapNames addObject:[NSNumber numberWithInt:textureMapName]];
    }
    
    // Load fonts too.
    [FontMapping load:[TextureMapping getScaleFactor]];
}

-(TextureData)getTextureData:(TEXTURE_ASSET)textureAsset
{
    TextureData textureData;
    textureData.name = [self getTextureName:textureAsset];
    
    textureData.left = [TextureMapping getLeft:textureAsset];
    textureData.bottom = [TextureMapping getBottom:textureAsset];
    textureData.width = [TextureMapping getWidth:textureAsset];
    textureData.height = [TextureMapping getHeight:textureAsset];
    
    textureData.assetWidth = [TextureMapping getPageWidth];
    textureData.assetHeight = [TextureMapping getPageHeight];
    
    return textureData;
}

-(int)getTextureName:(TEXTURE_ASSET)textureAsset
{
    int page = [TextureMapping getPage:textureAsset];
    
    return [[textureMapNames objectAtIndex:page] intValue];
}

#pragma mark - Texture loading

-(int)loadTexture:(NSString*)imageName
{
    // Put the image in memory.
    UIImage* image = [UIImage imageNamed:imageName];
    CGImageRef imageRef = image.CGImage;
    if (!imageRef)
    {
        logCritical(@"Failure loading image: %@", imageName);
    }
    
    size_t width = image.size.width;
    size_t height = image.size.height;
    size_t bytesPerPixel = 4;
    size_t bitsPerComponent = 8;
    
    // This is what we will end up sending to the GPU once we fill it in with the appropriate data.
    GLubyte* imageData = (GLubyte*) calloc(width * height * bytesPerPixel, sizeof(GLubyte));
    
    // Create a context to help build out the image data.
    CGContextRef imageContext = CGBitmapContextCreate(imageData, width, height, bitsPerComponent, width * bytesPerPixel, CGImageGetColorSpace(imageRef), (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    
    // The image origin starts at top left instead of bottom left, so we flip it to be bottom left.
    CGContextTranslateCTM(imageContext, 0, height);
    CGContextScaleCTM(imageContext, 1.0, -1.0);
    
    // Finally build our image data.
    CGContextDrawImage(imageContext, CGRectMake(0, 0, (CGFloat)width, (CGFloat)height), imageRef);
    // Release the context.
    CGContextRelease(imageContext);
    
    // Get the GPU ready to recieve the upload.
    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    
    // Texture rendering settings.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    // Send the image to the GPU.
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)width, (GLsizei)height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
    
    // We don't need duplicate image data, we don't need this one anymore now that the GPU has it.
    free(imageData);
    
    return textureID;
}

@end
