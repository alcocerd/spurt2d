//
//  TextureProducer.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/28/13.
//
//

#import "TextureMapping.h"

#import "ShaderQuad.h"

@interface TextureProducer : NSObject
{
@private
    NSMutableArray* textureMapNames;
}

+(TextureProducer*)sharedInstance;

-(TextureData)getTextureData:(TEXTURE_ASSET)textureAsset;
-(int)getTextureName:(TEXTURE_ASSET)textureAsset;

@end
