// This class generated by Spurtmap.

typedef enum
{
	TA_bybsy40_33_126 = 0,
	TA_bybsy30_33_126,
	TA_testimage,

	TA_TEXTURE_NAME_COUNT

} TEXTURE_ASSET;

@interface TextureMapping : NSObject
{

}

+(void)load:(int)scaleTier;

+(float)getScaleFactor;
+(NSArray*)getFileNames;
+(float)getLeft:(TEXTURE_ASSET)textureAsset;
+(float)getBottom:(TEXTURE_ASSET)textureAsset;
+(float)getWidth:(TEXTURE_ASSET)textureAsset;
+(float)getHeight:(TEXTURE_ASSET)textureAsset;
+(int)getPage:(TEXTURE_ASSET)textureAsset;
+(float)getPageWidth;
+(float)getPageHeight;

@end
