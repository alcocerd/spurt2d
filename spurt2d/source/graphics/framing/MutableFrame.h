//
//  MutableFrame.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/20/14.
//
//

#import "Frame.h"

@interface MutableFrame : Frame

// Accessors.
-(MutableCoordinate2d*)getPosition;
-(MutableCoordinate2d*)getScale;
-(MutableCoordinate1d*)getRotation;

/**
 Parent.  A frame can be parentless (nil).
 */
-(void)setParent:(Frame*)newParent;

// Alignments.
-(void)setHorizontalAlignment:(HORIZONTAL_ALIGNMENT)newHorizontalAlignment;
-(void)setVerticalAlignment:(VERTICAL_ALIGNMENT)newVerticalAlignment;

/**
 Sets the x position of the frame based on its current alignment (so if the alignment is set to its left side, this will set its left side to that value).
 */
-(void)setHorizontal:(float)x;
/**
 Directly sets the horizontal center of the Frame to this value regardless of its alignment.  This does not change the frame's alignment.
 */
-(void)setHorizontalCenter:(float)center;
/**
 Directly sets the left of the Frame to this value regardless of its alignment.  This does not change the frame's alignment.
 */
-(void)setLeft:(float)left;
/**
 Directly sets the right of the Frame to this value regardless of its alignment.  This does not change the frame's alignment.
 */
-(void)setRight:(float)right;

/**
 Sets the y position of the frame based on its current alignment (so if the alignment is set to its bottom side, this will set its bottom side to that value).
 */
-(void)setVertical:(float)y;
/**
 Directly sets the vertical center of the Frame to this value regardless of its alignment.  This does not change the frame's alignment.
 */
-(void)setVerticalCenter:(float)center;
/**
 Directly sets the bottom of the Frame to this value regardless of its alignment.  This does not change the frame's alignment.
 */
-(void)setBottom:(float)bottom;
/**
 Directly sets the top of the Frame to this value regardless of its alignment.  This does not change the frame's alignment.
 */
-(void)setTop:(float)top;

// Set margins from the parent's sides, these are based on current alignment.
-(void)setMarginLeft:(float)margin;
-(void)setMarginRight:(float)margin;
-(void)setMarginBottom:(float)margin;
-(void)setMarginTop:(float)margin;

// Scale.
-(void)setScaleUniform:(float)uniformScale;
-(void)setHorizontalScale:(float)horizontalScale;
-(void)setVerticalScale:(float)verticalScale;

// Rotation.
-(void)setRotationAsDegrees:(float)degrees;
-(void)setRotationAsRadians:(float)radians;

// Dimensions.
-(void)setWidth:(float)newWidth;
-(void)setHeight:(float)newHeight;

-(void)addHorizontal:(float)x;
-(void)addVertical:(float)y;

@end
