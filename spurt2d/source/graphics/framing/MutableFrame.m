//
//  MutableFrame.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/20/14.
//
//

#import "MutableFrame.h"

#import "MutableCoordinate1d.h"
#import "MutableCoordinate2d.h"

#import "Math.h"

@implementation MutableFrame

-(MutableCoordinate2d*)getPosition
{
    return position;
}

-(MutableCoordinate2d*)getScale
{
    return scale;
}

-(MutableCoordinate1d*)getRotation
{
    return rotation;
}

#pragma mark - Parent mutators

-(void)setParent:(Frame*)newParent
{
    parent = newParent;
}

#pragma mark - Alignment mutators

-(void)setHorizontalAlignment:(HORIZONTAL_ALIGNMENT)newHorizontalAlignment
{
    horizontalAlignment = newHorizontalAlignment;
}

-(void)setVerticalAlignment:(VERTICAL_ALIGNMENT)newVerticalAlignment
{
    verticalAlignment = newVerticalAlignment;
}

#pragma mark - Position mutators

-(void)setHorizontal:(float)x
{
    [position setX:x];
}

-(void)setHorizontalCenter:(float)center
{
    switch (horizontalAlignment)
    {
        case HA_CENTER:
        {
            [self setHorizontal:center];
            break;
        }
        case HA_LEFT:
        {
            [self setHorizontal:center - [self getScaledHalfWidth]];
            break;
        }
        case HA_RIGHT:
        {
            [self setHorizontal:center + [self getScaledHalfWidth]];
            break;
        }
        default:
        {
            logWarning(@"Alignment not found in %@", NSStringFromSelector(_cmd));
            break;
        }
    }
}

-(void)setLeft:(float)left
{
    float center = left + [self getScaledHalfWidth];
    [self setHorizontalCenter:center];
}

-(void)setRight:(float)right
{
    float center = right - [self getScaledHalfWidth];
    [self setHorizontalCenter:center];
}

-(void)setVertical:(float)y
{
    [position setY:y];
}

-(void)setVerticalCenter:(float)center
{
    switch (verticalAlignment)
    {
        case VA_CENTER:
        {
            [self setVertical:center];
            break;
        }
        case VA_BOTTOM:
        {
            [self setVertical:center - [self getScaledHalfHeight]];
            break;
        }
        case VA_TOP:
        {
            [self setVertical:center + [self getScaledHalfHeight]];
            break;
        }
        default:
        {
            logWarning(@"Alignment not found in %@", NSStringFromSelector(_cmd));
            break;
        }
    }
}

-(void)setBottom:(float)bottom
{
    float center = bottom + [self getScaledHalfHeight];
    [self setVerticalCenter:center];
}

-(void)setTop:(float)top
{
    float center = top - [self getScaledHalfHeight];
    [self setVerticalCenter:center];
}

#pragma mark - Position mutators - margins

-(void)setMarginLeft:(float)margin
{
    float side = -[[self getParent] getScaledHalfWidth];
    [self setHorizontal:side + margin];
}

-(void)setMarginRight:(float)margin
{
    float side = [[self getParent] getScaledHalfWidth];
    [self setHorizontal:side - margin];
}

-(void)setMarginBottom:(float)margin
{
    float side = -[[self getParent] getScaledHalfHeight];
    [self setVertical:side + margin];
}

-(void)setMarginTop:(float)margin
{
    float side = [[self getParent] getScaledHalfHeight];
    [self setVertical:side - margin];
}

#pragma mark - Scale mutators

-(void)setScaleUniform:(float)uniformScale
{
    [self setHorizontalScale:uniformScale];
    [self setVerticalScale:uniformScale];
}

-(void)setHorizontalScale:(float)horizontalScale
{
    [scale setX:horizontalScale];
}

-(void)setVerticalScale:(float)verticalScale
{
    [scale setY:verticalScale];
}

#pragma mark - Rotation mutators

-(void)setRotationAsDegrees:(float)degrees
{
    [rotation setX:degrees * RADIANS_PER_DEGREE];
}

-(void)setRotationAsRadians:(float)radians
{
    [rotation setX:radians];
}

#pragma mark - Dimension mutators

-(void)setWidth:(float)newWidth
{
    width = newWidth;
}

-(void)setHeight:(float)newHeight
{
    height = newHeight;
}

#pragma mark - Add

-(void)addHorizontal:(float)x
{
    [position addX:x];
}

-(void)addVertical:(float)y
{
    [position addY:y];
}

@end
