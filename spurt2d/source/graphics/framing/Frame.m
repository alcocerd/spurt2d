//
//  Frame.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/20/14.
//
//

#import "Frame.h"

#import "MutableCoordinate1d.h"
#import "MutableCoordinate2d.h"

#import "Math.h"
#import "ScreenOrientator.h"

@implementation Frame

static Frame* defaultParentFrame;

+(void)initialize
{
    static BOOL runOnce = false;
    if (!runOnce)
    {
        runOnce = true;
        
        defaultParentFrame = [[Frame alloc] initWithX:0.0f andY:0.0f andWidth:[[ScreenOrientator sharedInstance] getWidth] andHeight:[[ScreenOrientator sharedInstance] getHeight]];
    }
}

#pragma mark - Constructors

-(id)init
{
    return [self initWithX:0.0f andY:0.0f andWidth:1.0f andHeight:1.0f];
}

-(id)initWithPosition:(MutableCoordinate2d*)newPosition andScale:(MutableCoordinate2d*)newScale andRotation:(MutableCoordinate1d*)newRotation andWidth:(float)newWidth andHeight:(float)newHeight andVerticalAlignment:(VERTICAL_ALIGNMENT)newVerticalAlignment andHorizontalAlignment:(HORIZONTAL_ALIGNMENT)newHorizontalAlignment andParent:(Frame*)newParent
{
    if (self = [super init])
    {
        parent = newParent;
        position = newPosition;
        scale = newScale;
        rotation = newRotation;
        
        width = newWidth;
        height = newHeight;
        
        verticalAlignment = newVerticalAlignment;
        horizontalAlignment = newHorizontalAlignment;
        
        isCullingEnabled = true;
    }
    return self;
}

-(id)initWithX:(float)x andY:(float)y andWidth:(float)newWidth andHeight:(float)newHeight
{
    MutableCoordinate2d* p = [[MutableCoordinate2d alloc] initWithX:x andY:y];
    MutableCoordinate2d* s = [[MutableCoordinate2d alloc] initWithX:1.0f andY:1.0f];
    MutableCoordinate1d* r = [[MutableCoordinate1d alloc] initWithX:0.0f];
    
    return [self initWithPosition:p andScale:s andRotation:r andWidth:newWidth andHeight:newHeight andVerticalAlignment:VA_CENTER andHorizontalAlignment:HA_CENTER andParent:nil];
}

#pragma mark - Parent accessor

-(Frame*)getParent
{
    Frame* frame = parent;
    // If it's nil, set it to the default parent, unless this IS the default parent, in which case let it return nil.
    if (frame == nil && self != defaultParentFrame)
        frame = defaultParentFrame;
    
    return frame;
}

-(BOOL)hasParent
{
    return parent != nil;
}

#pragma mark - Position (relative) accessors

// X values, horizontal.
-(float)getHorizontal
{
    return [position getX];
}

-(float)getHorizontalCenter
{
    float center = 0.0f;
    switch (horizontalAlignment)
    {
        case HA_CENTER:
        {
            center = [self getHorizontal];
            break;
        }
        case HA_LEFT:
        {
            center = [self getHorizontal] + [self getScaledHalfWidth];
            break;
        }
        case HA_RIGHT:
        {
            center = [self getHorizontal] - [self getScaledHalfWidth];
            break;
        }
        default:
        {
            logWarning(@"Alignment not found in %@", NSStringFromSelector(_cmd));
            break;
        }
    }
    
    return center;
}

-(float)getLeft
{
    return [self getHorizontalCenter] - [self getScaledHalfWidth];
}

-(float)getRight
{
    return [self getHorizontalCenter] + [self getScaledHalfWidth];
}

// Y values, vertical.
-(float)getVertical
{
    return [position getY];
}

-(float)getVerticalCenter
{
    float center = 0.0f;
    switch (verticalAlignment)
    {
        case VA_CENTER:
        {
            center = [self getVertical];
            break;
        }
        case VA_BOTTOM:
        {
            center = [self getVertical] + [self getScaledHalfHeight];
            break;
        }
        case VA_TOP:
        {
            center = [self getVertical] - [self getScaledHalfHeight];
            break;
        }
        default:
        {
            logWarning(@"Alignment not found in %@", NSStringFromSelector(_cmd));
            break;
        }
    }
    
    return center;
}

-(float)getBottom
{
    return [self getVerticalCenter] - [self getScaledHalfHeight];
}

-(float)getTop
{
    return [self getVerticalCenter] + [self getScaledHalfHeight];
}

#pragma mark - Position (absolute) accessors

-(float)getHorizontalCenterAsAbsolute
{
    float x = [self getHorizontalCenter] + [[self getParent] getHorizontalCenterAsAbsolute];
    return x;
}

-(float)getLeftAsAbsolute
{
    float x = [self getLeft] + [[self getParent] getHorizontalCenterAsAbsolute];
    return x;
}

-(float)getRightAsAbsolute
{
    float x = [self getRight] + [[self getParent] getHorizontalCenterAsAbsolute];
    return x;
}

-(float)getVerticalCenterAsAbsolute
{
    float y = [self getVerticalCenter] + [[self getParent] getVerticalCenterAsAbsolute];
    return y;
}

-(float)getBottomAsAbsolute
{
    float y = [self getBottom] + [[self getParent] getVerticalCenterAsAbsolute];
    return y;
}

-(float)getTopAsAbsolute
{
    float y = [self getTop] + [[self getParent] getVerticalCenterAsAbsolute];
    return y;
}

#pragma mark - Scale accessors

-(float)getHorizontalScale
{
    return [scale getX];
}

-(float)getVerticalScale
{
    return [scale getY];
}

#pragma mark - Rotation accessors

-(float)getRotationAsDegrees
{
    return [self getRotationAsRadians] * RADIANS_PER_DEGREE;
}

-(float)getRotationAsRadians
{
    return [rotation getX];
}

#pragma mark - Dimensions

-(float)getWidth
{
    return width;
}

-(float)getHeight
{
    return height;
}

-(float)getHalfWidth
{
    return [self getWidth] / 2.0f;
}

-(float)getHalfHeight
{
    return [self getHeight] / 2.0f;
}

-(float)getScaledWidth
{
    return [self getWidth] * [scale getX];
}

-(float)getScaledHeight
{
    return [self getHeight] * [scale getY];
}

-(float)getScaledHalfWidth
{
    return [self getScaledWidth] / 2.0f;
}

-(float)getScaledHalfHeight
{
    return [self getScaledHeight] / 2.0f;
}

#pragma mark - Culling

-(BOOL)isCullable
{
    BOOL cullable = false;
    
    if (isCullingEnabled)
    {
        float left = [self getLeftAsAbsolute];
        float right = [self getRightAsAbsolute];
        float bottom = [self getBottomAsAbsolute];
        float top = [self getTopAsAbsolute];
        
        float leftBound = [[ScreenOrientator sharedInstance] getLeft];
        float rightBound = [[ScreenOrientator sharedInstance] getRight];
        float bottombound = [[ScreenOrientator sharedInstance] getBottom];
        float topBound = [[ScreenOrientator sharedInstance] getTop];
        
        cullable = true;
        // First line is horizontal bounds, second is vertical bounds (if both vertical and horizontal are inside the bounds, we will not cull).  Last line checks if the entire frame is larger than the bounds itself, then that counts as being inside as well.
        if (((((left > leftBound && left < rightBound) || (right > leftBound && right < rightBound) || (left <= leftBound && right >= rightBound)) &&
              ((bottom > bottombound && bottom < topBound) || (top > bottombound && top < topBound) || (bottom <= bottombound && top >= topBound))) ||
             (left <= leftBound && right >= rightBound && bottom <= bottombound && top >= topBound)))
        {
            cullable = false;
        }
    }
    
    return cullable;
}

-(void)enableCulling:(BOOL)newIsCullingEnabled
{
    isCullingEnabled = newIsCullingEnabled;
}

-(BOOL)isInside:(Coordinate2d*)coordinate
{
    BOOL isInside = false;
    
    float x = [coordinate getX];
    float y = [coordinate getY];
    
    if ((x >= [self getLeftAsAbsolute] && x <= [self getRightAsAbsolute]) && (y >= [self getBottomAsAbsolute] && y <= [self getTopAsAbsolute]))
    {
        isInside = true;
    }
    
    return isInside;
}

@end
