//
//  Frame.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/20/14.
//
//

@class Coordinate2d;

@class MutableCoordinate1d;
@class MutableCoordinate2d;

typedef enum
{
    VA_CENTER = 0,
    VA_BOTTOM,
    VA_TOP,
    
} VERTICAL_ALIGNMENT;

typedef enum
{
    HA_CENTER = 0,
    HA_LEFT,
    HA_RIGHT,
    
} HORIZONTAL_ALIGNMENT;

// TODO: Add support so we can inherit scale and rotation from parents.  Scale is fairly trivial but rotation can be a bit more involved considering how robust we want to make it (flexibility for the developer to control the point of rotation, sometimes the center of the parent, sometimes the center of the child, or other possibilities).  Currently not worth the effort (position is all that is needed for now), but in the future it could be.
@interface Frame : NSObject
{
@protected
    Frame* __weak parent;
    MutableCoordinate2d* position;
    MutableCoordinate2d* scale;
    // In radians.
    MutableCoordinate1d* rotation;
    
    float width;
    float height;
    
    VERTICAL_ALIGNMENT verticalAlignment;
    HORIZONTAL_ALIGNMENT horizontalAlignment;
    
    BOOL isCullingEnabled;
}

// Rotation is in radians.
-(id)initWithPosition:(MutableCoordinate2d*)newPosition andScale:(MutableCoordinate2d*)newScale andRotation:(MutableCoordinate1d*)newRotation andWidth:(float)newWidth andHeight:(float)newHeight andVerticalAlignment:(VERTICAL_ALIGNMENT)newVerticalAlignment andHorizontalAlignment:(HORIZONTAL_ALIGNMENT)newHorizontalAlignment andParent:(Frame*)newParent;
-(id)initWithX:(float)x andY:(float)y andWidth:(float)newWidth andHeight:(float)newHeight;

// If no parent is found, defaults to the default parent frame (which has the dimensions of the screen and its origin in the middle of the screen).
-(Frame*)getParent;
// Returns true if parent is not nil.
-(BOOL)hasParent;

// X values, relative.
/**
 Returns the y value based on its current alignment.
 */
-(float)getHorizontal;
// Center.
-(float)getHorizontalCenter;
// Left.
-(float)getLeft;
// Right.
-(float)getRight;

// Y values, relative.
/**
 Returns the y value based on its current alignment.
 */
-(float)getVertical;
// Center.
-(float)getVerticalCenter;
// Bottom.
-(float)getBottom;
// Top.
-(float)getTop;

// Returns the values in absolute space.
-(float)getHorizontalCenterAsAbsolute;
-(float)getLeftAsAbsolute;
-(float)getRightAsAbsolute;
-(float)getVerticalCenterAsAbsolute;
-(float)getBottomAsAbsolute;
-(float)getTopAsAbsolute;

// Scale.
-(float)getHorizontalScale;
-(float)getVerticalScale;

// Rotation.
-(float)getRotationAsDegrees;
-(float)getRotationAsRadians;

// Dimensional information.
-(float)getWidth;
-(float)getHeight;
-(float)getHalfWidth;
-(float)getHalfHeight;

// Dimemsional information - scaled.
-(float)getScaledWidth;
-(float)getScaledHeight;
-(float)getScaledHalfWidth;
-(float)getScaledHalfHeight;

/**
 Checks if any part of this frame is inside the view of the screen.  If any part is inside of the screen, it returns false, otherwise, return true.  Currently does not take rotation into account.
 */
-(BOOL)isCullable;

/**
 True to allow the frame to check for culling (will draw if not out of screen bounds), false will never cull (the frame will always draw, even if out of screen bounds).
 */
-(void)enableCulling:(BOOL)newIsCullingEnabled;

/**
 Checks if the coordinate is inside the frame, rotation is not taken into account.
 */
-(BOOL)isInside:(Coordinate2d*)coordinate;

@end
