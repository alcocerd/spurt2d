//
//  Quad.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/28/13.
//
//

#import "Quad.h"

#import "ShaderQuadTex.h"
#import "ShaderQuadTexCol.h"

#import "ScreenOrientator.h"
#import "TextureProducer.h"

#import "MutableFrame.h"
#import "MutableCoordinate1d.h"

#import <GLKit/GLKit.h>

@implementation Quad

-(id)init
{
    if (self = [super init])
    {
        [self setShaderQuad:nil];
        
        // Frame.
        frame = [[MutableFrame alloc] init];
        
        // Trimming.
        leftTrim = [MutableCoordinate1d coordinateWithX:1.0f];
        rightTrim = [MutableCoordinate1d coordinateWithX:1.0f];
        bottomTrim = [MutableCoordinate1d coordinateWithX:1.0f];
        topTrim = [MutableCoordinate1d coordinateWithX:1.0f];
        
        // Colors.
        red = [MutableCoordinate1d coordinateWithX:1.0f];
        green = [MutableCoordinate1d coordinateWithX:1.0f];
        blue = [MutableCoordinate1d coordinateWithX:1.0f];
        alpha = [MutableCoordinate1d coordinateWithX:1.0f];
        
        // Blend type.
        [self setBlendType:BT_DEFAULT];
        
        [self setActive:true];
    }
    return self;
}

+(Quad*)quadByTextureMapping:(TEXTURE_ASSET)textureAsset
{
    Quad* quad = [[Quad alloc] init];
    [quad setByTextureMapping:textureAsset];
    
    return quad;
}

-(void)draw
{
    if (isActive && ![frame isCullable])
    {
        // Model matrix.
        GLKMatrix4 modelMatrix = GLKMatrix4Identity;
        
        // Rotate
        float rotation = [frame getRotationAsRadians];
        modelMatrix = GLKMatrix4RotateZ(modelMatrix, rotation);
        
        // Scale.
        float horizontalScale = [frame getHorizontalScale];
        float verticalScale = [frame getVerticalScale];
        modelMatrix = GLKMatrix4ScaleWithVector4(modelMatrix, GLKVector4Make(horizontalScale, verticalScale, 0.0f, 1.0f));
        
        float leftTrimming = [leftTrim getX];
        float rightTrimming = [rightTrim getX];
        float bottomTrimming = [bottomTrim getX];
        float topTrimming = [topTrim getX];
        
        // Draw this image.
        float halfWidth = [frame getHalfWidth];
        float halfHeight = [frame getHalfHeight];
        float positions[8] = {  -halfWidth * leftTrimming, -halfHeight * bottomTrimming,
                                halfWidth * rightTrimming, -halfHeight * bottomTrimming,
                                -halfWidth * leftTrimming, halfHeight * topTrimming,
                                halfWidth * rightTrimming, halfHeight * topTrimming};
        
        float newPositions[8];
        float xPosition = [frame getHorizontalCenterAsAbsolute];
        float yPosition = [frame getVerticalCenterAsAbsolute];
        for (int i = 0; i < 8; i += 2)
        {
            GLKVector4 positionVector = GLKVector4Make(positions[i], positions[i+1], 0, 1);
            
            positionVector = GLKMatrix4MultiplyVector4(modelMatrix, positionVector);
            
            // Translate
            positionVector = GLKVector4Add(positionVector, GLKVector4Make(xPosition, yPosition, 0, 0));
            
            positionVector = [[ScreenOrientator sharedInstance] multiplyByProjectionMatrix:positionVector];
            
            newPositions[i] = positionVector.x;
            newPositions[i+1] = positionVector.y;
        }
        
        // Finalize our data to be sent off.
        PositionData trimmedPositionData = {newPositions[0], newPositions[1], newPositions[2], newPositions[3], newPositions[4], newPositions[5], newPositions[6], newPositions[7]};
        
        ColorData colorData = {[red getX], [green getX], [blue getX], [alpha getX]};
        
        TextureData trimmedTextureData = textureData;
        
        float halfWidthTexture = trimmedTextureData.width / 2.0f;
        float halfHeightTexture = trimmedTextureData.height / 2.0f;
        float textureCenterX = trimmedTextureData.left + halfWidthTexture;
        float textureCenterY = trimmedTextureData.bottom + halfHeightTexture;
        
        float leftTexture = textureCenterX - (halfWidthTexture * leftTrimming);
        float rightTexture = textureCenterX + (halfWidthTexture * rightTrimming);
        float bottomTexture = textureCenterY - (halfHeightTexture * bottomTrimming);
        float topTexture = textureCenterY + (halfHeightTexture * topTrimming);
        
        trimmedTextureData.left = leftTexture;
        trimmedTextureData.bottom = bottomTexture;
        trimmedTextureData.width = rightTexture - leftTexture;
        trimmedTextureData.height = topTexture - bottomTexture;
        
        // Draw.
        [[self getShaderQuad] drawWithPosition:trimmedPositionData andTexture:trimmedTextureData andColor:colorData andBlending:blendType];
    }
}

-(MutableFrame*)getFrame
{
    return frame;
}

-(MutableCoordinate2d*)getPosition
{
    return [frame getPosition];
}

-(MutableCoordinate2d*)getScale
{
    return [frame getScale];
}

-(MutableCoordinate1d*)getRotation
{
    return [frame getRotation];
}

-(MutableCoordinate1d*)getLeftTrim
{
    return leftTrim;
}

-(MutableCoordinate1d*)getRightTrim
{
    return rightTrim;
}

-(MutableCoordinate1d*)getBottomTrim
{
    return bottomTrim;
}

-(MutableCoordinate1d*)getTopTrim
{
    return topTrim;
}

-(MutableCoordinate1d*)getRed
{
    return red;
}

-(MutableCoordinate1d*)getGreen
{
    return green;
}

-(MutableCoordinate1d*)getBlue
{
    return blue;
}

-(MutableCoordinate1d*)getAlpha
{
    return alpha;
}

-(void)setByTextureMapping:(TEXTURE_ASSET)textureAsset
{    
    [self setTextureData:[[TextureProducer sharedInstance] getTextureData:textureAsset]];
    [frame setWidth:textureData.width];
    [frame setHeight:textureData.height];
}

-(void)setShaderQuad:(ShaderQuad*)newShaderQuad
{
    shaderQuad = newShaderQuad;
}

-(ShaderQuad*)getShaderQuad
{
    ShaderQuad* currentShaderQuad = shaderQuad;
    if (currentShaderQuad == nil)
    {
        // If any of the colors are anything but the default value (1), use the colored shader, otherwise use the uncolored shader.
        if ([alpha getX] != 1.0f || [red getX] != 1.0f || [green getX] != 1.0f || [blue getX] != 1.0f)
        {
            currentShaderQuad = [ShaderQuadTexCol sharedInstance];
        }
        else
        {
            currentShaderQuad = [ShaderQuadTex sharedInstance];
        }
    }
    
    return currentShaderQuad;
}

-(void)setTextureData:(TextureData)newTextureData
{
    textureData = newTextureData;
}

-(void)setBlendType:(BLEND_TYPE)newBlendType
{
    blendType = newBlendType;
}

-(void)setActive:(BOOL)newIsActive
{
    isActive = newIsActive;
}

-(void)setRed:(float)r Green:(float)g Blue:(float)b Alpha:(float)a
{
    [red setX:r];
    [green setX:g];
    [blue setX:b];
    [alpha setX:a];
}

@end
