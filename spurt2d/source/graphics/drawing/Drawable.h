//
//  Drawable.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/24/14.
//
//

@protocol Drawable <NSObject>

/**
 Draws itself to the screen.
 */
-(void)draw;

@end
