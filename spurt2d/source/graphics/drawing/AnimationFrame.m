//
//  AnimationFrame.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/27/14.
//
//

#import "AnimationFrame.h"

@implementation AnimationFrame

-(id)init
{
    logCritical(@"Creating a nil AnimationFrame.");
    return nil;
}

-(id)initWithTextureData:(TextureData)newTextureData andWidth:(float)newWidth andHeight:(float)newHeight
{
    if (self = [super init])
    {
        textureData = newTextureData;
        
        width = newWidth;
        height = newHeight;
    }
    return self;
}

-(TextureData)getTextureData
{
    return textureData;
}

-(float)getWidth
{
    return width;
}

-(float)getHeight
{
    return height;
}

@end
