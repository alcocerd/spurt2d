//
//  Quad.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/28/13.
//
//

#import "Drawable.h"
#import "ShaderQuad.h"

#import "TextureMapping.h"

@class MutableFrame;
@class MutableCoordinate1d;
@class MutableCoordinate2d;

@interface Quad : NSObject <Drawable>
{
@private
    ShaderQuad* shaderQuad;
    
@protected
    MutableFrame* frame;
    
    MutableCoordinate1d* red;
    MutableCoordinate1d* green;
    MutableCoordinate1d* blue;
    MutableCoordinate1d* alpha;

    MutableCoordinate1d* leftTrim;
    MutableCoordinate1d* rightTrim;
    MutableCoordinate1d* bottomTrim;
    MutableCoordinate1d* topTrim;
    
    TextureData textureData;
    
    BLEND_TYPE blendType;
    
    BOOL isActive;
}

+(Quad*)quadByTextureMapping:(TEXTURE_ASSET)textureAsset;

-(MutableFrame*)getFrame;

-(MutableCoordinate2d*)getPosition;
-(MutableCoordinate2d*)getScale;
-(MutableCoordinate1d*)getRotation;

// Colors.
-(MutableCoordinate1d*)getRed;
-(MutableCoordinate1d*)getGreen;
-(MutableCoordinate1d*)getBlue;
-(MutableCoordinate1d*)getAlpha;

// Trimming.  As an example, for left a value of 1.0f means no trim (render normally), 0.0f means trim to the center from the left (the left half of the image is not rendered), -1.0f means trim to the right from the left (meaning nothing will render).
-(MutableCoordinate1d*)getLeftTrim;
-(MutableCoordinate1d*)getRightTrim;
-(MutableCoordinate1d*)getBottomTrim;
-(MutableCoordinate1d*)getTopTrim;

-(void)setByTextureMapping:(TEXTURE_ASSET)textureAsset;

/**
 This defaults to nil, allowing the Quad to decide what ShaderQuad to use internally.  A Quad will honor a valid ShaderQuad being passed in here and use it.  Pass nil to let the Quad go back to figuring out what ShaderQuad to use.
 */
-(void)setShaderQuad:(ShaderQuad*)newShaderQuad;
/**
 Returns the ShaderQuad this Quad is using.
 */
-(ShaderQuad*)getShaderQuad;

-(void)setTextureData:(TextureData)newTextureData;

-(void)setBlendType:(BLEND_TYPE)newBlendType;

-(void)setActive:(BOOL)newIsActive;

-(void)setRed:(float)r Green:(float)g Blue:(float)b Alpha:(float)a;

@end
