//
//  Animation.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/25/14.
//
//

#import "Quad.h"

@class AnimationFrame;

typedef enum
{
    // Loops.
    AE_LOOP,
    // Disappears on the last frame.
    AE_DIE,
    // Stays and holds on the last frame.
    AE_HOLD,
    // Goes back and forth.
    AE_PINGPONG,
    
} ANIMATION_ENDING;

@interface Animation : Quad
{
@private
    NSMutableArray* frames;
    
    int currentFrame;
    int direction;
    
    int loopCounter;
    
    CFTimeInterval nextFrameUpdate;
    CFTimeInterval frameInterval;
    
    ANIMATION_ENDING animationEnding;
}

-(id)initWithFrames:(NSMutableArray*)newFrames andFramesPerSecond:(CFTimeInterval)framesPerSecond andEnding:(ANIMATION_ENDING)ending;

// Frame updates.
-(void)goToFirstFrame;
-(void)goToLastFrame;
-(void)setCurrentFrame:(int)newCurrentFrame;

// Frame manipulation.
-(void)setFrames:(NSMutableArray*)newAnimationFrames;
-(void)clearFrames;
-(void)addFrame:(AnimationFrame*)animationFrame;
-(void)addFrameByTextureMapping:(TEXTURE_ASSET)textureAsset;

// Frame intervals.
-(void)setFrameRateAsTimeInterval:(CFTimeInterval)newFrameInterval;
-(void)setFrameRateAsFramesPerSecond:(CFTimeInterval)framesPerSecond;

// Endings.
-(void)setEnding:(ANIMATION_ENDING)newAnimationEnding;

// Looping controls.
-(void)loopIndefinite;
-(void)loop:(int)loops;

@end
