//
//  AnimationFrame.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/27/14.
//
//

#import "ShaderQuad.h"

@interface AnimationFrame : NSObject
{
@private
    TextureData textureData;
    
    // In case the desired width and height is different than what is in texture data.
    float width;
    float height;
}

-(id)initWithTextureData:(TextureData)newTextureData andWidth:(float)newWidth andHeight:(float)newHeight;

-(TextureData)getTextureData;

-(float)getWidth;
-(float)getHeight;

@end
