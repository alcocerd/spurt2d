//
//  Text.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/25/14.
//
//

#import "Quad.h"

#import "FontMapping.h"

@class LinkedList;

// Note that scales and rotations don't work as expected with Text.  Hopefully this will be fixed soon.
@interface Text : Quad
{
@private
    // The unaltered text of whatever was passed in.
    NSString* fullText;
    
    // Useful for multiple lines, this contains a list of them.
    LinkedList* lines;
    
    // Information about the font.
    int asciiStart;
    int characterCount;
    float uniformHeight;
    float spaceWidth;
    float horizontalPadding;
    
    // Coordinates of the characters.
    float* lefts;
    float* bottoms;
    float* widths;
    
    // For a text to wrap to a new line after a certain width.
    float wrapWidth;
    
    // If we want the alignment centered instead of to the left (TODO: Make right alignment).
    BOOL isCenterAligned;
    
    float trackingAdjustment;
    float leading;
}

// Quick allocation.
+(Text*)textByFontMapping:(FONT_ASSET)fontAsset;

/**
 Calling this will also set the width and height of this object.
 */
-(void)setText:(NSString*)text;
/**
 Int to string.
 */
-(void)setTextAsInt:(int)value;
/**
 Pass in a number and it will add commas (1000 becomes 1,000);
 */
-(void)setTextAsNumberFormattedWithCommas:(NSNumber*)number;

/**
 Pass in a number and it will return a string with commas.
 */
-(NSString*)getTextAsNumberFormattedWithCommas:(NSNumber*)number;

-(void)setByFontMapping:(FONT_ASSET)fontAsset;

/**
 Wrapping for line breaks based on a given width.  If 0, will just wrap on every word.  Does not account for different screen sizes, so in most cases you probably want to use setWrapWidthFromMaster.
 */
-(void)setWrapWidth:(float)newWrapWidth;
/**
 Same as setWrapWidth, but takes screen size into account.
 */
-(void)setWrapWidthFromMaster:(float)newWrapWidth;
/**
 To disable wrapping, call setWrapWidth to reenable.
 */
-(void)disableWrapping;

/**
 Pass true to align to the center, false to align to the left.
 */
-(void)setIsCenterAligned:(BOOL)newIsCenterAligned;

/**
 A value of 0 uses the default distance between characters.  Negative values decreases the distance between characters, positive increases.  Takes screen size into account.
 */
-(void)setTrackingAdjustmentFromMaster:(float)newTrackingAdjustment;

/**
 The distance from the top of one line, to the top of the next (not the way leading should work, play with the numbers to get a feel).  Takes screen size into account.
 */
-(void)setLeadingFromMaster:(float)newLeading;

@end
