//
//  Animation.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/25/14.
//
//

#import "Animation.h"

#import "AnimationFrame.h"
#import "MutableFrame.h"

#import "Clock.h"
#import "TextureProducer.h"

const int LOOP_INDEFINITE = -1;

@interface Animation ()

-(void)updateFrame;
-(void)resetFrameIntervalFromNow;

@end

@implementation Animation

#pragma mark - Construction

-(id)init
{
    return [self initWithFrames:[[NSMutableArray alloc] init] andFramesPerSecond:1.0 andEnding:AE_LOOP];
}

-(id)initWithFrames:(NSMutableArray*)newFrames andFramesPerSecond:(CFTimeInterval)framesPerSecond andEnding:(ANIMATION_ENDING)ending
{
    if (self = [super init])
    {
        [self setFrames:frames];
        [self setFrameRateAsFramesPerSecond:framesPerSecond];
        [self setEnding:ending];
    }
    return self;
}

#pragma mark - Draw

-(void)draw
{
    [self updateFrame];
    
    [super draw];
}

#pragma mark - Frame updates

-(void)updateFrame
{
    CFTimeInterval now = [Clock getElapsedTime];
    while (now >= nextFrameUpdate)
    {
        nextFrameUpdate += frameInterval;
        
        int nextFrame = currentFrame + direction;
        int frameCount = [frames count];
        
        switch (animationEnding)
        {
            case AE_LOOP:
            {
                // If we passed our frame count, go back to the first frame.
                if (nextFrame >= frameCount)
                {
                    // Go back as long we are looping indefinitely or if our counter has loops left.
                    if (loopCounter == LOOP_INDEFINITE || loopCounter > 0)
                    {
                        nextFrame = 0;
                    }
                    // Keep decrementing this as long as there are loops left.
                    if (loopCounter > 0)
                    {
                        loopCounter--;
                    }
                }
                break;
            }
            case AE_DIE:
            {
                if (nextFrame >= frameCount)
                {
                    nextFrame = frameCount - 1;
                    [self setActive:false];
                }
                break;
            }
            case AE_HOLD:
            {
                
                break;
            }
            case AE_PINGPONG:
            {
                if (nextFrame >= frameCount)
                {
                    direction = -1;
                    nextFrame = frameCount - 2;
                }
                else if (nextFrame < 0)
                {
                    direction = 1;
                    nextFrame = 1;
                }
                break;
            }
            default:
            {
                logCritical(@"No animation ending found in %@.", NSStringFromSelector(_cmd));
                break;
            }
        }
        
        [self setCurrentFrame:nextFrame];
    }
}

-(void)goToFirstFrame
{
    [self setCurrentFrame:0];
    [self resetFrameIntervalFromNow];
}

-(void)goToLastFrame
{
    [self setCurrentFrame:[frames count] - 1];
    [self resetFrameIntervalFromNow];
}

-(void)setCurrentFrame:(int)newCurrentFrame
{
    if (newCurrentFrame >= 0 && newCurrentFrame < [frames count])
    {
        currentFrame = newCurrentFrame;
        
        AnimationFrame* animationFrame = [frames objectAtIndex:currentFrame];
        [self setTextureData:[animationFrame getTextureData]];
        
        [frame setWidth:[animationFrame getWidth]];
        [frame setHeight:[animationFrame getHeight]];
    }
}

#pragma mark - Frame manipulation

-(void)setFrames:(NSMutableArray*)newAnimationFrames
{
    frames = newAnimationFrames;
    
    // Refresh this.
    [self loopIndefinite];
    
    [self goToFirstFrame];
}

-(void)clearFrames
{
    [frames removeAllObjects];
}

-(void)addFrame:(AnimationFrame*)animationFrame
{
    [frames addObject:animationFrame];
    
    // If this is the first frame being added.
    if ([frames count] == 1)
        [self goToFirstFrame];
}

-(void)addFrameByTextureMapping:(TEXTURE_ASSET)textureAsset
{
    AnimationFrame* animationFrame = [[AnimationFrame alloc] initWithTextureData:[[TextureProducer sharedInstance] getTextureData:textureAsset] andWidth:[TextureMapping getWidth:textureAsset] andHeight:[TextureMapping getHeight:textureAsset]];
    
    [self addFrame:animationFrame];
}

#pragma mark - Frame rate

-(void)setFrameRateAsTimeInterval:(CFTimeInterval)newFrameInterval
{
    frameInterval = newFrameInterval;
    [self resetFrameIntervalFromNow];
}

-(void)setFrameRateAsFramesPerSecond:(CFTimeInterval)framesPerSecond
{
    [self setFrameRateAsTimeInterval:1.0 / framesPerSecond];
}

// Useful when changing the frame rate or frames, it will ensure that the first update after these changes will occur at the right time.
-(void)resetFrameIntervalFromNow
{
    nextFrameUpdate = [Clock getElapsedTime] + frameInterval;
}

#pragma mark - Animation ending

-(void)setEnding:(ANIMATION_ENDING)newAnimationEnding
{
    animationEnding = newAnimationEnding;
    direction = 1;
}

#pragma mark - Looping

-(void)loopIndefinite
{
    loopCounter = LOOP_INDEFINITE;
}

-(void)loop:(int)loops
{
    loopCounter = loops;
}

@end
