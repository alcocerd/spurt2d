//
//  Text.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/25/14.
//
//

#import "Text.h"

#import "ScreenOrientator.h"
#import "MutableFrame.h"
#import "MutableCoordinate1d.h"
#import "LinkedList.h"
#import "Math.h"

#import <GLKit/GLKit.h>

const float NO_WRAP = -1;

const int ASCII_NOT_FOUND = -1;

// Use \n to make new lines.
const int NEWLINE = 10;
const int SPACE = 32;

@interface Text ()

-(void)handleLineBreaks;

/**
 Returns the ascii value (adjusted for the ascii start value) at the index of the given string.
 */
-(int)getAdjustedAscii:(NSString*)text AtIndex:(int)index;
/**
 Returns the width of a text, assuming no line breaks (it does not check for new lines).
 */
-(float)getWidthOfText:(NSString*)text;
/**
 Figures out if a line should break either by \n or wrapping.
 */
-(BOOL)shouldLineBreak:(int)startIndex withCurrentWidth:(float)currentWidth;

-(BOOL)isNewLine:(int)ascii;
-(BOOL)isSpace:(int)ascii;
/**
 Returns true if the ascii is within this font's range.
 */
-(BOOL)isAsciiValid:(int)ascii;

// For getting coordinates of an ascii value.
-(float)getLeft:(int)ascii;
-(float)getBottom:(int)ascii;
-(float)getWidth:(int)ascii;

@end

@implementation Text

#pragma mark - Construction

-(id)init
{
    if (self = [super init])
    {
        fullText = @"";
        
        lines = [[LinkedList alloc] init];
        
        characterCount = 0;
        
        wrapWidth = NO_WRAP;
        
        [self setIsCenterAligned:false];
        
        trackingAdjustment = 0.0f;
        leading = 0.0f;
    }
    return self;
}

+(Text*)textByFontMapping:(FONT_ASSET)fontAsset;
{
    Text* text = [[Text alloc] init];
    [text setByFontMapping:fontAsset];
    
    return text;
}

#pragma mark - Draw

-(void)draw
{
    // Draw if we have text to print and if we are not cullable.
    if (isActive && [lines getHead] != nil && ![frame isCullable])
    {
        // Model matrix.
        GLKMatrix4 modelMatrix = GLKMatrix4Identity;
        
        // TODO: Rotations and scales don't work too well (they don't work as one would expect), some work here can easily remedy that.
        
        // Rotate
        float rotation = [frame getRotationAsRadians];
        modelMatrix = GLKMatrix4RotateZ(modelMatrix, rotation);
        
        // Scale.
        float horizontalScale = [frame getHorizontalScale];
        float verticalScale = [frame getVerticalScale];
        modelMatrix = GLKMatrix4ScaleWithVector4(modelMatrix, GLKVector4Make(horizontalScale, verticalScale, 0.0f, 1.0f));
        
        float currentHeight = 0.0f;
        
        float leftSide = [frame getHorizontalCenterAsAbsolute] - [frame getHalfWidth];
        float topSide = [frame getVerticalCenterAsAbsolute] + [frame getHalfHeight];
        
        // Go through all of our lines.
        LinkedNode* currentLine = [lines getHead];
        do
        {
            // Current line.
            NSString* textLine = (NSString*)[currentLine getValue];
            float currentWidth = 0.0f;
            
            // Handle center alignments if applicable.
            if (isCenterAligned)
            {
                currentWidth = ([frame getWidth] - [self getWidthOfText:textLine]) / 2.0f;
            }
            
            // Go through each character in this line.
            for (int i = 0; i < [textLine length]; i++)
            {
                // Get the ascii value of this character.
                int ascii = [self getAdjustedAscii:textLine AtIndex:i];
                // Width of this character.
                float width = [self getWidth:ascii];
                
                // Only render ascii values we have textures for.
                if ([self isAsciiValid:ascii])
                {
                    // Coordinate data.
                    float left = [self getLeft:ascii];
                    float bottom = [self getBottom:ascii];
                    
                    // The origin is the top left for Text to make things easier.
                    float positions[8] = {0.0f-horizontalPadding, -uniformHeight, width+horizontalPadding, -uniformHeight, 0.0f-horizontalPadding, 0.0f, width+horizontalPadding, 0.0f};
                    
                    float newPositions[8];
                    float xPosition = leftSide + currentWidth;
                    float yPosition = topSide + currentHeight;
                    for (int i = 0; i < 8; i += 2)
                    {
                        GLKVector4 positionVector = GLKVector4Make(positions[i], positions[i+1], 0, 1);
                        
                        positionVector = GLKMatrix4MultiplyVector4(modelMatrix, positionVector);
                        
                        // Translate
                        positionVector = GLKVector4Add(positionVector, GLKVector4Make(xPosition, yPosition, 0, 0));
                        
                        positionVector = [[ScreenOrientator sharedInstance] multiplyByProjectionMatrix:positionVector];
                        
                        newPositions[i] = positionVector.x;
                        newPositions[i+1] = positionVector.y;
                    }
                    
                    PositionData positionData = {newPositions[0], newPositions[1], newPositions[2], newPositions[3], newPositions[4], newPositions[5], newPositions[6], newPositions[7]};
                    
                    ColorData colorData = {[red getX], [green getX], [blue getX], [alpha getX]};
                    
                    TextureData characterTextureData = textureData;
                    characterTextureData.left += left - horizontalPadding;
                    characterTextureData.bottom += bottom;
                    characterTextureData.width = width + (horizontalPadding * 2.0f);
                    characterTextureData.height = uniformHeight;
                    
                    // Text does not support trimming yet.
                    // Draw.
                    [[self getShaderQuad] drawWithPosition:positionData andTexture:characterTextureData andColor:colorData andBlending:blendType];
                }
                // Accumulate to position correctly.
                currentWidth += width + trackingAdjustment;
            }
            // Accumulate to position correctly.
            currentHeight -= leading;
        }
        while ((currentLine = [currentLine getNext]));
    }
}

#pragma mark - Mutators

-(void)setText:(NSString*)text
{
    fullText = text;
    [self handleLineBreaks];
}

-(void)setTextAsInt:(int)value
{
    [self setText:[NSString stringWithFormat:@"%d", value]];
}

-(void)setTextAsNumberFormattedWithCommas:(NSNumber*)number
{
	[self setText:[self getTextAsNumberFormattedWithCommas:number]];
}

-(NSString*)getTextAsNumberFormattedWithCommas:(NSNumber*)number
{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setGroupingSeparator:@","];
    NSString* numberWithCommas = [numberFormatter stringForObjectValue:number];
    
	return numberWithCommas;
}

-(void)setByFontMapping:(FONT_ASSET)fontAsset
{
    TEXTURE_ASSET textureAsset = [FontMapping getTextureAsset:fontAsset];
    [self setByTextureMapping:textureAsset];
    
    asciiStart = [FontMapping getAsciiStart:fontAsset];
    characterCount = [FontMapping getCharacterCount:fontAsset];
    uniformHeight = [FontMapping getHeight:fontAsset];
    spaceWidth = [FontMapping getSpaceWidth:fontAsset];
    horizontalPadding = [FontMapping getHorizontalPadding:fontAsset];
    
    lefts = [FontMapping getLeft:fontAsset];
    bottoms = [FontMapping getBottom:fontAsset];
    widths = [FontMapping getWidth:fontAsset];
    
    leading = uniformHeight;
}

#pragma mark - Wrapping

-(void)setWrapWidth:(float)newWrapWidth
{
    wrapWidth = newWrapWidth;
}

-(void)setWrapWidthFromMaster:(float)newWrapWidth
{
    [self setWrapWidth:[[ScreenOrientator sharedInstance] fromMaster:newWrapWidth]];
}

-(void)disableWrapping
{
    wrapWidth = NO_WRAP;
}

#pragma mark - Alignment

-(void)setIsCenterAligned:(BOOL)newIsCenterAligned
{
    isCenterAligned = newIsCenterAligned;
}

#pragma mark - Tracking

-(void)setTrackingAdjustmentFromMaster:(float)newTrackingAdjustment
{
    trackingAdjustment = [[ScreenOrientator sharedInstance] fromMaster:newTrackingAdjustment];
}

#pragma mark - Leading

-(void)setLeadingFromMaster:(float)newLeading
{
    leading = [[ScreenOrientator sharedInstance] fromMaster:newLeading];
}

#pragma mark - Utility

-(void)handleLineBreaks
{
    // Create a new linked list.
    lines = [[LinkedList alloc] init];
    
    int currentLineStartIndex = 0;
    int currentLength = 0;
    
    float longestWidth = 0.0f;
    float currentWidth = 0.0f;
    float totalHeight = 0.0f;
    
    // Go through each character in the text.
    for (int i = 0; i < [fullText length]; i++)
    {
        // Grab the ascii value of this character.
        int ascii = [self getAdjustedAscii:fullText AtIndex:i];
        currentWidth += [self getWidth:ascii];
        currentLength++;
        
        // Check if we should line break or if we are at the last character.
        BOOL shouldLineBreak = [self shouldLineBreak:i withCurrentWidth:currentWidth];
        if (shouldLineBreak || i == [fullText length] - 1)
        {
            // If we had a break, remove the last character.
            if (shouldLineBreak)
            {
                currentWidth -= [self getWidth:ascii];
                currentLength--;
            }
            
            // Create the new line we just went through and add it to our list.
            NSString* line = [fullText substringWithRange:NSMakeRange(currentLineStartIndex, currentLength)];
            [lines addNode:line];
            
            // Set the next line's starting index.
            currentLineStartIndex = i + 1;
            
            // Keep track of our longest width since this will be our frame's new width.
            longestWidth = [Math maxFloat:currentWidth :longestWidth];
            // Total up the height for the frame's height as well.
            totalHeight += uniformHeight;
            
            // Resets for the next line.
            currentLength = 0;
            currentWidth = 0.0f;
        }
    }
    
    // Update the frame.
    [frame setWidth:longestWidth];
    [frame setHeight:totalHeight];
}

-(int)getAdjustedAscii:(NSString*)text AtIndex:(int)index
{
    // Gets the ascii value of a character in the given text, adjusted for this font's start point (offset).
    int ascii = ASCII_NOT_FOUND;
    if (index >= 0 && index < [text length])
    {
        ascii = ((int)[text characterAtIndex:index]) - asciiStart;
    }
    
    return ascii;
}

-(float)getWidthOfText:(NSString*)text
{
    // Just returns the total width of a given text, assuming it's one line.
    float totalWidth = 0.0f;
    for (int i = 0; i < [text length]; i++)
    {
        int ascii = [self getAdjustedAscii:text AtIndex:i];
        totalWidth += [self getWidth:ascii];
    }
    
    return totalWidth;
}

-(BOOL)shouldLineBreak:(int)startIndex withCurrentWidth:(float)currentWidth
{
    BOOL shouldLineBreak = false;
    int ascii = [self getAdjustedAscii:fullText AtIndex:startIndex];
    
    // If it's a new line character, done.
    if ([self isNewLine:ascii])
    {
        shouldLineBreak = true;
    }
    else
    {
        // Only check if we should line break via wrapping if we have an actual wrapping width and if we are currently on a space character.
        if (wrapWidth >= 0.0f && [self isSpace:ascii])
        {
            // Get the width of the next word.
            float wordWidth = 0.0f;
            // This boolean allows us to skip extra white space before the next word.
            BOOL foundNonWhiteSpace = false;
            for (int i = startIndex; i < [fullText length]; i++)
            {
                int ascii = [self getAdjustedAscii:fullText AtIndex:i];
                if ([self isSpace:ascii] && foundNonWhiteSpace)
                {
                    break;
                }
                else
                {
                    foundNonWhiteSpace = true;
                }
                
                // Total up the word width.
                wordWidth += [self getWidth:ascii];
            }
            
            // Now see if we went over our wrap width.
            if (currentWidth + wordWidth > wrapWidth)
            {
                shouldLineBreak = true;
            }
        }
    }
    
    return shouldLineBreak;
}

-(BOOL)isNewLine:(int)ascii
{
    return ascii == NEWLINE - asciiStart;
}

-(BOOL)isSpace:(int)ascii
{
    return ascii == SPACE - asciiStart;
}

-(BOOL)isAsciiValid:(int)ascii
{
    return ascii >= 0 && ascii < characterCount;
}

-(float)getLeft:(int)ascii
{
    float value = 0.0f;
    if ([self isAsciiValid:ascii])
        value = lefts[ascii];
    
    return value;
}

-(float)getBottom:(int)ascii
{
    float value = 0.0f;
    if ([self isAsciiValid:ascii])
        value = bottoms[ascii];
    
    return value;
}

-(float)getWidth:(int)ascii
{
    float value = 0.0f;
    if ([self isAsciiValid:ascii])
        value = widths[ascii];
    // Special case.
    else if ([self isSpace:ascii])
        value = spaceWidth;
    
    return value;
}

@end
