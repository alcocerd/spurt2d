//
//  ShaderProgram.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/17/13.
//
//

#import "ShaderProgram.h"

#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES2/gl.h>

// Duplication of functions from gl.h file.  These are just helper functions.
// Helper for glGetProgramiv and glGetShaderiv.
typedef void (*GLGetLogLength) (GLuint program, GLenum pname, GLint* params);
// Helper for glGetProgramInfoLog and glGetShaderInfoLog.
typedef void (*GLGetLog) (GLuint program, GLsizei bufsize, GLsizei* length, GLchar* infolog);

@interface ShaderProgram ()
{
    GLuint programHandle;
}

-(GLuint)buildVertexShader:(NSString*)vertexShaderName andFragmentShader:(NSString*)fragmentShaderName;
-(GLuint)compileShader:(NSString*)shaderName withType:(GLenum)shaderType;

-(void)exitApplicationAndLogError:(NSString*)baseMessage withHandle:(GLuint)handle andLengthFunction:(GLGetLogLength)lengthFunction andLogFunction:(GLGetLog)logfunction;
-(NSString*)getLog:(GLuint)handle withLengthFunction:(GLGetLogLength)lengthFunction andLogFunction:(GLGetLog)logfunction;

-(void)exitApplication;

@end

@implementation ShaderProgram

-(id)init
{
    logCritical(@"Creating a nil ShaderProgram.");
    return nil;
}

-(id)initWithVertexShader:(NSString*)vertexShaderName andFragmentShader:(NSString*)fragmentShaderName
{
    if (self = [super init])
    {
        programHandle = [self buildVertexShader:vertexShaderName andFragmentShader:fragmentShaderName];
    }
    
    return self;
}

#pragma mark - Shader building

-(GLuint)buildVertexShader:(NSString*)vertexShaderName andFragmentShader:(NSString*)fragmentShaderName
{
    // Compile our vertex and fragment shaders.
    GLuint vertexShader = [self compileShader:vertexShaderName withType:GL_VERTEX_SHADER];
    GLuint fragmentShader = [self compileShader:fragmentShaderName withType:GL_FRAGMENT_SHADER];
    
    // Create a program, attach the shaders we just compiled to it.
    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    
    // Link and validate the program.
    glLinkProgram(program);
    glValidateProgram(program);
    
    // If we succeeded, do nothing, otherwise something terrible went wrong.
    GLint linkSucceeded;
    glGetProgramiv(program, GL_LINK_STATUS, &linkSucceeded);
    if (linkSucceeded == GL_FALSE)
    {
        [self exitApplicationAndLogError:@"Error linking program." withHandle:program andLengthFunction:&glGetProgramiv andLogFunction:&glGetProgramInfoLog];
    }
    
    // We are done with these now that we have linked up our program.
    if (vertexShader)
    {
        glDetachShader(program, vertexShader);
        glDeleteShader(vertexShader);
    }
    if (fragmentShader)
    {
        glDetachShader(program, fragmentShader);
        glDeleteShader(fragmentShader);
    }
    
    return program;
}

-(GLuint)compileShader:(NSString*)shaderName withType:(GLenum)shaderType
{
    // Split up the name and the extension.
    NSString* fileName = [shaderName stringByDeletingPathExtension];
    NSString* extension = [shaderName pathExtension];
    
    // Get the contents of the shader before we send it off to OpenGL.
    NSString* shaderPath = [[NSBundle mainBundle] pathForResource:fileName ofType:extension];
    NSError* error;
    NSString* shaderString = [NSString stringWithContentsOfFile:shaderPath encoding:NSUTF8StringEncoding error:&error];
    if (!shaderString)
    {
        logCritical(@"Error reading shader contents: %@", error.localizedDescription);
        [self exitApplication];
    }
    
    // Now convert it to a const char* and compile that sucker.
    const GLchar* shaderSource = [shaderString UTF8String];
    GLuint shaderHandle = glCreateShader(shaderType);
    glShaderSource(shaderHandle, 1, &shaderSource, NULL);
    glCompileShader(shaderHandle);
    
    // If we succeeded, do nothing, otherwise something terrible happened.
    GLint compileSucceeded;
    glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSucceeded);
    if (compileSucceeded == GL_FALSE)
    {
        [self exitApplicationAndLogError:@"Error compiling shader." withHandle:shaderHandle andLengthFunction:&glGetShaderiv andLogFunction:&glGetShaderInfoLog];
    }
    
    return shaderHandle;
}

#pragma mark - Activating the program

-(void)use
{
    glUseProgram(programHandle);
}

#pragma mark - Attributes and uniforms

-(int)getAttribute:(NSString*)attributeName
{
    return glGetAttribLocation(programHandle, [attributeName UTF8String]);
}

-(int)getUniform:(NSString*)uniformName
{
    return glGetUniformLocation(programHandle, [uniformName UTF8String]);
}

#pragma mark - Logging

-(void)exitApplicationAndLogError:(NSString*)baseMessage withHandle:(GLuint)handle andLengthFunction:(GLGetLogLength)lengthFunction andLogFunction:(GLGetLog)logfunction
{
    // Print out whatever is in the logs and quit the program.
    NSString* log = [self getLog:handle withLengthFunction:lengthFunction andLogFunction:logfunction];
    logCritical(@"Shader program issue: %@\nLog: %@", baseMessage, log);
    
    [self exitApplication];
}

-(NSString*)getLog:(GLuint)handle withLengthFunction:(GLGetLogLength)lengthFunction andLogFunction:(GLGetLog)logfunction
{
    NSString* log = @"";
    
    GLint logLength = 0;
    lengthFunction(handle, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength >= 1)
    {
        char* logBytes = malloc(logLength);
        GLint logChars = 0;
        // Grab the log from OpenGL.
        logfunction(handle, logLength, &logChars, logBytes);
        log = [[NSString alloc] initWithBytes:logBytes length:logLength encoding:NSUTF8StringEncoding];
        free(logBytes);
    }
    
    return log;
}

#pragma mark - Quit

-(void)exitApplication
{
    // TODO: Look into other ways of handling shader errors instead of quitting the program.  Maybe load a default shader and go from there, but is that worth the work?  What are the benefits?
    exit(1);
    
}

-(void)dealloc
{
    if (programHandle)
        glDeleteProgram(programHandle);
}

@end
