//
//  ShaderQuad.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/29/13.
//
//

#import "ShaderQuad.h"

#import "ShaderProgram.h"

#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES2/gl.h>

#import "Math.h"

@interface ShaderQuad ()

-(void)addQuadData:(PositionData)positionData :(TextureData)textureData :(ColorData)colorData;

@end

@implementation ShaderQuad

// Since we use GL_TRIANGLES to draw, we need 6 vertices instead of 4 (4 would work with GL_TRIANGLE_STRIP or GL_TRIANGLE_FAN).
const int VERTICES = 6;

// XY (if 3d, would be 3 for x, y, and z).
const int POSITION_VALUES_PER_VERTEX = 2;
// XY.
const int TEXTURE_VALUES_PER_VERTEX = 2;
// RGBA.
const int COLOR_VALUES_PER_VERTEX = 4;

-(id)initWithVertexShader:(NSString*)vertexShaderName andFragmentShader:(NSString*)fragmentShaderName andPositionName:(NSString*)positionName andTextureName:(NSString*)textureName andColorName:(NSString*)colorName andUsesTextures:(BOOL)newUseTextures andUsesColors:(BOOL)newUseColors andMaxVerticesForBatch:(int)newMaxVerticesForBatch
{
    if (self = [super initWithVertexShader:vertexShaderName andFragmentShader:fragmentShaderName])
    {
        // Defaults for attributes and batch arrays.
        positions = NULL;
        textures = NULL;
        colors = NULL;
        positionLocation = 0;
        textureLocation = 0;
        colorLocation = 0;
        
        // Batch indices.
        vertexBatchCount = 0;
        positionBatchCount = 0;
        textureBatchCount = 0;
        colorBatchCount = 0;
        
        // Optionals.
        useTextures = newUseTextures;
        useColors = newUseColors;
        
        // Minimum batch is one quad.
        maxVerticesForBatch = [Math maxInt:newMaxVerticesForBatch :VERTICES];
        
        // Attributes and batch arrays.
        positionLocation = [shaderProgram getAttribute:positionName];
        positions = (float*) calloc(maxVerticesForBatch * POSITION_VALUES_PER_VERTEX, sizeof(float));
        if (useTextures)
        {
            textureLocation = [shaderProgram getAttribute:textureName];
            textures = (float*) calloc(maxVerticesForBatch * TEXTURE_VALUES_PER_VERTEX, sizeof(float));
        }
        if (useColors)
        {
            colorLocation = [shaderProgram getAttribute:colorName];
            colors = (float*) calloc(maxVerticesForBatch * COLOR_VALUES_PER_VERTEX, sizeof(float));
        }
    }
    return self;
}

-(void)drawWithPosition:(PositionData)positionData andTexture:(TextureData)textureData andColor:(ColorData)colorData andBlending:(BLEND_TYPE)blendType
{
    [Shader updateShader:self];
    if (useTextures)
        [Shader updateTextureName:textureData.name withShader:self];
    [Shader updateBlendType:blendType withShader:self];
    
    [self addQuadData:positionData :textureData :colorData];
}

-(void)activate
{
    [super activate];
    
    glEnableVertexAttribArray(positionLocation);
    if (useTextures)
        glEnableVertexAttribArray(textureLocation);
    if (useColors)
        glEnableVertexAttribArray(colorLocation);
}

-(void)deactivate
{
    [super deactivate];
    
    glDisableVertexAttribArray(positionLocation);
    if (useTextures)
        glDisableVertexAttribArray(textureLocation);
    if (useColors)
        glDisableVertexAttribArray(colorLocation);
}

-(void)finishBatch
{
    if (vertexBatchCount > 0)
    {
        // Give OpenGL the data we accumulated.
        glVertexAttribPointer(positionLocation, POSITION_VALUES_PER_VERTEX, GL_FLOAT, GL_FALSE, 0, positions);
        if (useTextures)
            glVertexAttribPointer(textureLocation, TEXTURE_VALUES_PER_VERTEX, GL_FLOAT, GL_FALSE, 0, textures);
        if (useColors)
            glVertexAttribPointer(colorLocation, COLOR_VALUES_PER_VERTEX, GL_FLOAT, GL_FALSE, 0, colors);
        
        // Draw the batch.
        glDrawArrays(GL_TRIANGLES, 0, vertexBatchCount);
        
        // Clear this batch for the next batch.
        vertexBatchCount = 0;
        positionBatchCount = 0;
        textureBatchCount = 0;
        colorBatchCount = 0;
    }
}

-(void)addQuadData:(PositionData)positionData :(TextureData)textureData :(ColorData)colorData
{
    // If we will exceed our max by adding this Quad, finish the batch first.
    if (vertexBatchCount + VERTICES > maxVerticesForBatch)
    {
        [self finishBatch];
    }
    
    vertexBatchCount += VERTICES;
    
    // POSITION.
    int index = positionBatchCount;
    // Bottom left.
    positions[index] = positionData.xBotLeft;
    positions[index+1] = positionData.yBotLeft;
    
    // Bottom right.
    positions[index+2] = positionData.xBotRight;
    positions[index+3] = positionData.yBotRight;
    
    // Top left.
    positions[index+4] = positionData.xTopLeft;
    positions[index+5] = positionData.yTopLeft;
    
    // Top left.
    positions[index+6] = positionData.xTopLeft;
    positions[index+7] = positionData.yTopLeft;
    
    // Bottom right.
    positions[index+8] = positionData.xBotRight;
    positions[index+9] = positionData.yBotRight;
    
    // Top right.
    positions[index+10] = positionData.xTopRight;
    positions[index+11] = positionData.yTopRight;
    
    positionBatchCount += VERTICES * POSITION_VALUES_PER_VERTEX;
    
    // TEXTURE.
    // Texture data is optional for subclasses.
    if (useTextures)
    {
        // Get the 4 sides of the texture.
        float left = textureData.left;
        float bottom = textureData.bottom;
        float right = left + textureData.width;
        float top = bottom + textureData.height;
        
        // Perform half pixel correction.
        left += 0.5f;
        bottom += 0.5f;
        right -= 0.5f;
        top -= 0.5f;
        
        // Now normalize the data between 0 and 1.
        left /= textureData.assetWidth;
        bottom /=  textureData.assetHeight;
        right /= textureData.assetWidth;
        top /= textureData.assetHeight;
        
        index = textureBatchCount;
        // Bottom left.
        textures[index] = left;
        textures[index+1] = bottom;
        
        // Bottom right.
        textures[index+2] = right;
        textures[index+3] = bottom;
        
        // Top left.
        textures[index+4] = left;
        textures[index+5] = top;
        
        // Top left.
        textures[index+6] = left;
        textures[index+7] = top;
        
        // Bottom right.
        textures[index+8] = right;
        textures[index+9] = bottom;
        
        // Top right.
        textures[index+10] = right;
        textures[index+11] = top;
        
        textureBatchCount += VERTICES * TEXTURE_VALUES_PER_VERTEX;
    }
    
    // COLOR.
    // Color data is optional for subclasses.
    if (useColors)
    {
        // Currently only supporting 1 color for the whole Quad.
        index = colorBatchCount;
        for (int i = 0; i < VERTICES; i++)
        {
            colors[index] = colorData.red;
            colors[index+1] = colorData.green;
            colors[index+2] = colorData.blue;
            colors[index+3] = colorData.alpha;
            
            index += COLOR_VALUES_PER_VERTEX;
        }
        
        colorBatchCount += VERTICES * COLOR_VALUES_PER_VERTEX;
    }
}

-(void)dealloc
{
    free(positions);
    free(textures);
    free(colors);
    
    positions = NULL;
    textures = NULL;
    colors = NULL;
}

@end
