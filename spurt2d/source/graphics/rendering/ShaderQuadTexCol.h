//
//  ShaderQuadTexCol.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/29/13.
//
//

#import "ShaderQuad.h"

@interface ShaderQuadTexCol : ShaderQuad

+(ShaderQuadTexCol*)sharedInstance;

@end
