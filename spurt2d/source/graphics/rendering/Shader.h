//
//  Shader.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/29/13.
//
//

typedef enum
{
    BT_DEFAULT,
    // Looks similar to default, but gives better looking transparency when programmatically setting the alpha value.  Great for background images that are a solid color that need a controlled transparency.
    BT_MANUAL_TRANSPARENT,
    BT_ERASE,
    BT_ADD,
    BT_MULTIPLY,
    BT_GLOW,
    BT_SCREEN,
    
} BLEND_TYPE;

@class ShaderProgram;

@interface Shader : NSObject
{
@protected
    ShaderProgram* shaderProgram;
}

-(id)initWithVertexShader:(NSString*)vertexShaderName andFragmentShader:(NSString*)fragmentShaderName;

// Mainly for subclasses to overwrite if necessary.
-(void)activate;
-(void)deactivate;
-(void)finishBatch;

// For Renderer to finish up a frame.
+(void)finishFrame;

// Mainly for sublcass use.
+(BOOL)updateShader:(Shader*)newShader;
+(BOOL)updateTextureName:(int)newTextureName withShader:(Shader*)shader;
+(BOOL)updateBlendType:(BLEND_TYPE)newBlendType withShader:(Shader*)shader;

@end
