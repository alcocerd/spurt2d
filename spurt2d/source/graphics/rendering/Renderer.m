//
//  Renderer.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/24/13.
//
//

#import "Renderer.h"

#import "Shader.h"

@implementation Renderer

-(id)init
{
    if (self = [super init])
    {
        // TODO: Make this get it from Shader, or make it more robust somehow.
        // Default blend type.
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);
        
        glClearColor(0.0f, 0.f, 0.0f, 1.0f);
        glDisable(GL_DEPTH_TEST);
    }
    return self;
}

-(void)startFrame
{
    // Clear the screen for a new frame.
    glClear(GL_COLOR_BUFFER_BIT);
}

-(void)finishFrame:(EAGLContext*)context
{
    [Shader finishFrame];
    
    // Swap buffers to draw the whole frame.
    [context presentRenderbuffer:GL_RENDERBUFFER];
}

@end
