//
//  ShaderQuadTexCol.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/29/13.
//
//

#import "ShaderQuadTexCol.h"

@interface ShaderQuadTexCol ()

-(id)initShaderQuadTexCol;

@end

@implementation ShaderQuadTexCol

-(id)initShaderQuadTexCol
{
    if (self = [super initWithVertexShader:@"QuadTextureColor.vsh" andFragmentShader:@"QuadTextureColor.fsh" andPositionName:@"position" andTextureName:@"textureCoordinate" andColorName:@"color" andUsesTextures:true andUsesColors:true andMaxVerticesForBatch:500*VERTICES])
    {
        
    }
    return self;
}

+(ShaderQuadTexCol*)sharedInstance
{
    static ShaderQuadTexCol* shaderQuadTexCol;
    static dispatch_once_t once;
    dispatch_once(&once, ^
    {
        shaderQuadTexCol = [[ShaderQuadTexCol alloc] initShaderQuadTexCol];
    });
    
    return shaderQuadTexCol;
}

@end
