//
//  Shader.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/29/13.
//
//

#import "Shader.h"

#import "ShaderProgram.h"

#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES2/gl.h>

@implementation Shader

static Shader* currentShader = nil;
static int currentTextureName = -1;
static BLEND_TYPE currentBlendType = BT_DEFAULT;

-(id)init
{
    logCritical(@"Creating a nil Shader.");
    return nil;
}

-(id)initWithVertexShader:(NSString*)vertexShaderName andFragmentShader:(NSString*)fragmentShaderName
{
    if (self = [super init])
    {
        shaderProgram = [[ShaderProgram alloc] initWithVertexShader:vertexShaderName andFragmentShader:fragmentShaderName];
    }
    return self;
}

-(void)activate
{
    [shaderProgram use];
}

-(void)deactivate
{
    // A Shader can clean up here, like disable attributes, etc.
}

-(void)finishBatch
{
    logWarning(@"%@, a Shader did not implement this method", NSStringFromSelector(_cmd));
}

+(void)finishFrame
{
    [currentShader finishBatch];
}

+(BOOL)updateShader:(Shader*)newShader
{
    BOOL didShaderChange = currentShader != newShader;
    
    if (didShaderChange)
    {
        [currentShader finishBatch];
        [currentShader deactivate];
        [newShader activate];
    }
    
    currentShader = newShader;
    
    return didShaderChange;
}

+(BOOL)updateTextureName:(int)newTextureName withShader:(Shader*)shader
{
    BOOL didTextureNameChange = currentTextureName != newTextureName;
    
    if (didTextureNameChange)
    {
        [shader finishBatch];
        glBindTexture(GL_TEXTURE_2D, newTextureName);
    }
    
    currentTextureName = newTextureName;
    
    return didTextureNameChange;
}

+(BOOL)updateBlendType:(BLEND_TYPE)newBlendType withShader:(Shader*)shader
{
    BOOL didBlendTypeChange = currentBlendType != newBlendType;
    
    if (didBlendTypeChange)
    {
        [shader finishBatch];
        
        switch (newBlendType)
        {
            case BT_DEFAULT:
            {
                glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
                break;
            }
            case BT_MANUAL_TRANSPARENT:
            {
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                break;
            }
            case BT_ERASE:
            {
                glBlendFunc(GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);
                break;
            }
            case BT_ADD:
            {
                glBlendFunc(GL_ONE, GL_ONE);
                break;
            }
            case BT_MULTIPLY:
            {
                glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
                break;
            }
            case BT_GLOW:
            {
                glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
                break;
            }
            case BT_SCREEN:
            {
                glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_COLOR);
                break;
            }
            default:
            {
                logCritical(@"Error in %@, no blend type found, value is: %d", NSStringFromSelector(_cmd), newBlendType);
                glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
                break;
            }
        }
    }
    
    currentBlendType = newBlendType;
    
    return currentBlendType;
}

@end
