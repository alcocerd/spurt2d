//
//  Renderer.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/24/13.
//
//

#import <GLKit/GLKit.h>

@interface Renderer : NSObject
{
    
}

-(void)startFrame;

-(void)finishFrame:(EAGLContext*)context;

@end
