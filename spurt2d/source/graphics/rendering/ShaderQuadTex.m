//
//  ShaderQuadTex.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/29/13.
//
//

#import "ShaderQuadTex.h"

@interface ShaderQuadTex ()

-(id)initShaderQuadTex;

@end

@implementation ShaderQuadTex

-(id)initShaderQuadTex
{
    if (self = [super initWithVertexShader:@"QuadTexture.vsh" andFragmentShader:@"QuadTexture.fsh" andPositionName:@"position" andTextureName:@"textureCoordinate" andColorName:@"" andUsesTextures:true andUsesColors:false andMaxVerticesForBatch:500*VERTICES])
    {
        
    }
    return self;
}

+(ShaderQuadTex*)sharedInstance
{
    static ShaderQuadTex* shaderQuadTex;
    static dispatch_once_t once;
    dispatch_once(&once, ^
    {
        shaderQuadTex = [[ShaderQuadTex alloc] initShaderQuadTex];
    });
    
    return shaderQuadTex;
}

@end
