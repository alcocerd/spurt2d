//
//  ShaderQuad.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/29/13.
//
//

#import "Shader.h"

extern const int VERTICES;
extern const int POSITION_VALUES_PER_VERTEX;
extern const int TEXTURE_VALUES_PER_VERTEX;
extern const int COLOR_VALUES_PER_VERTEX;

typedef struct
{
    float xBotLeft;
    float yBotLeft;
    
    float xBotRight;
    float yBotRight;
    
    float xTopLeft;
    float yTopLeft;
    
    float xTopRight;
    float yTopRight;
    
} PositionData;

typedef struct
{
    float name;
    
    float left;
    float bottom;
    float width;
    float height;
    
    // For normalization, we need the width and height of the entire asset.
    float assetWidth;
    float assetHeight;
    
} TextureData;

// Currently only supporting one color for the entire quad.
typedef struct
{
    float red;
    float green;
    float blue;
    float alpha;
    
} ColorData;

@interface ShaderQuad : Shader
{
@protected
    int vertexBatchCount;
    int positionBatchCount;
    int textureBatchCount;
    int colorBatchCount;
    
    int maxVerticesForBatch;
    
    float* positions;
    float* textures;
    float* colors;
    
    int positionLocation;
    int textureLocation;
    int colorLocation;
    
    // These are optional for Quads (although you'll probably want one of the two).
    BOOL useTextures;
    BOOL useColors;
}

-(id)initWithVertexShader:(NSString*)vertexShaderName andFragmentShader:(NSString*)fragmentShaderName andPositionName:(NSString*)positionName andTextureName:(NSString*)textureName andColorName:(NSString*)colorName andUsesTextures:(BOOL)newUseTextures andUsesColors:(BOOL)newUseColors andMaxVerticesForBatch:(int)newMaxVerticesForBatch;

/**
 Quads should use this to draw themselves.
 */
-(void)drawWithPosition:(PositionData)positionData andTexture:(TextureData)textureData andColor:(ColorData)colorData andBlending:(BLEND_TYPE)blendType;

@end
