//
//  ShaderProgram.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/17/13.
//
//

@interface ShaderProgram : NSObject
{
    
}
/**
 Pass the name of each file, including the extension, such as "filename.extension".  It Uses NSBundle's -pathForResource:ofType, which "does not recurse through other subdirectories" so use this accordingly.
 */
-(id)initWithVertexShader:(NSString*)vertexShaderName andFragmentShader:(NSString*)fragmentShaderName;

/**
 Use this to activate a shader (during run-time).  Only 1 shader is active at one time, whichever one was called last.
 */
-(void)use;

/**
 Get the location of an attribute for use (such as enabling/disabling).
 */
-(int)getAttribute:(NSString*)attributeName;

/**
 Get the location of a uniform for use.
 */
-(int)getUniform:(NSString*)uniformName;

@end
