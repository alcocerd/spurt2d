//
//  ScreenOrientator.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/7/14.
//
//

#import "ScreenOrientator.h"

// For OpenGL calls and matrix stuff.
#import <GLKit/GLKit.h>

// To get orientation notifications.
#import <UIKit/UIKit.h>

#import "Coordinate2d.h"
#import "Math.h"

@interface ScreenOrientator ()

-(id)initScreenOrientator;

@end

@implementation ScreenOrientator

-(id)initScreenOrientator
{
    if (self = [super init])
    {
        // Get the pixel density modifier.
        pixelScale = [[UIScreen mainScreen] scale];
    }
    return self;
}

+(ScreenOrientator*)sharedInstance
{
    static ScreenOrientator* screenOrientator;
    static dispatch_once_t once;
    dispatch_once(&once, ^
    {
        screenOrientator = [[ScreenOrientator alloc] initScreenOrientator];
    });
    
    return screenOrientator;
}

-(void)setScreenWidth:(float)newScreenWidth andScreenHeight:(float)newScreenHeight andMasterWidth:(float)newMasterWidth andMasterHeight:(float)newMasterHeight
{
    screenWidth = newScreenWidth * pixelScale;
    screenHeight = newScreenHeight * pixelScale;
    masterWidth = newMasterWidth;
    masterHeight = newMasterHeight;
    
    // Calculate the screen tier based on this device's screen size.
    int longScreenSide = (int)[self getLongSide];
    if (longScreenSide >= 2048)
        scaleTier = 0;
    else if (longScreenSide >= 960)
        scaleTier = 1;
    else
        scaleTier = 2;
    
    scaleFactor = 1 / [Math power:2 :scaleTier];
    
    glOrthof(0.0f, screenWidth, 0.0f, screenHeight, -1.0f, 1.0f);
    glViewport(0, 0, screenWidth, screenHeight);
    
    projectionMatrix = GLKMatrix4MakeOrtho(-screenWidth/2.0f, screenWidth/2.0f, -screenHeight/2.0f, screenHeight/2.0f, 0.0f, 0.0f);
}

-(GLKVector4)multiplyByProjectionMatrix:(GLKVector4)vector
{
    return GLKMatrix4MultiplyVector4(projectionMatrix, vector);
}

-(Coordinate2d*)orientInput:(Coordinate2d*)coordinate
{
    // Multiply the coordinates by our pixel density modifier.
    Coordinate2d* scaledCoordinate = [[Coordinate2d alloc] initWithX:[coordinate getX] * pixelScale andY:[coordinate getY] * pixelScale];
    float x = [scaledCoordinate getX] - [self getHalfWidth];
    float y = [self getHalfHeight] - [scaledCoordinate getY];
    
    return [[Coordinate2d alloc] initWithX:x andY:y];
}

-(float)getPixelScale
{
    return pixelScale;
}

-(float)getWidth
{
    return screenWidth;
}

-(float)getHeight
{
    return screenHeight;
}

-(float)getHalfWidth
{
    return [self getWidth] / 2.0f;
}

-(float)getHalfHeight
{
    return [self getHeight] / 2.0f;
}

-(float)getLeft
{
    return -[self getHalfWidth];
}

-(float)getRight
{
    return [self getHalfWidth];
}

-(float)getBottom
{
    return -[self getHalfHeight];
}

-(float)getTop
{
    return [self getHalfHeight];
}

-(float)getLongSide
{
    return [Math maxFloat:screenWidth :screenHeight];
}

-(float)getShortSide
{
    return [Math minFloat:screenWidth :screenHeight];
}

-(int)getScaleTier
{
    return scaleTier;
}

-(float)getScaleFactor
{
    return scaleFactor;
}

-(float)getMasterWidth
{
    return masterWidth;
}

-(float)getMasterHeight
{
    return masterHeight;
}

-(float)fromMasterLeft:(float)value
{
    float scaledScreenWidth = [self getWidth] / scaleFactor;
    float masterOffset = ([self getMasterWidth] - scaledScreenWidth) / 2.0f;
    return [self fromPercentWidth:(value - masterOffset) / scaledScreenWidth];
}

-(float)fromMasterRight:(float)value
{
    return [self fromMasterLeft:[self getMasterWidth] - value];
}

-(float)fromMasterBottom:(float)value
{
    float scaledScreenHeight = [self getHeight] / scaleFactor;
    float masterOffset = ([self getMasterHeight] - scaledScreenHeight) / 2.0f;
    return [self fromPercentHeight:(value - masterOffset) / scaledScreenHeight];
}

-(float)fromMasterTop:(float)value
{
    return [self fromMasterBottom:[self getMasterHeight] - value];
}

-(float)fromPercentWidth:(float)percentage
{
    return (percentage * [self getWidth]) - [self getHalfWidth];
}

-(float)fromPercentHeight:(float)percentage
{
    return (percentage * [self getHeight]) - [self getHalfHeight];
}

-(float)fromMaster:(float)value
{
    return value * scaleFactor;
}

@end
