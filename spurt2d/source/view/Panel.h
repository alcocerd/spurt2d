//
//  Panel.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/17/14.
//
//

#import "Drawable.h"
#import "Interactable.h"

#import "TouchPublisher.h"

@class Quad;
@class Text;
@class Animation;
@class Button;

typedef enum
{
    PS_INACTIVE,
    PS_BACKGROUND,
    PS_FOREGROUND,
    PS_ACTIVE,
} PANEL_STATE;

@interface Panel : NSObject <TouchSubscriber>
{
@private
    NSString* name;
    
@protected
    PANEL_STATE state;
    
    NSMutableArray* panels;
    NSMutableArray* drawables;
    NSMutableArray* interactables;
}

/**
 Panels can be retrived by name statically using getPanelByName.
 */
-(id)initWithName:(NSString*)newName;

// Methods for developers to utilize, at the moment these are not used explicitly by the system.
-(void)onInactive:(id)package;
-(void)onBackground:(id)package;
-(void)onForeground:(id)package;
-(void)onActive:(id)package;

// Methods the root panel should use to inform all panels about these events.
-(void)disseminateFirstUpdates;
/**
   Occurs if the panel is in the active state.
 */
-(BOOL)disseminateInteractions:(Touch*)touch;
-(void)disseminateUpdates;
/**
 Occurs if the panel is in the active, foreground, or background state.
 */
-(void)disseminateDrawings;
-(void)disseminatePauses;
-(void)disseminateUnpauses;

// Empty methods for subclasses to utilize as needed.
/**
 Called when the game goes through its first game loop.  Occurs regardless of the panel's state.
 */
-(void)onFirstUpdate;
/**
 Called when the game goes through a game loop.  Occurs when the panel is in the active, foreground, or background state.
 */
-(void)onUpdate;
/**
 Called when the game goes into the background.  Occurs regardless of the panel's state.
 */
-(void)onPause;
/**
 Called when the game comes back from the background.  Occurs regardless of the panel's state.
 */
-(void)onUnpause;
/**
 Allows the retrieval of Panels by name, so methods in this class can be used "anonymously" (ie you don't have to import the classes of panels you want to switch to, you can simply use the methods in this class on that panel).  Note that if more than one panel have the same name, this will simply return the first one it finds.  Also note that, if you want to use this method, you probably should not be destroying and recreating panels, instead panels should be kept alive for the duration of the execution.
 */
+(Panel*)getPanelByName:(NSString*)panelName;

// To help with one-liners.
-(Quad*)addQuad:(Quad*)quad;
-(Text*)addText:(Text*)text;
-(Animation*)addAnimation:(Animation*)animation;
-(Button*)addButton:(Button*)button;

@end
