//
//  Panel.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/17/14.
//
//

#import "Panel.h"

#import "Touch.h"

#import "Button.h"

@interface Panel ()

-(NSString*)getName;

@end

@implementation Panel

static NSMutableArray* allPanels;

+(void)initialize
{
    static BOOL runOnce = false;
    if (!runOnce)
    {
        runOnce = true;
        
        allPanels = [[NSMutableArray alloc] init];
    }
}

-(id)init
{
    return [self initWithName:@""];
}

-(id)initWithName:(NSString*)newName
{
    if (self = [super init])
    {
        name = newName;
        
        state = PS_INACTIVE;
        
        panels = [[NSMutableArray alloc] init];
        drawables = [[NSMutableArray alloc] init];
        interactables = [[NSMutableArray alloc] init];
        
        [allPanels addObject:self];
    }
    return self;
}

-(void)onInactive:(id)package
{
    state = PS_INACTIVE;
}

-(void)onBackground:(id)package
{
    state = PS_BACKGROUND;
}

-(void)onForeground:(id)package
{
    state = PS_FOREGROUND;
}

-(void)onActive:(id)package
{
    state = PS_ACTIVE;
}

-(void)disseminateFirstUpdates
{
    for (Panel* panel in panels)
    {
        [panel disseminateFirstUpdates];
    }
    
    [self onFirstUpdate];
}

-(BOOL)disseminateInteractions:(Touch*)touch
{
    BOOL didInteract = false;
    
    // Run this backwards to mimic the drawing order (so it looks correct to the user).
    int last = [panels count] - 1;
    for (int i = last; i >= 0; i--)
    {
        Panel* panel = [panels objectAtIndex:i];
        if ([panel disseminateInteractions:touch])
        {
            didInteract = true;
            break;
        }
    }
    
    if (!didInteract && state == PS_ACTIVE)
    {
        // Again, go backwards to mimic drawing.
        int last = [interactables count] - 1;
        for (int i = last; i >= 0; i--)
        {
            id<Interactable> interactable = [interactables objectAtIndex:i];
            if ([interactable interact:touch])
            {
                didInteract = true;
                break;
            }
        }
    }
    
    return didInteract;
}

-(void)disseminateUpdates
{
    for (Panel* panel in panels)
    {
        [panel disseminateUpdates];
    }
    
    if (state == PS_ACTIVE || state == PS_FOREGROUND || state == PS_BACKGROUND)
    {
        [self onUpdate];
    }
}

-(void)disseminateDrawings
{
    if (state == PS_ACTIVE || state == PS_FOREGROUND || state == PS_BACKGROUND)
    {
        // Draw this panel's contents.
        for (id<Drawable> drawable in drawables)
        {
            [drawable draw];
        }
    }
    
    // Allow child panels to draw after this panel so they are "on top."
    for (Panel* panel in panels)
    {
        [panel disseminateDrawings];
    }
}

-(void)disseminatePauses
{
    for (Panel* panel in panels)
    {
        [panel disseminatePauses];
    }
    
    [self onPause];
}

-(void)disseminateUnpauses
{
    for (Panel* panel in panels)
    {
        [panel disseminateUnpauses];
    }
    
    [self onUnpause];
}

-(void)onFirstUpdate
{
    
}

-(void)onUpdate
{
    
}

-(void)onPause
{
    
}

-(void)onUnpause
{
    
}

+(Panel*)getPanelByName:(NSString*)panelName
{
    Panel* panel = nil;
    for (Panel* p in allPanels)
    {
        if ([panelName isEqualToString:[p getName]])
        {
            panel = p;
            break;
        }
    }
    
    return panel;
}

-(NSString*)getName
{
    return name;
}

// TouchSubscriber protocol.
-(void)onTouch:(Touch*)touch
{
    [self disseminateInteractions:touch];
}

-(Quad*)addQuad:(Quad*)quad
{
    [drawables addObject:quad];
    return quad;
}

-(Text*)addText:(Text*)text
{
    [drawables addObject:text];
    return text;
}

-(Animation*)addAnimation:(Animation*)animation
{
    [drawables addObject:animation];
    return animation;
}

-(Button*)addButton:(Button*)button
{
    [interactables addObject:button];
    return button;
}

@end
