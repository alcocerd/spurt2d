//
//  ScreenOrientator.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/7/14.
//
//

#import <GLKit/GLKMathTypes.h>

@class Coordinate2d;

@interface ScreenOrientator : NSObject
{
@private
    GLKMatrix4 projectionMatrix;
    
    float screenWidth;
    float screenHeight;
    
    float masterWidth;
    float masterHeight;
    
    float pixelScale;
    
    int scaleTier;
    float scaleFactor;
}

+(ScreenOrientator*)sharedInstance;

-(void)setScreenWidth:(float)newScreenWidth andScreenHeight:(float)newScreenHeight andMasterWidth:(float)newMasterWidth andMasterHeight:(float)newMasterHeight;

-(GLKVector4)multiplyByProjectionMatrix:(GLKVector4)vector;

// Convert an input coordinate taking into account pixel density, orientation, etc.
-(Coordinate2d*)orientInput:(Coordinate2d*)coordinate;

// This shouldn't really be necessary by common needs, but here regardless.
-(float)getPixelScale;

// Information about the screen's dimensions.  If in portrait mode, the width returns the width and the height returns the height.  In landscape, they are swapped.
-(float)getWidth;
-(float)getHeight;
-(float)getHalfWidth;
-(float)getHalfHeight;

// Bounds assuming the origin is in the middle of the screen.
-(float)getLeft;
-(float)getRight;
-(float)getBottom;
-(float)getTop;

// The long side and short side of the device (iOS devices are historically 'taller' than they are 'wider,' therefore the long side would be the tall side).
-(float)getLongSide;
-(float)getShortSide;

// Based on the size of our screen, return what scale tier we are in (this helps with things like knowing which size assets to load).
// 0 is the master resolution, 1 would be the next smallest, and so on.
-(int)getScaleTier;
// Use this to scale things down from the master size (master will be 1, all other factors will be a fraction that gets smaller, such as 0.5, 0.25, etc).
-(float)getScaleFactor;

// Master dimensions.  These should be the dimensions of the largest supported device(s) for the project.  If in portrait mode, the width returns the width and the height returns the height.  In landscape, they are swapped.
-(float)getMasterWidth;
-(float)getMasterHeight;

// Methods to help with UI layouts.
/**
 This will help keep visuals consistent among several screen resolutions by scaling the given master value to this screen's size.  0.0f is the far left and the far right is the maximum value of master width.
 */
-(float)fromMasterLeft:(float)value;
/**
 This will help keep visuals consistent among several screen resolutions by scaling the given master value to this screen's size.  0.0f is the far right and the far left is the maximum value of master width.
 */
-(float)fromMasterRight:(float)value;
/**
 This will help keep visuals consistent among several screen resolutions by scaling the given master value to this screen's size.  0.0f is the far bottom and the far top is the maximum value of master height.
 */
-(float)fromMasterBottom:(float)value;
/**
 This will help keep visuals consistent among several screen resolutions by scaling the given master value to this screen's size.  0.0f is the far top and the far bottom is the maximum value of master height.
 */
-(float)fromMasterTop:(float)value;
/**
 For visuals which need to exist in a certain area of the screen, instead of tied to a specific pixel (like using the master resolutions).  0.0f is the far left, 1.0f is the far right.
 */
-(float)fromPercentWidth:(float)percentage;
/**
 For visuals which need to exist in a certain area of the screen, instead of tied to a specific pixel (like using the master resolutions).  0.0f is the far bottom, 1.0f is the far top.
 */
-(float)fromPercentHeight:(float)percentage;

/**
 For scaling values down to the device's screen size.
 */
-(float)fromMaster:(float)value;

@end
