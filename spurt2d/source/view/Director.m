//
//  Director.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/28/13.
//
//

#import "Director.h"

// Screen.
#import "ScreenOrientator.h"

// Visual.
#import "Renderer.h"
#import "TextureProducer.h"

// Input.
#import "TouchPublisher.h"

// Time.
#import "Clock.h"
#import "TimerScheduler.h"

// Panel.
#import "ExamplePanel.h"

@interface Director ()

-(void)setup;
-(void)checkFPS;

@end

@implementation Director

-(id)init
{
    logCritical(@"Creating a nil Director.");
    return nil;
}

-(id)initWithContext:(EAGLContext*)newContext andView:(GLKView*)newView andShowExample:(BOOL)showExample
{
    if (self = [super init])
    {
        context = newContext;
        view = newView;
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        
        isFirstGameLoop = true;
        
        [self setDoPrintFPS:false];
        [self setup];
        
        if (showExample)
        {
            panel = [[ExamplePanel alloc] init];
        }
    }
    return self;
}

#pragma mark - Setup

-(void)setup
{
    CGSize screenSize = view.frame.size;
    // Set up our ScreenOrientation with the information it needs.
    [[ScreenOrientator sharedInstance] setScreenWidth:screenSize.width andScreenHeight:screenSize.height andMasterWidth:[self getMasterWidth] andMasterHeight:[self getMasterHeight]];
    
    // This loads up resources, let's get that out of the way now so when called later, it doesn't bog down on the first call.
    [TextureProducer sharedInstance];
    
    renderer = [[Renderer alloc] init];
    
    timerScheduler = [[TimerScheduler alloc] init];
}

#pragma mark - Main loop

-(void)update
{
    Panel* rootPanel = [self getRootPanel];
    
    // First game loop.
    if (isFirstGameLoop)
    {
        isFirstGameLoop = false;
        
        [[TouchPublisher sharedInstance] subscribe:[self getRootPanel]];
        
        // TODO: Done here ideally to help with the first delta time not being so big, but it doesn't seem to help too much.  Rethink this.
        clock = [[Clock alloc] init];
        [clock update];
        
        [rootPanel disseminateFirstUpdates];
    }
    else
    {
        [clock update];
    }
    
    // Check time-based events.
    [timerScheduler update];
    
    // Handle input.  Panel is in charge of disseminating interactions when these get published.
    [[TouchPublisher sharedInstance] publishEvents];
    
    // Game logic.
    [rootPanel disseminateUpdates];
    
    // Rendering.
    // Start the frame.
    [renderer startFrame];
    // Draw.
    [rootPanel disseminateDrawings];
    // End the frame.
    [renderer finishFrame:context];
    
    // Frames per second debugging.
    if (doPrintFPS)
    {
        [self checkFPS];
    }
}

-(void)touchesBegan:(NSSet*)touches
{
    [[TouchPublisher sharedInstance] touchesBegan:touches];
}

-(void)touchesMoved:(NSSet*)touches
{
    [[TouchPublisher sharedInstance] touchesMoved:touches];
}

-(void)touchesEnded:(NSSet*)touches
{
    [[TouchPublisher sharedInstance] touchesEnded:touches];
}

-(void)pause
{
    [clock pause];
    [[self getRootPanel] disseminatePauses];
}

-(void)unpause
{
    [clock unpause];
    [[self getRootPanel] disseminateUnpauses];
}

// Max resolutions of the devices we support (1536 is the width of the iPad 3 and 2272 is the height of the iPhone 5s).
-(float)getMasterWidth
{
    return 1536.0f;
}

-(float)getMasterHeight
{
    return 2272.0f;
}

-(Panel*)getRootPanel
{
    return panel;
}

#pragma mark - FPS

-(void)setDoPrintFPS:(BOOL)newDoPrintFPS
{
    doPrintFPS = newDoPrintFPS;
}

-(void)checkFPS
{
    static CFTimeInterval now = 0.0;
    static CFTimeInterval nextUpdate = 0.0;
    static CFTimeInterval updateInterval = 3.0;
    static int frames = 0;
    
    frames++;
    
    now = [Clock getElapsedTime];
    if (now >= nextUpdate)
    {
        nextUpdate = now + updateInterval;
        logDebug(@"FPS: %f", frames / updateInterval);
        frames = 0;
    }
}

@end
