//
//  ExamplePanel.m
//  spurt2d
//
//  Created by Daniel Alcocer on 3/28/14.
//
//

#import "ExamplePanel.h"

#import "ScreenOrientator.h"
#import "MutableFrame.h"

#import "TimerScheduler.h"

#import "Quad.h"
#import "Text.h"

#import "Button.h"

@implementation ExamplePanel

-(id)init
{
    if (self = [super initWithName:@"Example_Root_Panel"])
    {
        [self onActive:nil];
    }
    
    return self;
}

-(void)onFirstUpdate
{
    // Make an image and align it to the top left of the screen.
    Quad* image = [self addQuad:[Quad quadByTextureMapping:TA_testimage]];
    [[image getFrame] setScaleUniform:3.0f];
    [[image getFrame] setHorizontalAlignment:HA_LEFT];
    [[image getFrame] setVerticalAlignment:VA_TOP];
    [[image getFrame] setMarginLeft:0.0f];
    [[image getFrame] setMarginTop:0.0f];
    // Make a button for it.
    Button* button = [self addButton:[Button buttonWithFrame:[image getFrame]]];
    [button registerEnter:^
     {
         logDebug(@"I've been clicked!");
     }];
    
    // Create some text, make the image above its parent, and align its left to the right of the image (with a bit of space in between).
    Text* text = [self addText:[Text textByFontMapping:FA_bybsy40_33_126]];
    [text setText:@"Welcome, press this button to my left."];
    [[text getFrame] setParent:[image getFrame]];
    [[text getFrame] setHorizontalAlignment:HA_LEFT];
    // 20 pixels of space in between, but let the ScreenOrientator scale down if needed (for smaller screens).
    float space = [[ScreenOrientator sharedInstance] fromMaster:20.0f];
    [[text getFrame] setMarginRight:-space];
    
    // Make another image wiggle indefinitely.
    Quad* anotherImage = [self addQuad:[Quad quadByTextureMapping:TA_testimage]];
    [[anotherImage getFrame] setScaleUniform:4.0f];
    [TimerScheduler startCourse:[Course wiggle:[anotherImage getRotation] atSpeed:0.4f during:INDEFINITE_TIME withRotationRangeInDegrees:20.0f withDelay:0.0 andResetRotation:false andOwner:self]];
}

@end
