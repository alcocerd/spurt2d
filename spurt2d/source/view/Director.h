//
//  Director.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/28/13.
//
//

@class EAGLContext;
@class GLKView;

@class Renderer;
@class Panel;
@class Clock;
@class TimerScheduler;

@interface Director : NSObject
{
@protected
    EAGLContext* context;
    GLKView* view;
    
    Renderer* renderer;
    Panel* panel;
    Clock* clock;
    TimerScheduler* timerScheduler;
    
    BOOL isFirstGameLoop;
    
    BOOL doPrintFPS;
}

-(id)initWithContext:(EAGLContext*)newContext andView:(GLKView*)newView andShowExample:(BOOL)showExample;

-(void)update;

-(void)touchesBegan:(NSSet*)touches;
-(void)touchesMoved:(NSSet*)touches;
-(void)touchesEnded:(NSSet*)touches;

-(void)pause;
-(void)unpause;

-(float)getMasterWidth;
-(float)getMasterHeight;

// Overwrite this to return your own panel if desired.
-(Panel*)getRootPanel;

// Prints FPS for debugging.
-(void)setDoPrintFPS:(BOOL)newDoPrintFPS;

@end
