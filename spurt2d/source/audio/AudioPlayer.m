//
//  AudioPlayer.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/17/14.
//
//

#import "AudioPlayer.h"

@interface AudioPlayer ()

-(id)initAudioPlayer;

-(AVAudioPlayer*)play:(NSString*)fileURL;
-(AVAudioPlayer*)loop:(NSString*)fileURL;

@end

@implementation AudioPlayer

-(id)initAudioPlayer
{
    if (self = [super init])
    {
        players = [[NSMutableArray alloc] init];
        
        mainMusicPlayer = nil;
        
        [self setSoundEnabled:true];
        [self setMusicEnabled:true];
    }
    return self;
}

+(AudioPlayer*)sharedInstance
{
    static AudioPlayer* audioPlayer;
    static dispatch_once_t once;
    dispatch_once(&once, ^
    {
        audioPlayer = [[AudioPlayer alloc] initAudioPlayer];
    });
    
    return audioPlayer;
}

-(AVAudioPlayer*)load:(NSString*)fileURL
{
    NSString* fileURLWithoutExtension = [fileURL stringByDeletingPathExtension];
    NSString* extension = [fileURL pathExtension];
    
    NSURL* url = [[NSBundle mainBundle] URLForResource:fileURLWithoutExtension withExtension:extension];
    NSError* error = nil;
    AVAudioPlayer* player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if (!player)
    {
        logCritical(@"Error creating AVAudioPlayer: %@, in method %@", [error localizedDescription], NSStringFromSelector(_cmd));
    }
    
    [player setDelegate:self];
    [player prepareToPlay];
    
    return player;
}

-(AVAudioPlayer*)play:(NSString*)fileURL
{
    AVAudioPlayer* player = [self load:fileURL];
    [player play];
    
    return player;
}

-(AVAudioPlayer*)loop:(NSString*)fileURL
{
    AVAudioPlayer* player = [self play:fileURL];
    player.numberOfLoops = -1;
    
    return player;
}

-(void)setSoundEnabled:(BOOL)isEnabled
{
    isSoundEnabled = isEnabled;
}

-(void)setMusicEnabled:(BOOL)isEnabled
{
    isMusicEnabled = isEnabled;
    
    if (!isEnabled)
    {
        [mainMusicPlayer stop];
    }
}

-(void)playSound:(NSString*)fileURL
{
    if (isSoundEnabled)
    {
        AVAudioPlayer* player = [self play:fileURL];
        [players addObject:player];
    }
}

-(void)loopMainMusic:(NSString*)fileURL
{
    [mainMusicPlayer stop];
    
    if (isMusicEnabled)
    {
        mainMusicPlayer = [self loop:fileURL];
    }
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer*)player
{
    // Players get paused for us, no need to do that here.
}

-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer*)player error:(NSError*)error
{
    logWarning(@"Error decoding audio: %@, in method %@", [error localizedDescription], NSStringFromSelector(_cmd));
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer*)player successfully:(BOOL)flag
{
    [players removeObjectIdenticalTo:player];
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer*)player withOptions:(NSUInteger)flags
{
    // Resume if we are told to.
    if (flags == AVAudioSessionInterruptionOptionShouldResume)
    {
        [player play];
    }
}

@end
