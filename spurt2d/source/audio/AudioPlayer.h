//
//  AudioPlayer.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/17/14.
//
//

#import <AVFoundation/AVFoundation.h>

@interface AudioPlayer : NSObject <AVAudioPlayerDelegate>
{
@private
    NSMutableArray* players;
    
    AVAudioPlayer* mainMusicPlayer;
    
    BOOL isSoundEnabled;
    BOOL isMusicEnabled;
}

+(AudioPlayer*)sharedInstance;

/**
 Enable or disable sound/music.  For example, if isEnabled is false, all future "playSound" or "loopSound" calls will be ignored.
 */
-(void)setSoundEnabled:(BOOL)isEnabled;
/**
 This works like setSoundEnabled, except that it will also stop the mainMusicPlayer if isEnabled is false.
 */
-(void)setMusicEnabled:(BOOL)isEnabled;

/**
 If you want to load a player and manage it yourself.
 */
-(AVAudioPlayer*)load:(NSString*)fileURL;

/**
 Plays the given file as a sound.
 */
-(void)playSound:(NSString*)fileURL;

/**
 Loops the file as music, if there was previously looping music (from this call), it is stopped.
 */
-(void)loopMainMusic:(NSString*)fileURL;

@end
