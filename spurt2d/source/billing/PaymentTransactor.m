//
//  PaymentTransactor.m
//  spurt2d
//
//  Created by Daniel Alcocer on 3/6/14.
//
//

#import "PaymentTransactor.h"

@interface PaymentTransactor ()

-(id)initPaymentTransactor;

-(void)recordPayment:(SKPaymentTransaction*)paymentTransaction;
-(void)purchasedRecieved:(SKPaymentTransaction*)paymentTransaction;

@end

@implementation PaymentTransactor

-(id)initPaymentTransactor
{
    if (self = [super init])
    {
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
        paymentListeners = [[NSMutableArray alloc] init];
        
        // Create an empty array to start.
        products = [[NSArray alloc] init];
    }
    return self;
}

+(PaymentTransactor*)sharedInstance
{
    static PaymentTransactor* paymentTransactor;
    static dispatch_once_t once;
    dispatch_once(&once, ^
    {
        paymentTransactor = [[PaymentTransactor alloc] initPaymentTransactor];
    });
    
    return paymentTransactor;
}

-(void)listenForPayments:(id<PaymentListener>)paymentListener
{
    [paymentListeners addObject:paymentListener];
}

-(void)requestProductList:(NSMutableSet*)productIdentifiers
{
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

-(void)requestPurchase:(NSString*)productIdentifier
{
    SKProduct* product = nil;
    
    // Find the product in our list that has the identifier given.
    for (SKProduct* indexedProduct in products)
    {
        if ([productIdentifier isEqualToString:indexedProduct.productIdentifier])
        {
            product = indexedProduct;
            break;
        }
    }
    
    // If we found it, request the purchase.
    if (product)
    {
        [[SKPaymentQueue defaultQueue] addPayment:[SKPayment paymentWithProduct:product]];
    }
}

-(SKProduct*)getProduct:(NSString*)productIdentifier
{
    SKProduct* product = nil;
    
    // Find the product in our list that has the identifier given.
    for (SKProduct* indexedProduct in products)
    {
        if ([productIdentifier isEqualToString:indexedProduct.productIdentifier])
        {
            product = indexedProduct;
            break;
        }
    }
    
    return product;
}

-(void)restorePurchases
{
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray*)transactions
{
    for (SKPaymentTransaction* paymentTransaction in transactions)
    {
        switch (paymentTransaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
            {
                [self purchasedRecieved:paymentTransaction];
                
                break;
            }
            case SKPaymentTransactionStateRestored:
            {
                [self purchasedRecieved:paymentTransaction];
                
                break;
            }
            case SKPaymentTransactionStateFailed:
            {
                [[SKPaymentQueue defaultQueue] finishTransaction:paymentTransaction];
                
                break;
            }
            default:
            {
                break;
            }
        }
    }
}

-(void)productsRequest:(SKProductsRequest*)request didReceiveResponse:(SKProductsResponse*)response
{
    products = response.products;
    
    for (id<PaymentListener> paymentListener in paymentListeners)
    {
        if ([paymentListener respondsToSelector:@selector(onReceivedProductList:)])
        {
            [paymentListener onReceivedProductList:response.products];
        }
    }
    
    for (NSString* invalidProductID in response.invalidProductIdentifiers)
    {
        logWarning(@"Invalid product id: %@", invalidProductID);
    }
}

-(void)request:(SKRequest*)request didFailWithError:(NSError*)error
{
    logWarning(@"Payment localized description error: %@", [error localizedDescription]);
}

-(void)recordPayment:(SKPaymentTransaction*)paymentTransaction
{
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:paymentTransaction.payment.productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)purchasedRecieved:(SKPaymentTransaction*)paymentTransaction
{
    [self recordPayment:paymentTransaction];
    [[SKPaymentQueue defaultQueue] finishTransaction:paymentTransaction];
    
    for (id<PaymentListener> paymentListener in paymentListeners)
    {
        if ([paymentListener respondsToSelector:@selector(onReceivedProduct:)])
        {
            [paymentListener onReceivedProduct:paymentTransaction.payment.productIdentifier];
        }
    }
}

@end
