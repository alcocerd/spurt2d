//
//  PaymentTransactor.h
//  spurt2d
//
//  Created by Daniel Alcocer on 3/6/14.
//
//

#import <StoreKit/StoreKit.h>

@protocol PaymentListener <NSObject>

@optional
/**
 An NSArray of SKProduct* objects.  This is typically called once after requestProductList is called and a response is receieved from Apple's servers (it should happen pretty quickly, typically seconds or less).  Use this if you want to have a list of all the products and their information.
 */
-(void)onReceivedProductList:(NSArray*)products;
/**
 Use this when you need to do something after the user has successfully purchased one of your products (i.e., if they bought some coins, give them some coins).
 */
-(void)onReceivedProduct:(NSString*)productIdentifier;

@end

@interface PaymentTransactor : NSObject <SKPaymentTransactionObserver, SKProductsRequestDelegate>
{
@private
    SKProductsRequest* productsRequest;
    NSMutableArray* paymentListeners;
    
    NSArray* products;
}

+(PaymentTransactor*)sharedInstance;

/**
 Add an object to the list of payment listeners.
 */
-(void)listenForPayments:(id<PaymentListener>)paymentListener;

// TODO: Add method for removing payment listeners.

/**
 This must be called for all products that the developer intends for the user to be able to purchase (if you want the user to be able to buy a product, it must be in this set). productIdentifiers is a set of NSStrings* of product identifiers (they must match what you put on Apple's backend).  This method can typically be called once on start up (but for certain systems, this may not be the case).
 */
-(void)requestProductList:(NSMutableSet*)productIdentifiers;
/**
 When the user wants to purchase something (like from clicking a button), this will request that product to be purchased.
 */
-(void)requestPurchase:(NSString*)productIdentifier;
/**
 Get the product for a given identifier; returns nil if that identifier was not found.
 */
-(SKProduct*)getProduct:(NSString*)productIdentifier;

/**
 If the user reinstalled the app, they can restore all non-consumable purchases to the app by calling this (via button-press for example).
 */
-(void)restorePurchases;

@end
