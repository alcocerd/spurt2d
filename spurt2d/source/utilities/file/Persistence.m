//
//  Persistence.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

#import "Persistence.h"

@interface Persistence ()

-(void)load;
-(NSString*)getFilePath;

@end

@implementation Persistence

-(id)init
{
    logCritical(@"Creating a nil Persistence.");
    return nil;
}

-(id)init:(NSString*)key
{
    if (self = [super init])
    {
        dictionaryKey = key;
        [self load];
    }
    return self;
}

-(void)load
{
    // Load the dictionary from disk as data.
    NSData* data = [NSData dataWithContentsOfFile:[self getFilePath]];
    
    // Convert to a dictionary.
    dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    // If it didn't exist (such as on the first run), create it.
    if (dictionary == nil)
    {
        dictionary = [[NSMutableDictionary alloc] init];
    }
}

-(NSString*)getFilePath
{
    NSString* directory = nil;
    
    // Go to the documents directory where we can safely write.
    NSArray* directories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true);
    if (directories)
    {
        directory = [directories objectAtIndex:0];
    }
    else
    {
        logCritical(@"%@, no documents directory found.", NSStringFromSelector(_cmd));
    }
    
    NSString* filePath = [directory stringByAppendingPathComponent:dictionaryKey];
    return filePath;
}

-(void)save
{
    // Convert dictionary to data.
    NSData* data = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    
    // Write the data to disk.
    [data writeToFile:[self getFilePath] atomically:true];
    
    // Use this instead for debugging write errors.
    //NSError* error;
    //[data writeToFile:[self getFilePath] options:NSDataWritingAtomic error:&error];
    //logDebug(@"Failure reason: %@ Description: %@", [error localizedFailureReason], [error localizedDescription]);
}

-(void)put:(id)object withKey:(NSString*)key
{
    [dictionary setObject:object forKey:key];
}

-(id)get:(NSString*)key
{
    return [dictionary objectForKey:key];
}

-(void)remove:(NSString*)key
{
    [dictionary removeObjectForKey:key];
}

-(void)removeAllObjects
{
    [dictionary removeAllObjects];
}

@end
