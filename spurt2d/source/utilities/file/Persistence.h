//
//  Persistence.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

// A class to help with loading/saving things to the disk.
@interface Persistence : NSObject
{
@private
    NSString* dictionaryKey;
    NSMutableDictionary* dictionary;
}

// Constructs this object as well as loads its dictionary into memory from the disk.
-(id)init:(NSString*)key;

// Save to disk.
-(void)save;

// Methods to manipulate what gets saved.
-(void)put:(id)object withKey:(NSString*)key;
-(id)get:(NSString*)key;
-(void)remove:(NSString*)key;
-(void)removeAllObjects;

@end
