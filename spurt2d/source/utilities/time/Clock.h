//
//  Clock.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

// Only one Clock can exist (whoever constructed it last) at any given time (well, for all intents and purposes since that instance can control the static variables).  A singleton doesn't work here since we don't want just anybody updating/pausing/unpausing the Clock.  Can we think of a better solution?
@interface Clock : NSObject

// To be called once per game loop, by whoever is in charge of the Clock.
-(void)update;

// Manipulate time as necessary, should only be used when the game is no longer in view for the user.
-(void)pause;
-(void)unpause;

// Methods a lot of objects may need - in seconds.
// Time between loops.
+(CFTimeInterval)getDeltaTime;
// Duration of the application while active (meaning we don't include time while in the background).
// You most likely want this over getTimeSinceStartup ("game time").
+(CFTimeInterval)getElapsedTime;
// Total time since the application started, even while in the background.
// You probably don't want to use this for many things ("real time").
+(CFTimeInterval)getTimeSinceStartup;
// Time the device tells us.
+(CFTimeInterval)getSystemTime;

@end
