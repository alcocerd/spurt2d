//
//  Clock.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

#import "Clock.h"

// For CACurrentMediaTime().
#import <QuartzCore/CABase.h>

@implementation Clock

static const CFTimeInterval DELTA_TIME_DEFAULT_ON_ERROR = 0.01;

// Start time of the application.
static CFTimeInterval applicationStartTime;

// The two time values a lot of objects will need.
static CFTimeInterval deltaTime;
static CFTimeInterval elapsedTime;
// The loop before this - only used for calculating delta time.
static CFTimeInterval lastElapsedTime;

// Pause markers.
static CFTimeInterval lastPauseStartTime;
static CFTimeInterval totalPauseTime;

static BOOL isPaused;

-(id)init
{
    if (self = [super init])
    {
        isPaused = false;
        
        // Pause time starts at 0 since the application inherently starts off not being paused!
        totalPauseTime = 0.0;
        
        // Tag the time so our clock conveniently starts at 0.
        applicationStartTime = [Clock getSystemTime];
        
        // Since calculations should be using getTimeSinceStartup (as opposed to getSystemTime), this should be 0.0.
        lastElapsedTime = 0.0;
        
        // Clock should be updated before anyone calls [Clock getDeltaTime] for the first time, but just in case.
        deltaTime = DELTA_TIME_DEFAULT_ON_ERROR;
        
        // More "just in cases" before the first update.
        elapsedTime = 0.0;
        lastPauseStartTime = 0.0;
    }
    return self;
}

-(void)update
{
    // Tag the current time of this loop.
    elapsedTime = [Clock getTimeSinceStartup] - totalPauseTime;
    
    // Find out what the difference is from the last loop to the current loop.
    deltaTime = elapsedTime - lastElapsedTime;
    if (deltaTime <= 0.0)
    {
        // This should never happen unless under extreme circumstances (like if the user is manipulating the device's clock while running the application?)
        deltaTime = DELTA_TIME_DEFAULT_ON_ERROR;
        logCritical(@"Delta time of 0. %@", NSStringFromSelector(_cmd));
    }
    
    // The current time will be our last loop for the next loop.
    lastElapsedTime = elapsedTime;
}

-(void)pause
{
    // In case we get excess redundant calls, this will prevent miscalculations.
    if (!isPaused)
    {
        isPaused = true;
        // Whatever loop we ran last, we'll consider that to be our true pause start time.
        lastPauseStartTime = elapsedTime;
    }
}

// Assumes this gets called before the game's next update function gets called, this assumption seems extremely reasonable.
-(void)unpause
{
    // In case we get excess redundant calls, this will prevent miscalculations.
    if (isPaused)
    {
        isPaused = false;
        // Get the total amount of time the application was paused for.
        totalPauseTime = [Clock getTimeSinceStartup] - lastPauseStartTime;
    }
}

+(CFTimeInterval)getDeltaTime
{
    return deltaTime;
}

+(CFTimeInterval)getElapsedTime
{
    return elapsedTime;
}

+(CFTimeInterval)getTimeSinceStartup
{
    return [Clock getSystemTime] - applicationStartTime;
}

+(CFTimeInterval)getSystemTime
{
    return CACurrentMediaTime();
}

@end
