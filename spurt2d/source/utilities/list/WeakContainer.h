//
//  WeakContainer.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/15/14.
//
//

// A small container class which holds a self-zeroing weak reference to an object.  Useful when you want a list (ie NSArray) of weakly referrenced objects.
@interface WeakContainer : NSObject
{
@private
    id __weak object;
}

-(id)init:(id)newObject;

// When this returns nil, the object is gone.
-(id)getObject;

@end
