//
//  LinkedNode.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/26/14.
//
//

@interface LinkedNode : NSObject
{
@private
    id value;
    LinkedNode* next;
}

-(void)setNext:(LinkedNode*)newNext;
-(LinkedNode*)getNext;

-(void)setValue:(id)newValue;
-(id)getValue;

@end
