//
//  LinkedList.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/26/14.
//
//

#import "LinkedList.h"

@implementation LinkedList

-(id)init
{
    if (self = [super init])
    {
        head = nil;
        tail = nil;
    }
    return self;
}

-(LinkedNode*)getHead
{
    return head;
}

-(LinkedNode*)getTail
{
    return tail;
}

-(LinkedNode*)addNode:(id)value
{
    // Create a new node.
    LinkedNode* newNode = [[LinkedNode alloc] init];
    [newNode setValue:value];
    
    // If this is our first node, have the head and tail point to it.
    if (head == nil)
    {
        head = newNode;
        tail = newNode;
    }
    // Else update the new tail.
    else
    {
        [tail setNext:newNode];
        tail = newNode;
    }
    
    return newNode;
}

@end
