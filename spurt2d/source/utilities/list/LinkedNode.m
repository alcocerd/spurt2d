//
//  LinkedNode.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/26/14.
//
//

#import "LinkedNode.h"

@implementation LinkedNode

-(id)init
{
    if (self = [super init])
    {
        value = nil;
        next = nil;
    }
    return self;
}

-(void)setNext:(LinkedNode*)newNext
{
    next = newNext;
}

-(LinkedNode*)getNext
{
    return next;
}

-(void)setValue:(id)newValue
{
    value = newValue;
}

-(id)getValue
{
    return value;
}

@end
