//
//  WeakContainer.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/15/14.
//
//

#import "WeakContainer.h"

@implementation WeakContainer

-(id)init
{
    return [self init:nil];
}

-(id)init:(id)newObject
{
    if (self = [super init])
    {
        object = newObject;
    }
    return self;
}

-(id)getObject
{
    return object;
}

@end
