//
//  LinkedList.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/26/14.
//
//

#import "LinkedNode.h"

// Extremely basic linked list.
@interface LinkedList : NSObject
{
@private
    LinkedNode* head;
    LinkedNode* tail;
}

-(LinkedNode*)getHead;
-(LinkedNode*)getTail;

-(LinkedNode*)addNode:(NSString*)node;

@end
