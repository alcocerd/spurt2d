//
//  Log.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/17/13.
//
//

#ifdef DEBUG
/*
 Debugging function.  Calls to this function can be left in code without being compiled/ran for release builds.
 */
void logDebug(NSString* stringFormat, ...);
#else
// Empty macro so this doesn't get compiled in release builds.
#define logDebug(...) {}
#endif

/*
 Log warnings for both debug and release builds, call this when something fishy that shouldn't happen, happens.
 */
void logWarning(NSString* stringFormat, ...);
/*
 Log critical errors for both debug and release builds, something really bad happened that should not have happened.
 */
void logCritical(NSString* stringFormat, ...);
