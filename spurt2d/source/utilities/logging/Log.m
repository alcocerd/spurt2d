//
//  Log.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/17/13.
//
//

#import "Log.h"

#include <asl.h>

// Macro to build a logging function.
#define MAKE_LOG_FUNCTION(LOG_FUNCTION_NAME, LOG_LEVEL)                                 \
void LOG_FUNCTION_NAME(NSString* stringFormat, ...)                                     \
{                                                                                       \
    /* We only want the following block to occur once (via GCD).*/                      \
    static dispatch_once_t once;                                                        \
    dispatch_once(&once, ^                                                              \
    {                                                                                   \
        /* Tell ASL that we want to see these messages in the debugging console.*/      \
        asl_add_log_file(NULL, STDERR_FILENO);                                          \
        /* Uncomment this line if you want to see all messages*/                        \
        /* (ASL_LEVEL_DEBUG being the lowest priority) in the device log*/              \
        /* (the default is up to ASL_LEVEL_INFO I believe, but not 100% sure).*/        \
        /*asl_set_filter(NULL, ASL_FILTER_MASK_UPTO(ASL_LEVEL_DEBUG));*/                \
    });                                                                                 \
                                                                                        \
    /* Build the message and have ASL log it.*/                                         \
    va_list list;                                                                       \
    va_start(list, stringFormat);                                                       \
    NSString* message = [[NSString alloc] initWithFormat:stringFormat arguments:list];  \
    asl_log(NULL, NULL, LOG_LEVEL, "%s", [message UTF8String]);                         \
    va_end(list);                                                                       \
}                                                                                       \

// Now make a few logging functions.
#ifdef DEBUG
MAKE_LOG_FUNCTION(logDebug, ASL_LEVEL_DEBUG)
#endif

MAKE_LOG_FUNCTION(logWarning, ASL_LEVEL_WARNING)
MAKE_LOG_FUNCTION(logCritical, ASL_LEVEL_CRIT)

#undef MAKE_LOG_FUNCTION
