//
//  Coordinate2d.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/6/14.
//
//

@interface Coordinate2d : NSObject
{
@protected
    float x;
    float y;
}

-(id)initWithX:(float)newX andY:(float)newY;

// Shorter methods for initialization.
+(Coordinate2d*)coordinateWithCoordinate:(Coordinate2d*)otherCoordinate;
+(Coordinate2d*)coordinateWithX:(float)newX andY:(float)newY;

-(float)getX;
-(float)getY;

-(float)getAngle:(Coordinate2d*)otherCoordinate;
-(float)getDistance:(Coordinate2d*)otherCoordinate;

@end
