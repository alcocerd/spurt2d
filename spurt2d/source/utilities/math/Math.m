//
//  Math.m
//  spurt2d
//
//  Created by Daniel Alcocer on 12/28/13.
//
//

#import "Math.h"

const float PI = 3.14159265359f;
const float PI_OVER_TWO = PI / 2.0f;
const float THREE_PI_OVER_TWO = 3.0f * PI / 2.0f;
const float TWO_PI = 2.0f * PI;

const float DEGREES_PER_RADIAN = 180.0f / PI;
const float RADIANS_PER_DEGREE = PI / 180.0f;

@implementation Math

+(float)minFloat:(float)a :(float)b
{
    if (a > b)
        return b;
    else
        return a;
}

+(float)maxFloat:(float)a :(float)b
{
    if (a > b)
        return a;
    else
        return b;
}

+(int)minInt:(int)a :(int)b
{
    if (a > b)
        return b;
    else
        return a;
}

+(int)maxInt:(int)a :(int)b
{
    if (a > b)
        return a;
    else
        return b;
}

+(float)clampFloat:(float)value Min:(float)min Max:(float)max
{
    if (value < min)
        value = min;
    else if (value > max)
        value = max;
    return value;
}

+(int)clampInt:(int)value Min:(int)min Max:(int)max
{
    if (value < min)
        value = min;
    else if (value > max)
        value = max;
    return value;
}

+(float)cos:(float)value
{
    return cosf(value);
}

+(float)sin:(float)value
{
    return sinf(value);
}

+(float)arcTan:(float)y :(float)x
{
    return atan2f(y, x);
}

+(float)distance:(float)x1 :(float)y1 :(float)x2 :(float)y2
{
    return [Math squareRoot:[Math power:x2 - x1 :2] + [Math power:y2 - y1 :2]];
}

+(float)squareRoot:(float)value
{
    return sqrtf(value);
}

+(float)power:(float)base :(float)exponent
{
    return powf(base, exponent);
}

+(float)absolute:(float)value
{
    return fabsf(value);
}

+(double)floorDouble:(double)value
{
    return floor(value);
}

+(double)ceilingDouble:(double)value
{
    return ceil(value);
}

+(float)floor:(float)value
{
    return floorf(value);
}

+(float)ceiling:(float)value
{
    return ceilf(value);
}

+(BOOL)nextBoolean
{
    return [Math nextInt:2] == 0;
}

+(BOOL)nextBoolean:(float)trueChance
{
    float f = [Math nextFloat];
    if (f > trueChance)
        return false;
    else
        return true;
}

+(unsigned int)nextInt
{
    return arc4random();
}

+(unsigned int)nextInt:(int)n
{
    return [Math nextInt] % n;
}

+(float)nextFloat
{
    return ((float)(arc4random() % INT_MAX)) / (float)INT_MAX;
}

+(float)nextFloat:(float)n
{
    return [Math nextFloat] * n;
}

+(BOOL)isPositiveDouble:(double)d
{
    return d > 0.0;
}

+(double)getSignDouble:(double)d
{
    if (d > 0.0)
        return 1.0;
    else if (d < 0.0)
        return -1.0;
    else
        return 0.0;
}

+(float)getSignFloat:(float)f
{
    if (f > 0.0f)
        return 1.0f;
    else if (f < 0.0f)
        return -1.0f;
    else
        return 0.0f;
}

+(float)roundFloat:(float)f
{
    return roundf(f);
}

@end
