//
//  MutableCoordinate1d.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/6/14.
//
//

#import "MutableCoordinate1d.h"

@implementation MutableCoordinate1d

+(MutableCoordinate1d*)coordinateWithX:(float)newX
{
    return [[MutableCoordinate1d alloc] initWithX:newX];
}

-(void)setX:(float)newX
{
    x = newX;
}

-(void)addX:(float)xToAdd
{
    x += xToAdd;
}

@end
