//
//  Coordinate1d.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/6/14.
//
//

@interface Coordinate1d : NSObject
{
@protected
    float x;
}

-(id)initWithX:(float)newX;

// Shorter method for initialization.
+(Coordinate1d*)coordinateWithX:(float)newX;

-(float)getX;

@end
