//
//  Coordinate1d.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/6/14.
//
//

#import "Coordinate1d.h"

@implementation Coordinate1d

-(id)init
{
    return [self initWithX:0.0f];
}

-(id)initWithX:(float)newX
{
    if (self = [super init])
    {
        x = newX;
    }
    return self;
}

+(Coordinate1d*)coordinateWithX:(float)newX
{
    return [[Coordinate1d alloc] initWithX:newX];
}

-(float)getX
{
    return x;
}

@end
