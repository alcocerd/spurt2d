//
//  MutableCoordinate1d.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/6/14.
//
//

#import "Coordinate1d.h"

@interface MutableCoordinate1d : Coordinate1d

+(MutableCoordinate1d*)coordinateWithX:(float)newX;

-(void)setX:(float)newX;
-(void)addX:(float)xToAdd;

@end
