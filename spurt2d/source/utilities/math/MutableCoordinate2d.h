//
//  MutableCoordinate2d.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/6/14.
//
//

#import "Coordinate2d.h"

@interface MutableCoordinate2d : Coordinate2d

+(MutableCoordinate2d*)coordinateWithCoordinate:(Coordinate2d*)otherCoordinate;
+(MutableCoordinate2d*)coordinateWithX:(float)newX andY:(float)newY;

-(void)moveDistance:(float)distance WithAngle:(float)angle;

-(void)set:(Coordinate2d*)coordinate;

-(void)setX:(float)newX;
-(void)setY:(float)newY;
-(void)addX:(float)xToAdd;
-(void)addY:(float)yToAdd;

@end
