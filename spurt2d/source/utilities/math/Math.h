//
//  Math.h
//  spurt2d
//
//  Created by Daniel Alcocer on 12/28/13.
//
//

extern const float PI;
extern const float PI_OVER_TWO;
extern const float THREE_PI_OVER_TWO;
extern const float TWO_PI;

extern const float DEGREES_PER_RADIAN;
extern const float RADIANS_PER_DEGREE;

@interface Math : NSObject

// Logic.
+(float)minFloat:(float)a :(float)b;
+(float)maxFloat:(float)a :(float)b;
+(int)minInt:(int)a :(int)b;
+(int)maxInt:(int)a :(int)b;
+(float)clampFloat:(float)value Min:(float)min Max:(float)max;
+(int)clampInt:(int)value Min:(int)min Max:(int)max;

// Trigonometry.
+(float)cos:(float)value;
+(float)sin:(float)value;
+(float)arcTan:(float)y :(float)x;

// Geometry.
+(float)distance:(float)x1 :(float)y1 :(float)x2 :(float)y2;

// 
+(float)squareRoot:(float)value;
+(float)power:(float)base :(float)exponent;
+(float)absolute:(float)value;
+(double)floorDouble:(double)value;
+(double)ceilingDouble:(double)value;
+(float)floor:(float)value;
+(float)ceiling:(float)value;

// Random numbers.
+(BOOL)nextBoolean;
+(BOOL)nextBoolean:(float)trueChance;
+(unsigned int)nextInt;
+(unsigned int)nextInt:(int)n;
+(float)nextFloat;
+(float)nextFloat:(float)n;

// Signs.
// NOTE: Be careful of how you use these as precision (or lack thereof) can easily skew expected/intended results in certain circumstances.
// Returns positive if > 0.
+(BOOL)isPositiveDouble:(double)d;
// Returns 1.0 if positive, -1.0 if negative, 0.0 if 0.
+(double)getSignDouble:(double)d;
+(float)getSignFloat:(float)f;

+(float)roundFloat:(float)f;

@end
