//
//  MutableCoordinate2d.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/6/14.
//
//

#import "MutableCoordinate2d.h"

#import "Math.h"

@implementation MutableCoordinate2d

+(MutableCoordinate2d*)coordinateWithCoordinate:(Coordinate2d*)otherCoordinate
{
    return [[MutableCoordinate2d alloc] initWithX:[otherCoordinate getX] andY:[otherCoordinate getY]];
}

+(MutableCoordinate2d*)coordinateWithX:(float)newX andY:(float)newY
{
    return [[MutableCoordinate2d alloc] initWithX:newX andY:newY];
}

-(void)moveDistance:(float)distance WithAngle:(float)angle
{
    float xMove = distance * [Math cos:angle];
    float yMove = distance * [Math sin:angle];
    
    [self addX:xMove];
    [self addY:yMove];
}

-(void)set:(Coordinate2d*)coordinate
{
    [self setX:[coordinate getX]];
    [self setY:[coordinate getY]];
}

-(void)setX:(float)newX
{
    x = newX;
}

-(void)setY:(float)newY
{
    y = newY;
}

-(void)addX:(float)xToAdd
{
    x += xToAdd;
}

-(void)addY:(float)yToAdd
{
    y += yToAdd;
}

@end
