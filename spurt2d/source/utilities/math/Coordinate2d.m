//
//  Coordinate2d.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/6/14.
//
//

#import "Coordinate2d.h"

#import "Math.h"

@implementation Coordinate2d

-(id)init
{
    return [self initWithX:0.0f andY:0.0f];
}

-(id)initWithX:(float)newX andY:(float)newY
{
    if (self = [super init])
    {
        x = newX;
        y = newY;
    }
    return self;
}

+(Coordinate2d*)coordinateWithCoordinate:(Coordinate2d*)otherCoordinate
{
    return [[Coordinate2d alloc] initWithX:[otherCoordinate getX] andY:[otherCoordinate getY]];
}

+(Coordinate2d*)coordinateWithX:(float)newX andY:(float)newY
{
    return [[Coordinate2d alloc] initWithX:newX andY:newY];
}

-(float)getX
{
    return x;
}

-(float)getY
{
    return y;
}

-(float)getAngle:(Coordinate2d*)otherCoordinate
{
    float angle = [Math arcTan:[otherCoordinate getY] - y :[otherCoordinate getX] - x];
    return angle;
}

-(float)getDistance:(Coordinate2d*)otherCoordinate
{
    float distance = [Math distance:[otherCoordinate getX] :[otherCoordinate getY] :x :y];
    return distance;
}

@end
