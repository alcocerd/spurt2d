//
//  EventPublisher.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

#import "EventPublisher.h"

@interface EventPublisher()

-(id)initEventPublisher;
-(NSMutableArray*)getSubscriptionsToEvent:(int)eventID;
-(NSNumber*)getKeyToEvent:(int)eventID;

@end

@implementation EventPublisher

-(id)initEventPublisher
{
    if (self = [super init])
    {
        eventSubscriptions = [[NSMutableDictionary alloc] init];
    }
    return self;
}

// Singleton.
+(EventPublisher*)sharedInstance
{
    static EventPublisher* eventPublisher;
    static dispatch_once_t once;
    dispatch_once(&once, ^
    {
        eventPublisher = [[EventPublisher alloc] initEventPublisher];
    });
    
    return eventPublisher;
}

// Let anyone subscribe to an event.  Return the EventSubscriber so they can publish events if needed, and to be able to unsubscribe.
-(EventSubscriber*)subscribe:(int)eventID withOwner:(id)owner andOperation:(void(^)(id package))operation
{
    // Create the object.
    EventSubscriber* eventSubscriber = [[EventSubscriber alloc] initEventSubscriber:eventID withOwner:owner andOperation:operation];
    
    // Now get the list it belongs to (we sort subscriptions in a dictionary by eventID so that when we publish an event, only that event's number of subscribers matters).
    NSMutableArray* subscriptionsToThisEvent = [self getSubscriptionsToEvent:eventID];
    if(subscriptionsToThisEvent == nil)
    {
        // If the list is nil, no one has subscribed to it yet, so make that list and add it to the dictionary.
        subscriptionsToThisEvent = [[NSMutableArray alloc] init];
        NSNumber* key = [self getKeyToEvent:eventID];
        [eventSubscriptions setObject:subscriptionsToThisEvent forKey:key];
    }
    
    // Add the EventSubscriber to the subscription list.
    [subscriptionsToThisEvent addObject:eventSubscriber];
    
    return eventSubscriber;
}

/*-(void)unsubscribe:(EventSubscriber*)eventSubscriber
{
    // We won't go checking for nils to remove here.  Consider adding a method which does a "clean" to remove all nil'd objects for a given eventID, and probably another method for all events too.
    if (eventSubscriber != nil)
    {
        // Get the list it belongs to, and remove it from that list.
        int eventID = [eventSubscriber getEventID];
        NSMutableArray* subscriptionsToThisEvent = [self getSubscriptionsToEvent:eventID];
        if (subscriptionsToThisEvent != nil)
        {
            [subscriptionsToThisEvent removeObjectIdenticalTo:eventSubscriber];
        }
    }
}*/

-(void)publish:(int)eventID withPackage:(id)package
{
    NSMutableArray* removalList = [[NSMutableArray alloc] init];
    
    NSMutableArray* subscriptionsToThisEvent = [self getSubscriptionsToEvent:eventID];
    
    // If no one has subscribed to this event, it could be nil, so do nothing.
    if (subscriptionsToThisEvent != nil)
    {
        // Otherwise, let them all know that the event was published!
        for (int i = 0; i < [subscriptionsToThisEvent count]; i++)
        {
            EventSubscriber* eventSubscriber = [subscriptionsToThisEvent objectAtIndex:i];
            // If the EventSubscriber's owner is gone, mark it for deletion.
            if (![eventSubscriber hasOwner])
            {
                [removalList addObject:eventSubscriber];
            }
            // Else publish the event.
            else
            {
                [eventSubscriber onPublish:package];
            }
        }
    }
    
    // Remove all subscriptions whose owners are gone.
    [subscriptionsToThisEvent removeObjectsInArray:removalList];
}

// Helper methods to get stuff (lists, keys) from event types.
-(NSMutableArray*)getSubscriptionsToEvent:(int)eventID
{
    NSNumber* key = [self getKeyToEvent:eventID];
    NSMutableArray* subscriptionsToThisEvent = [eventSubscriptions objectForKey:key];
    return subscriptionsToThisEvent;
}

-(NSNumber*)getKeyToEvent:(int)eventID
{
    return [NSNumber numberWithInt:eventID];
}

@end
