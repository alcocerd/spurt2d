//
//  CountdownTimer.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/15/14.
//
//

#import "CountdownTimer.h"

@implementation CountdownTimer

-(id)init
{
    logCritical(@"Creating a nil CountdownTimer.");
    return nil;
}

-(id)initCountdownWithDuration:(CFTimeInterval)newDurationTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation
{
    if (self = [super initWithDuration:newDurationTime andOwner:newOwner andOperation:newOperation])
    {
        
    }
    return self;
}

-(BOOL)checkExpiration
{
    BOOL isExpired = [super checkExpiration];
    
    if (isExpired)
    {
        [self fire];
    }
    
    return isExpired;
}

@end
