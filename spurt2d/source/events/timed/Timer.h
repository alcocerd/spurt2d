//
//  Timer.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

extern const CFTimeInterval INDEFINITE_DURATION;

@interface Timer : NSObject
{
@private
    // The time the Timer started.
    CFTimeInterval startTime;
    // The duration of how long the Timer lasts.
    CFTimeInterval durationTime;
    
    // The running time of this Timer.
    CFTimeInterval elapsedTime;

    // Pause markers.
    CFTimeInterval lastPauseStartTime;
    CFTimeInterval totalPauseTime;
    
    __weak id owner;
    
    BOOL isPaused;
    
    void(^operation)(void);
}

// A Timer with a duration time (pass in INDEFINITE_DURATION or a negative value to make it run indefinitely) and the operation to perform.
-(id)initWithDuration:(CFTimeInterval)newDurationTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation;

// Checks if the Timer is complete and ready to be deallocated (Timer subclasses can perform their important duties by overwriting this).
-(BOOL)checkExpiration;
// Just a simple check to see if it has expired.
-(BOOL)isExpired;

-(BOOL)hasOwner;

// Removes its reference to its owner so TimerScheduler can remove it the next time it updates (and will then deallocate as long as no one else has a reference to this object).
-(void)kill;

// Timer will use this to fire its event, however the public can fire these events prematurelly as many times as desired without affecting the Timer in any way (it will continue to fire as told beforehand).
-(void)fire;

// Pause and unpause timers as necessary.
-(void)pause;
-(void)unpause;

// Category functionality.  By giving Timers categories to be classified by, they can easily be paused/unpaused together (ie pause all game timers when the player hits the pause button, while not affecting timers which animate the clouds in the background).  Note that a Timer doesn't need a category to function, this is purely optional.
-(void)addCategory:(int)category;
-(void)removeCategory:(int)category;
// To be called by whoever has a list of all the Timers, but can be called directly if a need arises.
-(void)pauseCategory:(int)category;
-(void)unpauseCategory:(int)category;

/**
 Time left until the timer expires; this method has undefined behavior for when the duration is indefinite (it will return a CFTimeInterval, that is all that is guaranteed).
 */
-(CFTimeInterval)getTimeLeft;
/**
 The amount of time the Timer has been running.
 */
-(CFTimeInterval)getElapsedTime;
/**
 The amount of time the Timer was given to run.
 */
-(CFTimeInterval)getDurationTime;

@end
