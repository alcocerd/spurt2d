//
//  IntervalTimer.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/15/14.
//
//

#import "IntervalTimer.h"

@implementation IntervalTimer

const CFTimeInterval INTERVAL_EVERY_LOOP = 0.0;

-(id)init
{
    logCritical(@"Creating a nil IntervalTimer.");
    return nil;
}

-(id)initIntervalWithDuration:(CFTimeInterval)newDurationTime andInterval:(CFTimeInterval)newIntervalTime andDelayTime:(CFTimeInterval)newDelayTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation
{
    // TODO: Think of a less hacky way to fix this.
    // We do this since if you pass INDEFINITE_DURATION and a delay of anything 1 or greater, it will never run (since the duration will be 0 or greater).
    CFTimeInterval adjustedDuration = newDurationTime+newDelayTime;
    if (newDurationTime < 0.0)
        adjustedDuration = INDEFINITE_DURATION;
    
    if (self = [super initWithDuration:adjustedDuration andOwner:newOwner andOperation:newOperation])
    {
        intervalTime = newIntervalTime;
        delayTime = newDelayTime;
        nextFireTime = newDelayTime;
    }
    return self;
}

-(BOOL)checkExpiration
{
    BOOL isExpired = [super checkExpiration];
    
    // Once elapsed time has passed the delay time, we can start firing.
    CFTimeInterval elapsed = [self getElapsedTime];
    if (elapsed >= delayTime)
    {
        // Special flag for firing every loop, but still respecting the delay time.
        if (intervalTime <= INTERVAL_EVERY_LOOP)
        {
            [self fire];
        }
        else
        {
            // If the time get backed up, catch up so we can honor our commitment of firing the appropriate amount of times.
            while (elapsed >= nextFireTime)
            {
                nextFireTime += intervalTime;
                [self fire];
            }
        }
    }
    
    return isExpired;
}

-(CFTimeInterval)getDurationTimeWithoutDelay
{
    return [self getDurationTime] - delayTime;
}

-(CFTimeInterval)getElapsedTimeWithoutDelay
{
    return [self getElapsedTime] - delayTime;
}

@end
