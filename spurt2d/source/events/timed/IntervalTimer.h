//
//  IntervalTimer.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/15/14.
//
//

#import "Timer.h"

extern const CFTimeInterval INTERVAL_EVERY_LOOP;

// Be careful of having intervals smaller than the framerate.  This class will ensure that it fires the appropriate amount of times (so if we hit a lag spike, it will catch up and call any "missed" firings).  So, if you have an interval of 1 millisecond (0.001 seconds) and a framerate of ~16 milliseconds, it will fire ~16 times a loop, this is probably not desired.  Instead, just use INTERVAL_EVERY_LOOP to fire once per loop.
@interface IntervalTimer : Timer
{
@private
    CFTimeInterval intervalTime;
    CFTimeInterval delayTime;
    CFTimeInterval nextFireTime;
}

/**
 Note that the delay gets added to the duration (for when getDurationTime is called).  IntervalTime can be INTERVAL_EVERY_LOOP (or <= 0) to signify that it should run once every loop.  Assume passing negative values to any other CFTimeIntervals here will cause undefined (probably bad) behavior.
 */
-(id)initIntervalWithDuration:(CFTimeInterval)newDurationTime andInterval:(CFTimeInterval)newIntervalTime andDelayTime:(CFTimeInterval)newDelayTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation;

/**
 Returns the duration time and substracts the delay.
 */
-(CFTimeInterval)getDurationTimeWithoutDelay;

/**
 Returns the elapsed time and substracts out the delay.
 */
-(CFTimeInterval)getElapsedTimeWithoutDelay;

@end
