//
//  Course.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/16/14.
//
//

#import "IntervalTimer.h"

@class Coordinate1d;
@class Coordinate2d;
@class MutableCoordinate1d;
@class MutableCoordinate2d;

@interface Course : IntervalTimer
{
@protected
    CFTimeInterval lastElapsedTime;
}

-(id)initCourseWithDuration:(CFTimeInterval)newDurationTime andDelayTime:(CFTimeInterval)newDelayTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation;

// For internal use mainly.
/**
 Use this internally instead of [Clock getDeltaTime] as this allows the Course to keep track of its own deltas.
 */
-(CFTimeInterval)getCourseDeltaTime;

/**
 Coordinate to translate, coordinate to go to, duration to get there, delay before it starts, and a boolean to make it go to the destination when it ends (usually true, but usually false if you want to combine several calls on the same coordinate).
 */
+(Course*)translate:(MutableCoordinate2d*)coordinate to:(Coordinate2d*)destination during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay andForceDestination:(BOOL)forceDestination andOwner:newOwner;

/**
 Coordinate to translate, float to go to, duration to get there, delay before it starts, and a boolean to make it go to the destination when it ends (usually true, but usually false if you want to combine several calls on the same coordinate).
 */
+(Course*)translateSingle:(MutableCoordinate1d*)coordinate to:(float)destination during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay andForceDestination:(BOOL)forceDestination andOwner:newOwner;

/**
 Go in a certain direction with no destination in mind.
 */
+(Course*)translate:(MutableCoordinate2d*)coordinate withAngleInDegrees:(float)angleInDegrees during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay withSpeed:(float)speed andOwner:newOwner;

/**
 Coordinate to scale, coordinate to go to, duration to get there, delay before it starts, and a boolean to make it go to the destination when it ends (usually true, but usually false if you want to combine several calls on the same coordinate).
 */
+(Course*)scaleUniform:(MutableCoordinate2d*)coordinate to:(float)destination during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay andForceDestination:(BOOL)forceDestination andOwner:newOwner;

/**
 Coordinate to wiggle, speed to wiggle at, duration of it, range of rotation, delay before this starts, and a boolean to make it go back to the rotation it started at (false will leave it wherever it ends up).
 */
+(Course*)wiggle:(MutableCoordinate1d*)coordinate atSpeed:(float)speed during:(CFTimeInterval)duration withRotationRangeInDegrees:(float)rotationRange withDelay:(CFTimeInterval)delay andResetRotation:(BOOL)resetRotation andOwner:newOwner;

/**
 Coordinate to pulse (like for scaling), speed to pulse at, duration of it, range of pulsing, delay before this starts, and a boolean to make it go back to the point it started at (false will leave it wherever it ends up).
 */
+(Course*)pulse:(MutableCoordinate2d*)coordinate atSpeed:(float)speed during:(CFTimeInterval)duration withRange:(float)range withDelay:(CFTimeInterval)delay andReset:(BOOL)reset andOwner:newOwner;

/**
 Spin a certain distance, ie pass -360 for one full rotation going clockwise and 360 for one full rotation going counter clockwise.
 */
+(Course*)spin:(MutableCoordinate1d*)coordinate withTotalRotationInDegrees:(float)totalRotation during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay andForceDestination:(BOOL)forceDestination andOwner:newOwner;

/**
 Launch an object with an arc using gravity.
 */
+(Course*)launch:(MutableCoordinate2d*)coordinate withAngleInDegrees:(float)angleInDegrees andForce:(float)force withGravity:(float)gravity during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay andOwner:newOwner;

@end
