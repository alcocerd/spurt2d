//
//  Timer.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

#import "Timer.h"

#import "Clock.h"

const CFTimeInterval INDEFINITE_DURATION = -1.0;

@implementation Timer

-(id)init
{
    logCritical(@"Creating a nil Timer.");
    return nil;
}

-(id)initWithDuration:(CFTimeInterval)newDurationTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation
{
    if (self = [super init])
    {
        startTime = [Clock getElapsedTime];
        durationTime = newDurationTime;
        
        elapsedTime = 0.0;
        
        lastPauseStartTime = 0.0;
        totalPauseTime = 0.0;
        
        owner = newOwner;
        
        isPaused = false;
        
        operation = newOperation;
    }
    return self;
}

-(BOOL)checkExpiration
{
    BOOL isExpired = false;
    if (!isPaused)
    {
        elapsedTime = [Clock getElapsedTime] - totalPauseTime - startTime;
        isExpired = [self isExpired];
    }
    
    return isExpired;
}

-(BOOL)isExpired
{
    BOOL isExpired = false;
    
    // Timer is expired if this timer is not indefinite (a negative value) and our elapsed time has met or passed our delay time.
    if (durationTime >= 0.0 && elapsedTime >= durationTime)
        isExpired = true;
    
    return isExpired;
}

-(BOOL)hasOwner
{
    return owner != nil;
}

-(void)kill
{
    owner = nil;
}

-(void)fire
{
    operation();
}

-(void)pause
{
    if (!isPaused)
    {
        isPaused = true;
        lastPauseStartTime = elapsedTime;
    }
}

-(void)unpause
{
    if (isPaused)
    {
        isPaused = false;
        totalPauseTime = ([Clock getElapsedTime] - startTime) - lastPauseStartTime;
    }
}

-(void)addCategory:(int)category
{
    
}

-(void)removeCategory:(int)category
{
    
}

-(void)pauseCategory:(int)category
{
    
}

-(void)unpauseCategory:(int)category
{
    
}

-(CFTimeInterval)getTimeLeft
{
    return durationTime - elapsedTime;
}

-(CFTimeInterval)getElapsedTime
{
    return elapsedTime;
}

-(CFTimeInterval)getDurationTime
{
    return durationTime;
}

@end
