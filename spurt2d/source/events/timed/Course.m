//
//  Course.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/16/14.
//
//

#import "Course.h"

#import "Clock.h"

#import "MutableCoordinate1d.h"
#import "MutableCoordinate2d.h"

#import "Math.h"

@implementation Course

-(id)initCourseWithDuration:(CFTimeInterval)newDurationTime andDelayTime:(CFTimeInterval)newDelayTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation
{
    if (self = [super initIntervalWithDuration:newDurationTime andInterval:INTERVAL_EVERY_LOOP andDelayTime:newDelayTime andOwner:newOwner andOperation:newOperation])
    {
        lastElapsedTime = [Clock getElapsedTime];
    }
    return self;
}

#pragma mark - Time

-(CFTimeInterval)getCourseDeltaTime
{
    CFTimeInterval now = [Clock getElapsedTime];
    CFTimeInterval deltaTime = now - lastElapsedTime;
    lastElapsedTime = now;
    
    return deltaTime;
}

#pragma mark - Courses

+(Course*)translate:(MutableCoordinate2d*)coordinate to:(Coordinate2d*)destination during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay andForceDestination:(BOOL)forceDestination andOwner:(id)newOwner
{
    // Make a copy to ensure the destination does not change.
    destination = [Coordinate2d coordinateWithCoordinate:destination];
    
    float distance = [coordinate getDistance:destination];
    float angle = [coordinate getAngle:destination];
    float speed = 0.0f;
    if (duration > 0.0)
    {
        speed = distance / duration;
    }
    else
    {
        duration = 0.0;
    }
    
    Course* course;
    __weak __block Course* weakCourse;
    void(^operation)(void) = ^
    {
        Course* strongCourse = weakCourse;
        float movement = speed * [strongCourse getCourseDeltaTime];
        [coordinate moveDistance:movement WithAngle:angle];
        
        // Last time this gets called.
        if (forceDestination && [strongCourse isExpired])
        {
            [coordinate set:destination];
        }
    };
    
    course = [[Course alloc] initCourseWithDuration:duration andDelayTime:delay andOwner:newOwner andOperation:operation];
    weakCourse = course;
    
    return course;
}

+(Course*)translateSingle:(MutableCoordinate1d*)coordinate to:(float)destination during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay andForceDestination:(BOOL)forceDestination andOwner:(id)newOwner
{
    float distance = destination - [coordinate getX];
    float speed = 0.0f;
    if (duration > 0.0)
    {
        speed = distance / duration;
    }
    else
    {
        duration = 0.0;
    }
    
    Course* course;
    __weak __block Course* weakCourse;
    void(^operation)(void) = ^
    {
        Course* strongCourse = weakCourse;
        float movement = speed * [strongCourse getCourseDeltaTime];
        [coordinate addX:movement];
        
        // Last time this gets called.
        if (forceDestination && [strongCourse isExpired])
        {
            [coordinate setX:destination];
        }
    };
    
    course = [[Course alloc] initCourseWithDuration:duration andDelayTime:delay andOwner:newOwner andOperation:operation];
    weakCourse = course;
    
    return course;
}

+(Course*)translate:(MutableCoordinate2d*)coordinate withAngleInDegrees:(float)angleInDegrees during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay withSpeed:(float)speed andOwner:newOwner
{
    float angleInRadians = angleInDegrees * RADIANS_PER_DEGREE;
    
    Course* course;
    __weak __block Course* weakCourse;
    void(^operation)(void) = ^
    {
        Course* strongCourse = weakCourse;
        float movement = speed * [strongCourse getCourseDeltaTime];
        [coordinate moveDistance:movement WithAngle:angleInRadians];
    };
    
    course = [[Course alloc] initCourseWithDuration:duration andDelayTime:delay andOwner:newOwner andOperation:operation];
    weakCourse = course;
    
    return course;
}

+(Course*)scaleUniform:(MutableCoordinate2d*)coordinate to:(float)destination during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay andForceDestination:(BOOL)forceDestination andOwner:(id)newOwner
{
    return [Course translate:coordinate to:[Coordinate2d coordinateWithX:destination andY:destination] during:duration withDelay:delay andForceDestination:forceDestination andOwner:newOwner];
}

+(Course*)wiggle:(MutableCoordinate1d*)coordinate atSpeed:(float)speed during:(CFTimeInterval)duration withRotationRangeInDegrees:(float)rotationRange withDelay:(CFTimeInterval)delay andResetRotation:(BOOL)resetRotation andOwner:(id)newOwner
{
    rotationRange *= RADIANS_PER_DEGREE;
    
    float startingValue = [coordinate getX];
    float lowerBound = startingValue - (rotationRange / 2.0f);
    float upperBound = startingValue + (rotationRange / 2.0f);
    __block float direction = 1.0f;
    
    Course* course;
    __weak __block Course* weakCourse;
    void(^operation)(void) = ^
    {
        Course* strongCourse = weakCourse;
        float movement = direction * speed * [strongCourse getCourseDeltaTime];
        [coordinate addX:movement];
        
        // Flip directions.
        while ([coordinate getX] < lowerBound || [coordinate getX] > upperBound)
        {
            // These lines ensure the wiggle is consistent given the same input.
            if ([coordinate getX] < lowerBound)
            {
                float overextension = lowerBound - [coordinate getX];
                [coordinate setX:lowerBound + overextension];
            }
            else if ([coordinate getX] > upperBound)
            {
                float overextension = [coordinate getX] - upperBound;
                [coordinate setX:upperBound - overextension];
            }
            
            direction *= -1.0f;
        }
        
        // Last time this gets called.
        if (resetRotation && [strongCourse isExpired])
        {
            [coordinate setX:startingValue];
        }
    };
    
    course = [[Course alloc] initCourseWithDuration:duration andDelayTime:delay andOwner:newOwner andOperation:operation];
    weakCourse = course;
    
    return course;
}

+(Course*)pulse:(MutableCoordinate2d*)coordinate atSpeed:(float)speed during:(CFTimeInterval)duration withRange:(float)range withDelay:(CFTimeInterval)delay andReset:(BOOL)reset andOwner:newOwner
{
    float startingValue = [coordinate getX];
    
    float lowerBound = startingValue - (range / 2.0f);
    float upperBound = startingValue + (range / 2.0f);
    __block float direction = 1.0f;
    
    Course* course;
    __weak __block Course* weakCourse;
    void(^operation)(void) = ^
    {
        Course* strongCourse = weakCourse;
        float movement = direction * speed * [strongCourse getCourseDeltaTime];
        [coordinate addX:movement];
        [coordinate addY:movement];
        
        // Flip directions.
        while ([coordinate getX] < lowerBound || [coordinate getX] > upperBound)
        {
            // These lines ensure the pulse is consistent given the same input.
            if ([coordinate getX] < lowerBound)
            {
                float overextension = lowerBound - [coordinate getX];
                [coordinate setX:lowerBound + overextension];
                [coordinate setY:lowerBound + overextension];
            }
            else if ([coordinate getX] > upperBound)
            {
                float overextension = [coordinate getX] - upperBound;
                [coordinate setX:upperBound - overextension];
                [coordinate setY:upperBound - overextension];
            }
            
            direction *= -1.0f;
        }
        
        // Last time this gets called.
        if (reset && [strongCourse isExpired])
        {
            [coordinate setX:startingValue];
            [coordinate setY:startingValue];
        }
    };
    
    course = [[Course alloc] initCourseWithDuration:duration andDelayTime:delay andOwner:newOwner andOperation:operation];
    weakCourse = course;
    
    return course;
}

+(Course*)spin:(MutableCoordinate1d*)coordinate withTotalRotationInDegrees:(float)totalRotation during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay andForceDestination:(BOOL)forceDestination andOwner:(id)newOwner
{
    totalRotation *= RADIANS_PER_DEGREE;
    
    float destination = [coordinate getX] + totalRotation;
    float speed = 0.0f;
    if (duration > 0.0)
    {
        speed = totalRotation / duration;
    }
    else
    {
        duration = 0.0;
    }
    
    Course* course;
    __weak __block Course* weakCourse;
    void(^operation)(void) = ^
    {
        Course* strongCourse = weakCourse;
        float movement = speed * [strongCourse getCourseDeltaTime];
        [coordinate addX:movement];
        
        // Last time this gets called.
        if (forceDestination && [strongCourse isExpired])
        {
            [coordinate setX:destination];
        }
    };
    
    course = [[Course alloc] initCourseWithDuration:duration andDelayTime:delay andOwner:newOwner andOperation:operation];
    weakCourse = course;
    
    return course;
}

+(Course*)launch:(MutableCoordinate2d*)coordinate withAngleInDegrees:(float)angleInDegrees andForce:(float)force withGravity:(float)gravity during:(CFTimeInterval)duration withDelay:(CFTimeInterval)delay andOwner:newOwner
{
    float angleInRadians = angleInDegrees * RADIANS_PER_DEGREE;
    
    float xStart = [coordinate getX];
    float yStart = [coordinate getY];
    
    float xStartVelocity = force * [Math cos:angleInRadians];
    float yStartVelocity = force * [Math sin:angleInRadians];
    
    Course* course;
    __weak __block Course* weakCourse;
    void(^operation)(void) = ^
    {
        Course* strongCourse = weakCourse;
        float time = (float)[strongCourse getElapsedTimeWithoutDelay];
        
        [coordinate setX:xStart + (time * xStartVelocity)];
        [coordinate setY:yStart + ((time * yStartVelocity) - 0.5f * gravity * [Math power:time :2.0f])];
    };
    
    course = [[Course alloc] initCourseWithDuration:duration andDelayTime:delay andOwner:newOwner andOperation:operation];
    weakCourse = course;
    
    return course;
}

@end
