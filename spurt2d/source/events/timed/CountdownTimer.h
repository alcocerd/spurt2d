//
//  CountdownTimer.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/15/14.
//
//

#import "Timer.h"

@interface CountdownTimer : Timer

-(id)initCountdownWithDuration:(CFTimeInterval)newDurationTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation;

@end
