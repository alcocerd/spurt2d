//
//  TimerScheduler.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

// Think about this some more: use forward declarations and move these to the implementation file or is this decision better?
#import "Timer.h"
#import "CountdownTimer.h"
#import "IntervalTimer.h"
#import "Course.h"

extern const CFTimeInterval FIRE_COUNTDOWN_IMMEDIATELY;

// Notes about owner parameter:
// Sending "self" as owner is ok as long as the block does not contain any strong references to "self" (otherwise this will cause a circular reference)  If the block must contain strong references to "self" (although there is little reason for this since it's easy to avoid), passing a generic object (i.e. id) that is a member of "self" should work (as long as it is not nil).  Just remember to set that object to nil when "self" plans to deallocate (doing this in dealloc will not work as the block will still have a strong reference to "self").  Going this route will probably require a lifecycle system.
@interface TimerScheduler : NSObject

-(void)update;

// TODO: finish this feature.
+(void)pauseCategory:(int)category;
+(void)unpauseCategory:(int)category;

/**
 If duration time is 0 or negative value, this will fire immediately (or just pass in FIRE_COUNTDOWN_IMMEDIATELY).  If 0, it will run the next time TimerScheduler has its update method called.  Otherwise, the behavior is as expected for anything over 0.  Pass in the operation to perform.
 */
+(CountdownTimer*)startCountdownTimerWithDuration:(CFTimeInterval)newDurationTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation;

/**
 Pass in INDEFINITE_DURATION (or any negative value) to make this timer run indefinitely, or give a finite duration.  Then specifiy the time interval (INTERVAL_EVERY_LOOP or any value <= 0.0 to signify to run once per loop), the delay before it starts firing, and the operation to perform.
 */
+(IntervalTimer*)startIntervalTimerWithDuration:(CFTimeInterval)newDurationTime andInterval:(CFTimeInterval)newIntervalTime andDelayTime:(CFTimeInterval)newDelayTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation;

// Just a helper method to allow one-liners, ie: Course* course = [TimerScheduler startCourse:[Course doWhatever]];
+(Course*)startCourse:(Course*)course;

/**
 Add a timer.
 */
+(Timer*)addTimer:(Timer*)timer;

// Use kill inside Timer.
//+(void)killTimer:(Timer*)timer;

@end
