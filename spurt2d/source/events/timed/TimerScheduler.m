//
//  TimerScheduler.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

#import "TimerScheduler.h"

const CFTimeInterval FIRE_COUNTDOWN_IMMEDIATELY = 0.0;

@implementation TimerScheduler

static NSMutableArray* timers;

-(id)init
{
    if (self = [super init])
    {
        timers = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)update
{
    NSMutableArray* removalList = [[NSMutableArray alloc] init];
    
    // Run the timers.  Delete the expired ones or the ones with no owners.
    for (int i = 0; i < [timers count]; i++)
    {
        Timer* timer = [timers objectAtIndex:i];
        if (![timer hasOwner] || [timer checkExpiration])
        {
            [removalList addObject:timer];
        }
    }
    
    [timers removeObjectsInArray:removalList];
}

+(void)pauseCategory:(int)category
{
    for (Timer* timer in timers)
    {
        [timer pauseCategory:category];
    }
}

+(void)unpauseCategory:(int)category
{
    for (Timer* timer in timers)
    {
        [timer unpauseCategory:category];
    }
}

+(CountdownTimer*)startCountdownTimerWithDuration:(CFTimeInterval)newDurationTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation
{
    CountdownTimer* timer = [[CountdownTimer alloc] initCountdownWithDuration:newDurationTime andOwner:newOwner andOperation:newOperation];
    
    // We fire negative values immediately.
    if (newDurationTime <= FIRE_COUNTDOWN_IMMEDIATELY)
    {
        [timer fire];
    }
    else
    {
        [self addTimer:timer];
    }
    
    return timer;
}

+(IntervalTimer*)startIntervalTimerWithDuration:(CFTimeInterval)newDurationTime andInterval:(CFTimeInterval)newIntervalTime andDelayTime:(CFTimeInterval)newDelayTime andOwner:(id)newOwner andOperation:(void(^)(void))newOperation
{
    IntervalTimer* timer = [[IntervalTimer alloc] initIntervalWithDuration:newDurationTime andInterval:newIntervalTime andDelayTime:newDelayTime andOwner:newOwner andOperation:newOperation];
    
    return (IntervalTimer*)[self addTimer:timer];
}

+(Course*)startCourse:(Course*)course
{
    return (Course*)[self addTimer:course];
}

+(Timer*)addTimer:(Timer*)timer
{
    [timers addObject:timer];
    
    return timer;
}


/*+(void)killTimer:(Timer*)timer
{
    if (timer != nil)
    {
        [timers removeObjectIdenticalTo:timer];
    }
}*/

@end
