//
//  EventSubscriber.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

#import "EventSubscriber.h"

@implementation EventSubscriber

-(id)init
{
    logCritical(@"Creating a nil EventSubscriber.");
    return nil;
}

-(id)initEventSubscriber:(int)newEventID withOwner:(id)newOwner andOperation:(void(^)(id package))newOperation
{
    if (self = [super init])
    {
        operation = newOperation;
        owner = newOwner;
        eventID = newEventID;
    }
    return self;
}

-(void)onPublish:(id)package
{
    operation(package);
}

-(int)getEventID
{
    return eventID;
}

-(BOOL)hasOwner
{
    return owner != nil;
}

-(void)unsubscribe
{
    owner = nil;
}

@end
