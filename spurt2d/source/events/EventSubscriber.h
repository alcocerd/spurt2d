//
//  EventSubscriber.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

@interface EventSubscriber : NSObject
{
@private
    int eventID;
    __weak id owner;
    void(^operation)(id package);
}

-(id)initEventSubscriber:(int)newEventID withOwner:(id)newOwner andOperation:(void(^)(id package))newOperation;

-(void)onPublish:(id)package;
-(int)getEventID;
-(BOOL)hasOwner;

// Removes its reference to its owner so EventPublisher can remove it the next time it publishes the event id in this object (and will then deallocate as long as no one else has a reference to this object).
-(void)unsubscribe;

@end
