//
//  EventPublisher.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/14/14.
//
//

#import "EventSubscriber.h"

@interface EventPublisher : NSObject
{
@private
    NSMutableDictionary* eventSubscriptions;
}

+(EventPublisher*)sharedInstance;

// Sending "self" as owner is ok as long as the block does not contain any strong references to "self" (otherwise this will cause a circular reference)  If the block must contain strong references to "self" (although there is little reason for this since it's easy to avoid), passing a generic object (i.e. id) that is a member of "self" should work (as long as it is not nil).  Just remember to set that object to nil when "self" plans to deallocate (doing this in dealloc will not work as the block will still have a strong reference to "self").  Going this route will probably require a lifecycle system.
-(EventSubscriber*)subscribe:(int)eventID withOwner:(id)owner andOperation:(void(^)(id package))operation;

// Use unsubscribe inside EventSubscriber.
//-(void)unsubscribe:(EventSubscriber*)eventSubscriber;

-(void)publish:(int)eventID withPackage:(id)package;

@end
