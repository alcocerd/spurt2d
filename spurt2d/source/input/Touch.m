//
//  Touch.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/12/14.
//
//

#import "Touch.h"

#import "Coordinate2d.h"

@implementation Touch

-(id)init
{
    return [self initWithCoordinate:[Coordinate2d coordinateWithX:0.0f andY:0.0f] AndType:TT_TOUCH_BEGAN];
}

-(id)initWithCoordinate:(Coordinate2d*)coordinate AndType:(TOUCH_TYPE)type
{
    if (self = [super init])
    {
        touchCoordinate = coordinate;
        touchType = type;
    }
    return self;
}

-(Coordinate2d*)getCoordinate
{
    return touchCoordinate;
}

-(TOUCH_TYPE)getType
{
    return touchType;
}

@end
