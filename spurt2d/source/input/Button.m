//
//  Button.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/24/14.
//
//

#import "Button.h"

#import "Frame.h"

@implementation Button

-(id)init
{
    return [self initWithFrame:nil];
}

-(id)initWithFrame:(Frame*)newFrame
{
    if (self = [super init])
    {
        frames = [[NSMutableArray alloc] init];
        
        [frames addObject:newFrame];
        
        operationPress = nil;
        operationEnter = nil;
        operationExit = nil;
        
        wasInside = false;
        
        [self setActive:true];
        [self setPressOnUp:true];
        
        [self registerPress:NULL];
        [self registerEnter:NULL];
        [self registerExit:NULL];
    }
    return self;
}

+(Button*)buttonWithFrame:(Frame*)newFrame
{
    return [[Button alloc] initWithFrame:newFrame];
}

-(void)addFrame:(Frame*)newFrame
{
    [frames addObject:newFrame];
}

-(void)removeFrame:(Frame*)newFrame
{
    [frames removeObject:newFrame];
}

-(BOOL)interact:(Touch*)touch
{
    BOOL didInteract = false;
    
    // If this button is not active, don't do anything and return false.
    if (isActive)
    {
        TOUCH_TYPE touchType = [touch getType];
        
        // If the touch is an "on down" and this button is also an "on down," then this matches.  If both are "on up," then it matches as well.
        BOOL doesMatchPressType = (touchType == TT_TOUCH_BEGAN && !doesPressOnUp) || (touchType == TT_TOUCH_ENDED && doesPressOnUp);
        // If the touch is inside any frame.
        BOOL isInside = false;
        for (Frame* frame in frames)
        {
            isInside = [frame isInside:[touch getCoordinate]];
            if (isInside)
            {
                break;
            }
        }
        // A touch can enter a button if a user just started touching the screen or is moving their finger around.
        BOOL canEnter = touchType == TT_TOUCH_BEGAN || touchType == TT_TOUCH_MOVED;
        // A touch can exit a button if a user just lifted their finger from the screen or is moving their finger around.
        BOOL canExit = touchType == TT_TOUCH_ENDED || touchType == TT_TOUCH_MOVED;
        
        // If we can enter, we were outside the last loop, and we are now inside, then enter!
        if (canEnter && !wasInside && isInside)
        {
            [self enter];
            // We consider entering a button as an interaction.
            didInteract = true;
        }
        // If we can exit, we were inside the last loop, and we are now outside, then exit!
        if (canExit && wasInside && !isInside)
        {
            [self exit];
        }
        // If we matched press types and we collided, then press!
        if (doesMatchPressType && isInside)
        {
            // Pressing also counts as an exit.
            [self exit];
            // Press.
            [self press];
            // We consider pressing a button as an interaction.
            didInteract = true;
            // No longer considered inside since we pressed.
            isInside = false;
        }
        
        wasInside = isInside;
    }
    return didInteract;
}

-(void)setActive:(BOOL)newIsActive
{
    isActive = newIsActive;
}

-(void)setPressOnUp:(BOOL)newDoesPressOnUp
{
    doesPressOnUp = newDoesPressOnUp;
}

-(void)registerPress:(void(^)(void))newOperationPress
{
    operationPress = newOperationPress;
}

-(void)registerEnter:(void(^)(void))newOperationEnter
{
    operationEnter = newOperationEnter;
}

-(void)registerExit:(void(^)(void))newOperationExit
{
    operationExit = newOperationExit;
}

-(void)press
{
    if (operationPress != NULL)
        operationPress();
}

-(void)enter
{
    if (operationEnter != NULL)
        operationEnter();
}

-(void)exit
{
    if (operationExit != NULL)
        operationExit();
}

@end
