//
//  Button.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/24/14.
//
//

#import "Interactable.h"

@class Frame;

@interface Button : NSObject <Interactable>
{
@private
    NSMutableArray* frames;
    
    void(^operationPress)(void);
    void(^operationEnter)(void);
    void(^operationExit)(void);
    
    BOOL wasInside;
    
    BOOL isActive;
    BOOL doesPressOnUp;
}

-(id)initWithFrame:(Frame*)newFrame;

+(Button*)buttonWithFrame:(Frame*)newFrame;

/**
 Add a frame to include when checking for interactions.
 */
-(void)addFrame:(Frame*)newFrame;
/**
 Remove a frame to exclude when checking for interactions.
 */
-(void)removeFrame:(Frame*)newFrame;

/**
 True to activate the button, false to deactivate.  While false, it will never register any events.
 */
-(void)setActive:(BOOL)newIsActive;
/**
 True makes the button press when the user lifts their finger on the button.  False makes the button press when the user touches down on the button with their finger.
 */
-(void)setPressOnUp:(BOOL)newDoesPressOnUp;

/**
 The operation to perform when the button is pressed.
 */
-(void)registerPress:(void(^)(void))newOperationPress;
/**
 The operation to perform when the button is entered (usually for animations).
 */
-(void)registerEnter:(void(^)(void))newOperationEnter;
/**
 The operation to perform when the button is exited (usually for animations).
 */
-(void)registerExit:(void(^)(void))newOperationExit;

/**
 Not really for public use as the button will call this when appropriate, but can be used to simulate the operation.
 */
-(void)press;
/**
 Not really for public use as the button will call this when appropriate, but can be used to simulate the operation.
 */
-(void)enter;
/**
 Not really for public use as the button will call this when appropriate, but can be used to simulate the operation.
 */
-(void)exit;

@end
