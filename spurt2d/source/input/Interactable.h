//
//  Interactable.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/24/14.
//
//

#import "Touch.h"

@protocol Interactable <NSObject>

/**
 Returns true if the given touch interacted with this object (this will result in consuming this touch so no other object may interact with it), false otherwise.
 */
-(BOOL)interact:(Touch*)touch;

@end
