//
//  TouchPublisher.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/12/14.
//
//

#import "Touch.h"

@class Touch;

@protocol TouchSubscriber <NSObject>

-(void)onTouch:(Touch*)touch;

@end

// Currently no support for multi-touch, will add later.
@interface TouchPublisher : NSObject
{
@private
    NSMutableArray* touchSubscribers;
    
    NSMutableArray* touchQueue;
    
    Touch* lastTouch;
}

+(TouchPublisher*)sharedInstance;

// Register for events.
-(void)subscribe:(id<TouchSubscriber>)subscriber;
-(void)unsubscribe:(id<TouchSubscriber>)subscriber;

// Director should call these; these send out events.  Might add these functions with Coordinate2d instead of NSSet so anyone can "simulate" input more easily.
-(void)touchesBegan:(NSSet*)touches;
-(void)touchesMoved:(NSSet*)touches;
-(void)touchesEnded:(NSSet*)touches;

// For Director to call when it is ready to have the game handle input logic.
-(void)publishEvents;

/**
 Returns the last position recieved, if none recieved yet, returns nil.
 */
-(Coordinate2d*)getLastPosition;

@end
