//
//  TouchPublisher.m
//  spurt2d
//
//  Created by Daniel Alcocer on 1/12/14.
//
//

#import "TouchPublisher.h"

#import "WeakContainer.h"
#import "ScreenOrientator.h"
#import "Coordinate2d.h"

// For UITouch.
#import <UIKit/UIKit.h>

@interface TouchPublisher ()

-(id)initTouchPublisher;

-(void)publishTouch:(NSSet*)touches withType:(TOUCH_TYPE)type;

@end

@implementation TouchPublisher

-(id)initTouchPublisher
{
    if (self = [super init])
    {
        touchSubscribers = [[NSMutableArray alloc] init];
        
        touchQueue = [[NSMutableArray alloc] init];
        
        lastTouch = nil;
    }
    return self;
}

+(TouchPublisher*)sharedInstance
{
    static TouchPublisher* touchPublisher;
    static dispatch_once_t once;
    dispatch_once(&once, ^
    {
        touchPublisher = [[TouchPublisher alloc] initTouchPublisher];
    });
    
    return touchPublisher;
}

-(void)subscribe:(id<TouchSubscriber>)subscriber
{
    // Wrap the subscribers in a WeakContainer so this publisher doesn't claim ownership of them.
    WeakContainer* subscriberContainer = [[WeakContainer alloc] init:subscriber];
    [touchSubscribers addObject:subscriberContainer];
}

-(void)unsubscribe:(id<TouchSubscriber>)subscriber
{
    if (subscriber != nil)
    {
        // Unwrap the containers.
        int index = 0;
        for (WeakContainer* subscriberContainer in touchSubscribers)
        {
            if (subscriber == (id<TouchSubscriber>)[subscriberContainer getObject])
            {
                [touchSubscribers removeObjectAtIndex:index];
                break;
            }
            index++;
        }
    }
}

-(void)touchesBegan:(NSSet*)touches
{
    [self publishTouch:touches withType:TT_TOUCH_BEGAN];
}

-(void)touchesMoved:(NSSet*)touches
{
    [self publishTouch:touches withType:TT_TOUCH_MOVED];
}

-(void)touchesEnded:(NSSet*)touches
{
    [self publishTouch:touches withType:TT_TOUCH_ENDED];
}

-(void)publishTouch:(NSSet*)touches withType:(TOUCH_TYPE)type
{
    NSArray* touchList = [touches allObjects];
    UITouch* uiTouch = [touchList lastObject];
    CGPoint touchPoint = [uiTouch locationInView:uiTouch.view];
    
    Coordinate2d* coordinate = [[Coordinate2d alloc] initWithX:touchPoint.x andY:touchPoint.y];
    coordinate = [[ScreenOrientator sharedInstance] orientInput:coordinate];
    
    Touch* touch = [[Touch alloc] initWithCoordinate:coordinate AndType:type];
    [touchQueue addObject:touch];
}

-(void)publishEvents
{
    NSMutableArray* removalList = [[NSMutableArray alloc] init];
    
    // This makes thread assumptions (namely that the input and game share the same thread).
    for (Touch* touch in touchQueue)
    {
        // Unwrap the containers and mark nil'd subscribers for deletion.
        for (int i = 0; i < [touchSubscribers count]; i++)
        {
            WeakContainer* subscriberContainer = [touchSubscribers objectAtIndex:i];
            
            id<TouchSubscriber> touchSubscriber = (id<TouchSubscriber>)[subscriberContainer getObject];
            if (touchSubscriber == nil)
            {
                [removalList addObject:subscriberContainer];
            }
            else
            {
                [touchSubscriber onTouch:touch];
            }
        }
        
        lastTouch = touch;
    }
    
    // Clear out the queue.
    [touchQueue removeAllObjects];
    
    // Remove the nil'd subscribers.
    [touchSubscribers removeObjectsInArray:removalList];
}

-(Coordinate2d*)getLastPosition
{
    if (lastTouch)
        return [lastTouch getCoordinate];
    else
        return nil;
}

@end
