//
//  Touch.h
//  spurt2d
//
//  Created by Daniel Alcocer on 1/12/14.
//
//

@class Coordinate2d;

typedef enum
{
    TT_TOUCH_BEGAN,
    TT_TOUCH_MOVED,
    TT_TOUCH_ENDED,
    
} TOUCH_TYPE;

@interface Touch : NSObject
{
@private
    TOUCH_TYPE touchType;
    Coordinate2d* touchCoordinate;
}

-(id)initWithCoordinate:(Coordinate2d*)coordinate AndType:(TOUCH_TYPE)type;

-(Coordinate2d*)getCoordinate;
-(TOUCH_TYPE)getType;

@end
